<?php

/* white-simple/woo_ext_related_products_list.twig */
class __TwigTemplate_44fa9b2870ef207df86d988fc23471a7f95d58f6f5f996f6a05f2460dbc9b569 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row nsn_woo_ext_search_block_padding_sm\">
\t<ul class=\"products nsn_woo_ext_search_ul_multiline\">

\t";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products_list"]) ? $context["products_list"] : $this->getContext($context, "products_list")));
        foreach ($context['_seq'] as $context["_key"] => $context["nextProduct"]) {
            // line 5
            echo "\t\t<div class=\"row nsn_woo_ext_search_next_woo_product\">
\t\t\t<li>
\t\t\t\t<h3><a href=\"";
            // line 7
            echo $this->getAttribute($context["nextProduct"], "permalink", array());
            echo "\" class=\"nsn_woo_ext_search_a_link\">";
            echo $this->getAttribute($context["nextProduct"], "post_title", array());
            echo "</a></h3>

\t\t\t\t";
            // line 9
            if (($this->getAttribute($context["nextProduct"], "is_in_stock", array(), "array") == 1)) {
                // line 10
                echo "\t\t\t\t\t<img src=\"";
                echo (isset($context["plugin_images"]) ? $context["plugin_images"] : $this->getContext($context, "plugin_images"));
                echo "in_stock.png\">
\t\t\t\t";
            }
            // line 11
            echo "&nbsp;
\t\t\t\t<span class=\"price\">
\t\t\t\t\t<span class=\"woocommerce-Price-amount amount\">";
            // line 13
            echo $this->getAttribute($context["nextProduct"], "formatted_regular_price", array());
            echo "</span>
\t\t\t\t\t";
            // line 14
            if (( !twig_test_empty($this->getAttribute($context["nextProduct"], "formatted_regular_price", array())) &&  !twig_test_empty($this->getAttribute($context["nextProduct"], "formatted_sale_price", array())))) {
                echo "-";
            }
            // line 15
            echo "\t\t\t\t\t<span class=\"woocommerce-Price-amount amount\">";
            echo $this->getAttribute($context["nextProduct"], "formatted_sale_price", array());
            echo "</span>
\t\t\t\t</span>
\t\t\t</li>
\t\t</div>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['nextProduct'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "
\t</ul>
</div>";
    }

    public function getTemplateName()
    {
        return "white-simple/woo_ext_related_products_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 20,  59 => 15,  55 => 14,  51 => 13,  47 => 11,  41 => 10,  39 => 9,  32 => 7,  28 => 5,  24 => 4,  19 => 1,);
    }
}
/* <div class="row nsn_woo_ext_search_block_padding_sm">*/
/* 	<ul class="products nsn_woo_ext_search_ul_multiline">*/
/* */
/* 	{% for nextProduct in products_list %}*/
/* 		<div class="row nsn_woo_ext_search_next_woo_product">*/
/* 			<li>*/
/* 				<h3><a href="{{ nextProduct.permalink }}" class="nsn_woo_ext_search_a_link">{{ nextProduct.post_title }}</a></h3>*/
/* */
/* 				{% if nextProduct['is_in_stock'] == 1 %}*/
/* 					<img src="{{ plugin_images }}in_stock.png">*/
/* 				{% endif %}&nbsp;*/
/* 				<span class="price">*/
/* 					<span class="woocommerce-Price-amount amount">{{ nextProduct.formatted_regular_price }}</span>*/
/* 					{% if nextProduct.formatted_regular_price is not empty and nextProduct.formatted_sale_price is not empty %}-{% endif %}*/
/* 					<span class="woocommerce-Price-amount amount">{{ nextProduct.formatted_sale_price }}</span>*/
/* 				</span>*/
/* 			</li>*/
/* 		</div>*/
/* 	{% endfor %}*/
/* */
/* 	</ul>*/
/* </div>*/
