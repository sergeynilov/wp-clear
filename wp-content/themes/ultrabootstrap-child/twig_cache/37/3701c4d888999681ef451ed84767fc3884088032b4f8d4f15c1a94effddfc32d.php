<?php

/* white-simple/base.twig */
class __TwigTemplate_b356e82fecf7f300e4d2cede58f4286e2975e2939de9a562e3fcd5e2e5c43307 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'attached_media' => array($this, 'block_attached_media'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["library"] = $this->loadTemplate("white-simple/lib/library.twig", "white-simple/base.twig", 1);
        // line 2
        $this->displayBlock('header', $context, $blocks);
        // line 17
        echo "
<div id=\"content\">";
        // line 18
        $this->displayBlock('content', $context, $blocks);
        echo "</div>

";
        // line 20
        $this->displayBlock('footer', $context, $blocks);
    }

    // line 2
    public function block_header($context, array $blocks = array())
    {
        // line 3
        echo "
<head>
    ";
        // line 5
        $this->displayBlock('attached_media', $context, $blocks);
        // line 7
        echo "

</head>
<body>

<div class=\"container \">
    <main>


";
    }

    // line 5
    public function block_attached_media($context, array $blocks = array())
    {
        // line 6
        echo "    ";
    }

    // line 18
    public function block_content($context, array $blocks = array())
    {
    }

    // line 20
    public function block_footer($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "white-simple/base.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 20,  68 => 18,  64 => 6,  61 => 5,  48 => 7,  46 => 5,  42 => 3,  39 => 2,  35 => 20,  30 => 18,  27 => 17,  25 => 2,  23 => 1,);
    }
}
/* {% import 'white-simple/lib/library.twig' as library %}*/
/* {% block header %}*/
/* */
/* <head>*/
/*     {% block attached_media %}*/
/*     {% endblock attached_media %}*/
/* */
/* */
/* </head>*/
/* <body>*/
/* */
/* <div class="container ">*/
/*     <main>*/
/* */
/* */
/* {% endblock header %}*/
/* */
/* <div id="content">{% block content %}{% endblock content %}</div>*/
/* */
/* {% block footer %}*/
/* {% endblock footer %}*/
