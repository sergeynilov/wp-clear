<?php

/* white-simple/filter_attrs_box.twig */
class __TwigTemplate_4f4319b3e297c3120b2853084e4d33cbc9aafe81a1957de5af55f2e673ab0bcc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attrs_options_list"]) ? $context["attrs_options_list"] : $this->getContext($context, "attrs_options_list")));
        foreach ($context['_seq'] as $context["_key"] => $context["attrs_option"]) {
            // line 3
            echo "
\t";
            // line 4
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_start", array(0 => ("Filter on " . $this->getAttribute($context["attrs_option"], "attr_name", array(), "array")), 1 => "", 2 => "fa fa-search-plus", 3 => (((isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix")) . "block_") . $this->getAttribute($context["attrs_option"], "attr_name", array(), "array"))), "method");
            echo "
\t<div class=\"row\">
\t\t";
            // line 7
            echo "
\t\t";
            // line 9
            echo "\t\t\t";
            // line 10
            echo "\t\t\t\t";
            // line 11
            echo "\t\t\t\t";
            // line 12
            echo "\t\t\t\t";
            // line 13
            echo "\t\t\t\t\t";
            // line 14
            echo "\t\t\t\t";
            // line 15
            echo "
\t\t\t";
            // line 17
            echo "\t\t\t";
            // line 18
            echo "\t\t";
            // line 19
            echo "
\t\t";
            // line 21
            echo "\t\t\t";
            $context["attrs_list"] = $this->getAttribute($context["attrs_option"], "attrs_list", array(), "array");
            // line 22
            echo "
\t\t\t<ul class=\"nsn_woo_ext_search_ul_multiline left\" >
\t\t\t\t";
            // line 24
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["attrs_list"]) ? $context["attrs_list"] : $this->getContext($context, "attrs_list")));
            foreach ($context['_seq'] as $context["_key"] => $context["next_attr"]) {
                // line 25
                echo "
\t\t\t\t\t";
                // line 26
                if ( !(($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "show_in_extended_search_hide_with_zero_products", array()) == "yes") &&  !$this->getAttribute($context["next_attr"], "products_count", array(), "any", true, true))) {
                    // line 27
                    echo "\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"";
                    // line 28
                    echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
                    echo $this->getAttribute($context["attrs_option"], "attr_name", array(), "array");
                    echo "_next_attr_";
                    echo $this->getAttribute($context["next_attr"], "slug", array());
                    echo "\" value=\"";
                    echo $this->getAttribute($context["next_attr"], "slug", array());
                    echo "\" class=\"";
                    echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
                    echo "cbx_";
                    echo $this->getAttribute($context["attrs_option"], "attr_name", array(), "array");
                    echo "_selection_filters\"  >

\t\t\t\t\t\t\t";
                    // line 30
                    if ((($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "run_search_by_click", array()) == "both") || ($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "run_search_by_click", array()) == "link"))) {
                        // line 31
                        echo "\t\t\t\t\t\t\t<a class=\"nsn_woo_ext_search_a_link\" onclick=\"javascript:  nsn_woo_ext_search_frontendFuncsObj.filter_elementClicked( 'attr', '";
                        echo $this->getAttribute($context["attrs_option"], "attr_name", array(), "array");
                        echo "', '";
                        echo $this->getAttribute($context["next_attr"], "slug", array());
                        echo "' );return false;\">
\t\t\t\t\t\t\t\t";
                    }
                    // line 33
                    echo "\t\t\t\t\t\t\t\t&nbsp;";
                    echo $this->getAttribute($context["next_attr"], "name", array());
                    if ($this->getAttribute($context["next_attr"], "products_count", array(), "any", true, true)) {
                        echo "<b>(";
                        echo $this->getAttribute($context["next_attr"], "products_count", array());
                        echo "</b>)";
                    } else {
                    }
                    // line 34
                    echo "\t\t\t\t\t\t\t\t";
                    if ((($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "run_search_by_click", array()) == "both") || ($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "run_search_by_click", array()) == "link"))) {
                        // line 35
                        echo "\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t";
                    }
                    // line 37
                    echo "
\t\t\t\t\t\t</li>
\t\t\t\t\t";
                }
                // line 40
                echo "
\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_attr'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 42
            echo "\t\t\t</ul>

\t\t";
            // line 45
            echo "
\t</div>
\t";
            // line 47
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_end", array(0 => (("Filter on " . $this->getAttribute($context["attrs_option"], "attr_name", array(), "array")) . " Block End")), "method");
            echo "

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attrs_option'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "





";
    }

    public function getTemplateName()
    {
        return "white-simple/filter_attrs_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 50,  135 => 47,  131 => 45,  127 => 42,  120 => 40,  115 => 37,  111 => 35,  108 => 34,  99 => 33,  91 => 31,  89 => 30,  75 => 28,  72 => 27,  70 => 26,  67 => 25,  63 => 24,  59 => 22,  56 => 21,  53 => 19,  51 => 18,  49 => 17,  46 => 15,  44 => 14,  42 => 13,  40 => 12,  38 => 11,  36 => 10,  34 => 9,  31 => 7,  26 => 4,  23 => 3,  19 => 2,);
    }
}
/* {#attrs_options_list::{{ attrs_options_list | d }}#}*/
/* {% for attrs_option in attrs_options_list %}*/
/* */
/* 	{{ library.header_block_start("Filter on " ~ attrs_option['attr_name'], "", "fa fa-search-plus", plugin_prefix ~ "block_" ~ attrs_option['attr_name'] ) }}*/
/* 	<div class="row">*/
/* 		{#{% set is_block_used = false %}#}*/
/* */
/* 		{#{% if attrs_option['attr_name'] == 'rating' %}#}*/
/* 			{#<div class="nsn_woo_ext_search_block_padding_sm">#}*/
/* 				{#Select Rating range from 1 till 5:<br>#}*/
/* 				{#<dt id="slider_rating"></dt>#}*/
/* 				{#<dd>#}*/
/* 					{#<input type="text" readonly id="slider_rating_range_info" style="width: 90%" >#}*/
/* 				{#</dd>#}*/
/* */
/* 			{#</div>#}*/
/* 			{#{% set is_block_used = true %}#}*/
/* 		{#{% endif %}#}*/
/* */
/* 		{#{% if is_block_used != true %}#}*/
/* 			{% set attrs_list= attrs_option['attrs_list'] %}*/
/* */
/* 			<ul class="nsn_woo_ext_search_ul_multiline left" >*/
/* 				{% for next_attr in attrs_list %}*/
/* */
/* 					{% if not ( woo_ext_search_options.show_in_extended_search_hide_with_zero_products == "yes" and next_attr.products_count is not defined ) %}*/
/* 						<li>*/
/* 							<input type="checkbox" id="{{ plugin_prefix }}{{ attrs_option['attr_name'] }}_next_attr_{{ next_attr.slug }}" value="{{ next_attr.slug }}" class="{{ plugin_prefix }}cbx_{{ attrs_option['attr_name'] }}_selection_filters"  >*/
/* */
/* 							{% if woo_ext_search_options.run_search_by_click == 'both' or woo_ext_search_options.run_search_by_click == 'link' %}*/
/* 							<a class="nsn_woo_ext_search_a_link" onclick="javascript:  nsn_woo_ext_search_frontendFuncsObj.filter_elementClicked( 'attr', '{{ attrs_option['attr_name'] }}', '{{ next_attr.slug }}' );return false;">*/
/* 								{% endif %}*/
/* 								&nbsp;{{ next_attr.name }}{% if next_attr.products_count is defined %}<b>({{ next_attr.products_count }}</b>){% else %}{% endif %}*/
/* 								{% if woo_ext_search_options.run_search_by_click == 'both' or woo_ext_search_options.run_search_by_click == 'link' %}*/
/* 							</a>*/
/* 							{% endif %}*/
/* */
/* 						</li>*/
/* 					{% endif %}*/
/* */
/* 				{% endfor %}*/
/* 			</ul>*/
/* */
/* 		{#{% endif %}#}*/
/* */
/* 	</div>*/
/* 	{{ library.header_block_end( "Filter on " ~ attrs_option['attr_name'] ~ " Block End") }}*/
/* */
/* {% endfor %}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
