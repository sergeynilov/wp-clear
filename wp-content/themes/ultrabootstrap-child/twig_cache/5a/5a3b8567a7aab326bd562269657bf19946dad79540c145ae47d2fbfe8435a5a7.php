<?php

/* white-simple/filter_rating_box.twig */
class __TwigTemplate_5fc724e0db63679d5e6ab6aa5c84bf5393786e6dd69c30aaf957a38c1c1f59dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "show_in_extended_search_rating", array()) == "yes")) {
            // line 2
            echo "\t";
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : null), "header_block_start", array(0 => "Filter on rating", 1 => "", 2 => "fa fa-search-plus", 3 => ((isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null) . "block_rating")), "method");
            echo "
\t<div class=\"row nsn_woo_ext_search_block_padding_md\">
\t\t<div class=\"form-group\">
\t\t\t<label for=\"";
            // line 5
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
            echo "slider_rating_range_info\" class=\"col-xs-12 control-label\">Select Rating range from 1 till 5</label>
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<div id=\"slider_rating\"></div>
\t\t\t\t<input type=\"text\" readonly id=\"slider_rating_range_info\" style=\"width: 90%\" >
\t\t\t</div>
\t\t</div>

\t\t<div class=\"form-group\">
\t\t\t<label for=\"";
            // line 13
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
            echo "cbx_include_without_rating\" class=\"col-xs-6 control-label\">Include without rating</label>
\t\t\t<div class=\"col-xs-6 \">
\t\t\t\t<input type=\"checkbox\" value=\"1\" id=\"";
            // line 15
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
            echo "cbx_include_without_rating\" >
\t\t\t</div>
\t\t</div>

\t</div>
\t";
            // line 20
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : null), "header_block_end", array(0 => "Filter on rating Block End"), "method");
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "white-simple/filter_rating_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 20,  44 => 15,  39 => 13,  28 => 5,  21 => 2,  19 => 1,);
    }
}
/* {% if woo_ext_search_options.show_in_extended_search_rating == 'yes' %}*/
/* 	{{ library.header_block_start("Filter on rating", "", "fa fa-search-plus", plugin_prefix ~ "block_rating" ) }}*/
/* 	<div class="row nsn_woo_ext_search_block_padding_md">*/
/* 		<div class="form-group">*/
/* 			<label for="{{ plugin_prefix }}slider_rating_range_info" class="col-xs-12 control-label">Select Rating range from 1 till 5</label>*/
/* 			<div class="col-xs-12">*/
/* 				<div id="slider_rating"></div>*/
/* 				<input type="text" readonly id="slider_rating_range_info" style="width: 90%" >*/
/* 			</div>*/
/* 		</div>*/
/* */
/* 		<div class="form-group">*/
/* 			<label for="{{ plugin_prefix }}cbx_include_without_rating" class="col-xs-6 control-label">Include without rating</label>*/
/* 			<div class="col-xs-6 ">*/
/* 				<input type="checkbox" value="1" id="{{ plugin_prefix }}cbx_include_without_rating" >*/
/* 			</div>*/
/* 		</div>*/
/* */
/* 	</div>*/
/* 	{{ library.header_block_end( "Filter on rating Block End") }}*/
/* {% endif %}*/
