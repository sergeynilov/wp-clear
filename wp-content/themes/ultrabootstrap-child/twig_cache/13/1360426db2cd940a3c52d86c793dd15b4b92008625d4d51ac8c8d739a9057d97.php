<?php

/* white-simple/filter_controls_box.twig */
class __TwigTemplate_ea98be810a09917ba5084a21ae506ba7970e244d5ed1920f15162f27fb15f4e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_start", array(0 => "Filter Controls", 1 => "", 2 => "fa fa-search-plus"), "method");
        echo "


<div class=\"btn-group search_btn_group\" > ";
        // line 5
        echo "

\t<div class=\"row\">
\t\t<div class=\"form-group\">
\t\t\t<label for=\"";
        // line 9
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "cbx_only_in_stock\" class=\"col-xs-12 col-sm-5 control-label\">Only in stock.</label>
\t\t\t<div class=\"col-xs-12 col-sm-7 \">
\t\t\t\t<input type=\"checkbox\" value=\"1\" id=\"";
        // line 11
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "cbx_only_in_stock\">
\t\t\t</div>
\t\t</div>
\t</div>

\t<div class=\"row\">
\t\t<div class=\"form-group\">
\t\t\t<label for=\"";
        // line 18
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "orderby\" class=\"col-xs-12 col-sm-5 control-label\">Order by</label>
\t\t\t<div class=\"col-xs-12 col-sm-7 padding_right_sm padding_left_sm\">
\t\t\t\t<select name=\"";
        // line 20
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "orderby\" id=\"";
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "orderby\" class=\"orderby form-control\">
\t\t\t\t\t<option value=\"menu_order\" selected=\"selected\">Default sorting</option>
\t\t\t\t\t<option value=\"popularity\">By popularity</option>
\t\t\t\t\t<option value=\"rating\">By average rating</option>
\t\t\t\t\t<option value=\"date\">By newness</option>
\t\t\t\t\t<option value=\"price\">By price: low to high</option>
\t\t\t\t\t<option value=\"price-desc\">By price: high to low</option>
\t\t\t\t</select>
\t\t\t</div>
\t\t</div>
\t</div>



\t<div class=\"row  block_padding_sm\">
\t\t<button type=\"button\" class=\"search-submit\" onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.runFilterSearch('loadWooProducts');return false;\" >Search</button>

\t\t<button type=\"button\" class=\"search-submit  pull-right\" onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.clearAllInputs();return false;\" >Clear All Inputs</button>

\t</div>


\t<div class=\"row\">
\t\t<fieldset class=\" thin_border block_padding_xs\">
\t\t\t<legend>Bookmarks:</legend>

\t\t\t<div class=\"block_padding_xs\">
\t\t\t\t<span aria-hidden=\"true\" class=\"glyphicon glyphicon-bookmark\"></span>
\t\t\t\t<span id=\"span_have_bookmarks\"></span>
\t\t\t\t<button id=\"button_show_existing_bookmarks\" onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.showBookmarks();return false;\" class=\"pull-right\">&nbsp;Show</button>
\t\t\t</div>


\t\t\t<div class=\"block_padding_xs\">
\t\t\t\t<span aria-hidden=\"true\" class=\"glyphicon glyphicon-bookmark\"></span>
\t\t\t\t<button onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.showNewBookmarkDialog();return false;\" class=\"pull-right\">&nbsp;Save current filters as Bookmark</button>
\t\t\t</div>

\t\t</fieldset>
\t</div>



</div>

";
        // line 65
        echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_end", array(0 => "Filter Controls Block End"), "method");
    }

    public function getTemplateName()
    {
        return "white-simple/filter_controls_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 65,  51 => 20,  46 => 18,  36 => 11,  31 => 9,  25 => 5,  19 => 1,);
    }
}
/* {{ library.header_block_start("Filter Controls", "", "fa fa-search-plus") }}*/
/* */
/* */
/* <div class="btn-group search_btn_group" > {# bottom_margin_md top_margin_md#}*/
/* */
/* */
/* 	<div class="row">*/
/* 		<div class="form-group">*/
/* 			<label for="{{ plugin_prefix }}cbx_only_in_stock" class="col-xs-12 col-sm-5 control-label">Only in stock.</label>*/
/* 			<div class="col-xs-12 col-sm-7 ">*/
/* 				<input type="checkbox" value="1" id="{{ plugin_prefix }}cbx_only_in_stock">*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* */
/* 	<div class="row">*/
/* 		<div class="form-group">*/
/* 			<label for="{{ plugin_prefix }}orderby" class="col-xs-12 col-sm-5 control-label">Order by</label>*/
/* 			<div class="col-xs-12 col-sm-7 padding_right_sm padding_left_sm">*/
/* 				<select name="{{ plugin_prefix }}orderby" id="{{ plugin_prefix }}orderby" class="orderby form-control">*/
/* 					<option value="menu_order" selected="selected">Default sorting</option>*/
/* 					<option value="popularity">By popularity</option>*/
/* 					<option value="rating">By average rating</option>*/
/* 					<option value="date">By newness</option>*/
/* 					<option value="price">By price: low to high</option>*/
/* 					<option value="price-desc">By price: high to low</option>*/
/* 				</select>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* */
/* */
/* */
/* 	<div class="row  block_padding_sm">*/
/* 		<button type="button" class="search-submit" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.runFilterSearch('loadWooProducts');return false;" >Search</button>*/
/* */
/* 		<button type="button" class="search-submit  pull-right" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.clearAllInputs();return false;" >Clear All Inputs</button>*/
/* */
/* 	</div>*/
/* */
/* */
/* 	<div class="row">*/
/* 		<fieldset class=" thin_border block_padding_xs">*/
/* 			<legend>Bookmarks:</legend>*/
/* */
/* 			<div class="block_padding_xs">*/
/* 				<span aria-hidden="true" class="glyphicon glyphicon-bookmark"></span>*/
/* 				<span id="span_have_bookmarks"></span>*/
/* 				<button id="button_show_existing_bookmarks" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.showBookmarks();return false;" class="pull-right">&nbsp;Show</button>*/
/* 			</div>*/
/* */
/* */
/* 			<div class="block_padding_xs">*/
/* 				<span aria-hidden="true" class="glyphicon glyphicon-bookmark"></span>*/
/* 				<button onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.showNewBookmarkDialog();return false;" class="pull-right">&nbsp;Save current filters as Bookmark</button>*/
/* 			</div>*/
/* */
/* 		</fieldset>*/
/* 	</div>*/
/* */
/* */
/* */
/* </div>*/
/* */
/* {{ library.header_block_end("Filter Controls Block End") }}*/
