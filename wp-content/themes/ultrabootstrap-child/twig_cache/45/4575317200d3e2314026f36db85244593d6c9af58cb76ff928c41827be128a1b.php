<?php

/* white-simple/woo_ext_products_list.twig */
class __TwigTemplate_45c7090ac5903272a9d24309dbb6a7743dbda9b6ab14a8062f567d50a25ae9fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 164
        echo "  ";
        // line 165
        echo "
<h2 class=\"site_content_title margin_top_lg\">
\t<span class=\"fa fa-building-o fa-2x middle_icon\" ></span>&nbsp;Found Products (";
        // line 167
        echo twig_length_filter($this->env, (isset($context["products_list"]) ? $context["products_list"] : $this->getContext($context, "products_list")));
        echo ")
</h2>

";
        // line 173
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products_list"]) ? $context["products_list"] : $this->getContext($context, "products_list")));
        foreach ($context['_seq'] as $context["_key"] => $context["nextProduct"]) {
            // line 174
            echo "\t<div class=\"col-xs-12 col-sm-6 next_woo_product\">
\t\t<ul class=\"products ul_multiline\">

\t\t\t";
            // line 178
            echo "            <span class=\"visible-xs\">
\t            ";
            // line 179
            echo $this->getAttribute($this, "show_next_product_info", array(0 => $context["nextProduct"], 1 => (isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), 2 => (isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : $this->getContext($context, "attrs_to_show")), 3 => (isset($context["service_category_for_attrs_array"]) ? $context["service_category_for_attrs_array"] : $this->getContext($context, "service_category_for_attrs_array")), 4 => "_xs"), "method");
            echo "
\t        </span>
\t        <span class=\"visible-sm\">
\t            ";
            // line 182
            echo $this->getAttribute($this, "show_next_product_info", array(0 => $context["nextProduct"], 1 => (isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), 2 => (isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : $this->getContext($context, "attrs_to_show")), 3 => (isset($context["service_category_for_attrs_array"]) ? $context["service_category_for_attrs_array"] : $this->getContext($context, "service_category_for_attrs_array")), 4 => "_sm"), "method");
            echo "
\t        </span>
\t        <span class=\"visible-md\">
\t            ";
            // line 185
            echo $this->getAttribute($this, "show_next_product_info", array(0 => $context["nextProduct"], 1 => (isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), 2 => (isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : $this->getContext($context, "attrs_to_show")), 3 => (isset($context["service_category_for_attrs_array"]) ? $context["service_category_for_attrs_array"] : $this->getContext($context, "service_category_for_attrs_array")), 4 => "_md"), "method");
            echo "
\t        </span>
\t        <span class=\"visible-lg\">
\t            ";
            // line 188
            echo $this->getAttribute($this, "show_next_product_info", array(0 => $context["nextProduct"], 1 => (isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), 2 => (isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : $this->getContext($context, "attrs_to_show")), 3 => (isset($context["service_category_for_attrs_array"]) ? $context["service_category_for_attrs_array"] : $this->getContext($context, "service_category_for_attrs_array")), 4 => "_md"), "method");
            echo "
\t        </span>

\t\t</ul>

\t</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['nextProduct'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    // line 1
    public function getshow_next_product_info($__nextProduct__ = null, $__fields_to_show__ = null, $__attrs_to_show__ = null, $__service_category_for_attrs_array__ = null, $__field_param__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "nextProduct" => $__nextProduct__,
            "fields_to_show" => $__fields_to_show__,
            "attrs_to_show" => $__attrs_to_show__,
            "service_category_for_attrs_array" => $__service_category_for_attrs_array__,
            "field_param" => $__field_param__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 2
            echo "
\t";
            // line 4
            echo "\t<li class=\"post-";
            echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "id", array(), "array");
            echo " \">
\t";
            // line 5
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "title", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "title", array()))) {
                // line 6
                echo "\t\t";
                // line 7
                echo "\t\t\t";
                // line 8
                echo "\t\t\t\t";
                // line 9
                echo "\t\t\t";
                // line 10
                echo "\t\t\t";
                // line 11
                echo "\t\t";
                // line 12
                echo "\t\t<h4 class=\"row woo_product_next_row \">
\t\t\t";
                // line 13
                if ((($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "purchase_note", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "purchase_note", array())) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "purchase_note", array(), "array") != ""))) {
                    // line 14
                    echo "\t\t\t\t<span class=\"fa fa-info-circle pull-left\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "purchase_note", array(), "array");
                    echo "\"></span>&nbsp;
\t\t\t";
                }
                // line 16
                echo "\t\t\t<a href=\"";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "guid", array(), "array");
                echo "\" class=\"woo_products_title woo_products_field_value a_link\">";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "id", array(), "array");
                echo "->";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "post_title", array(), "array");
                echo "</a>
\t\t</h4>
\t";
            }
            // line 19
            echo "
\t";
            // line 20
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "image", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "image", array()))) {
                // line 21
                echo "\t\t<div class=\"row \">
\t\t\t";
                // line 22
                if ((($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "is_in_stock", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "is_in_stock", array())) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "is_in_stock", array(), "array") == 1))) {
                    // line 23
                    echo "\t\t\t<div class=\"img_banner\"  >
\t\t\t\t<img src=\"";
                    // line 24
                    echo (isset($context["site_url"]) ? $context["site_url"] : $this->getContext($context, "site_url"));
                    echo "/wp-content/plugins/wooExtSearch/images/in_stock.png\">
\t\t\t</div>
\t\t\t";
                }
                // line 27
                echo "
\t\t\t<div class=\"next_hostel_image\" >
\t\t\t\t<a href=\"";
                // line 29
                echo "\">
\t\t\t\t\t<div class=\"image_border\" style=\"background-image: url('";
                // line 30
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "image_url", array(), "array");
                echo "'); width:180px; height:180px;\"   id=\"spotlight_img_hostel_";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "id", array(), "array");
                echo "\"></div>
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>
\t";
            }
            // line 35
            echo "
\t\t";
            // line 37
            echo "\t\t\t";
            // line 38
            echo "\t\t";
            // line 39
            echo "

\t\t";
            // line 42
            echo "\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : $this->getContext($context, "attrs_to_show")));
            foreach ($context['_seq'] as $context["_key"] => $context["next_attr_to_show"]) {
                // line 43
                echo "\t\t\t";
                // line 44
                echo "\t\t\t";
                // line 45
                echo "\t\t\t";
                $context["is_field_used"] = false;
                // line 46
                echo "
\t\t\t";
                // line 47
                if ((($this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array") == "rating") && (($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "rating_average", array()) > 0) || ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "comments_count", array()) > 0)))) {
                    // line 48
                    echo "\t\t\t\t<div class=\"row woo_product_next_row\">
\t\t\t\t\t<img src=\"";
                    // line 49
                    echo (isset($context["site_url"]) ? $context["site_url"] : $this->getContext($context, "site_url"));
                    echo "/wp-content/plugins/wooExtSearch/images/stars/stars";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "rating_average", array());
                    echo ".png\" title=\"Rated ";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "rating_average", array());
                    echo " out of 5\" class=\"pull-left\">&nbsp;
\t\t\t\t\t &nbsp;(<span class=\"rating\">";
                    // line 50
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "comments_count", array());
                    echo "</span> customer reviews)
\t\t\t\t\t";
                    // line 51
                    $context["is_field_used"] = true;
                    // line 52
                    echo "\t\t\t\t</div>
\t\t\t";
                }
                // line 54
                echo "
\t\t\t";
                // line 55
                if ((twig_in_filter($this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), twig_get_array_keys_filter((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")))) && ((isset($context["is_field_used"]) ? $context["is_field_used"] : $this->getContext($context, "is_field_used")) == false))) {
                    // line 56
                    echo "
\t\t\t\t";
                    // line 57
                    if (($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), array(), "array") != ""))) {
                        // line 58
                        echo "\t\t\t\t<div class=\"row woo_product_next_row\">
\t\t\t\t\t<span class=\"woo_products_label\">";
                        // line 59
                        echo twig_title_string_filter($this->env, $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"));
                        echo ":</span>
\t\t\t\t\t<span class=\"woo_products_";
                        // line 60
                        echo $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array");
                        echo " woo_products_field_value\">
\t\t\t\t\t\t";
                        // line 62
                        echo "\t\t\t\t\t\t";
                        $context["service_post_id"] = "";
                        // line 63
                        echo "\t\t\t\t\t\t";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["service_category_for_attrs_array"]) ? $context["service_category_for_attrs_array"] : $this->getContext($context, "service_category_for_attrs_array")));
                        foreach ($context['_seq'] as $context["key_attr"] => $context["next_service_category_for_attr"]) {
                            // line 64
                            echo "\t\t\t\t\t\t\t";
                            if (($this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array") == $context["key_attr"])) {
                                // line 65
                                echo "\t\t\t\t\t\t\t\t";
                                $context["attr_key_name"] = ((("nsn-woo-ext-search-service_category_attr_" . $this->getAttribute($context["next_attr_to_show"], "attr_name", array())) . "_") . call_user_func_array($this->env->getFilter('change_submit_key')->getCallable(), array($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), $this->getAttribute($context["next_attr_to_show"], "attr_name", array()), array(), "array"))));
                                // line 67
                                echo "\t\t\t\t\t\t\t\t";
                                // line 68
                                echo "\t\t\t\t\t\t\t\t";
                                if ($this->getAttribute($context["next_service_category_for_attr"], (isset($context["attr_key_name"]) ? $context["attr_key_name"] : $this->getContext($context, "attr_key_name")), array(), "array", true, true)) {
                                    // line 69
                                    echo "\t\t\t\t\t\t\t\t";
                                    $context["service_post_id"] = $this->getAttribute($context["next_service_category_for_attr"], (isset($context["attr_key_name"]) ? $context["attr_key_name"] : $this->getContext($context, "attr_key_name")), array(), "array");
                                    // line 70
                                    echo "   \t\t\t\t\t\t\t    ";
                                }
                                // line 71
                                echo "
\t\t\t\t\t\t\t\t";
                                // line 73
                                echo "\t\t\t\t\t\t\t";
                            }
                            // line 74
                            echo "\t\t\t\t\t\t";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['key_attr'], $context['next_service_category_for_attr'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 75
                        echo "\t\t\t\t\t\t";
                        // line 76
                        echo "\t\t\t\t\t\t";
                        if ((($this->getAttribute($context["next_attr_to_show"], "show_link_to_post", array(), "array") == "yes") && ((isset($context["service_post_id"]) ? $context["service_post_id"] : $this->getContext($context, "service_post_id")) != ""))) {
                            // line 77
                            echo "\t\t\t\t\t\t<a class=\"a_link\" href=\"";
                            echo call_user_func_array($this->env->getFilter('make_post_url')->getCallable(), array((isset($context["service_post_id"]) ? $context["service_post_id"] : $this->getContext($context, "service_post_id"))));
                            echo "\">";
                            echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), array(), "array");
                            echo "</a>
\t\t\t\t\t\t";
                        } else {
                            // line 79
                            echo "\t\t\t\t\t\t";
                            echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), array(), "array");
                            echo "
\t\t\t\t\t\t";
                        }
                        // line 81
                        echo "\t\t\t\t\t</span>
\t\t\t\t</div>
\t\t\t\t";
                    }
                    // line 84
                    echo "
\t\t\t";
                }
                // line 86
                echo "
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_attr_to_show'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 88
            echo "\t\t";
            // line 89
            echo "

\t";
            // line 91
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "post_date", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "post_date", array()))) {
                // line 92
                echo "\t\t<div class=\"row woo_product_next_row\">
\t\t\t<span class=\"woo_products_label\">";
                // line 93
                echo twig_title_string_filter($this->env, "Publication");
                echo ":</span>
\t\t\t<span class=\"woo_products_post_date woo_products_field_value\">
\t\t\t\t";
                // line 95
                echo call_user_func_array($this->env->getFilter('date_time')->getCallable(), array($this->getAttribute($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "post", array()), "post_date", array())));
                echo "
\t\t\t</span>
\t\t</div>
\t";
            }
            // line 99
            echo "

\t";
            // line 101
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "categories", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "categories", array()))) {
                // line 102
                echo "\t\t<div class=\"row woo_product_next_row\">
\t\t\t<span class=\"woo_products_label\">Categor";
                // line 103
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "categories_list", array(), "array")) > 1)) {
                    echo "ies";
                } else {
                    echo "y";
                }
                echo ":</span>
\t\t\t";
                // line 104
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "categories_list", array(), "array"), "ret_titles", array(), "array"));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["next_category_title"]) {
                    // line 105
                    echo "\t\t\t<span class=\"woo_products_categories woo_products_field_value\">
\t\t\t\t<a href=\"";
                    // line 106
                    echo call_user_func_array($this->env->getFilter('make_woo_product_category_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "categories_list", array(), "array"), "ret_slugs", array(), "array"), $this->getAttribute($context["loop"], "index0", array()), array(), "array")));
                    echo "\" class=\"a_link\">";
                    echo $context["next_category_title"];
                    echo "</a>
\t\t\t\t";
                    // line 107
                    if ( !$this->getAttribute($context["loop"], "last", array())) {
                        echo ", ";
                    }
                    // line 108
                    echo "\t\t\t</span>
\t\t\t";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_category_title'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 110
                echo "\t\t</div>
\t";
            }
            // line 112
            echo "

\t";
            // line 114
            if ((($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sale_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "sale_price", array())) || ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "regular_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "regular_price", array())))) {
                // line 115
                echo "\t\t<div class=\"row woo_product_next_row\">
\t\t\t<span class=\"woo_products_label\">Price:</span>

\t\t\t<span class=\"woo_products_sale_price woo_products_field_value\">
\t\t\t\t";
                // line 119
                if ((($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "regular_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "regular_price", array(), "array") > 0)) && ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "regular_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "regular_price", array())))) {
                    // line 120
                    echo "\t\t\t\t\t";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "formatted_regular_price", array(), "array");
                    echo "
\t\t\t\t";
                }
                // line 122
                echo "
\t\t\t\t";
                // line 123
                if ((((($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "regular_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "regular_price", array(), "array") > 0)) && ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "regular_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "regular_price", array()))) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "sale_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "sale_price", array(), "array") > 0))) && ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sale_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "sale_price", array())))) {
                    // line 124
                    echo "\t\t\t\t&nbsp;-&nbsp;
\t\t\t\t";
                }
                // line 126
                echo "
\t\t\t\t";
                // line 127
                if ((($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "sale_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "sale_price", array(), "array") > 0)) && ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sale_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "sale_price", array())))) {
                    // line 128
                    echo "\t\t\t\t\t";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "formatted_sale_price", array(), "array");
                    echo "
\t\t\t\t";
                }
                // line 130
                echo "\t\t\t</span>

\t\t</div>
\t";
            }
            // line 134
            echo "

\t\t";
            // line 136
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "is_in_stock", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "is_in_stock", array()))) {
                // line 137
                echo "\t\t\t<div class=\"row woo_product_next_row\">
\t\t\t\t<span class=\"woo_products_label\">In Stock:</span>
\t\t\t<span class=\"woo_products_is_in_stock woo_products_field_value\">
\t\t\t\t";
                // line 140
                if ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "is_in_stock", array(), "array")) {
                    echo "Yes";
                } else {
                    echo "No";
                }
                // line 141
                echo "\t\t\t</span>
\t\t\t</div>
\t\t";
            }
            // line 144
            echo "
\t</li>


\t";
            // line 149
            echo "\t\t";
            // line 150
            echo "\t\t\t";
            // line 151
            echo "\t\t";
            // line 152
            echo "\t";
            // line 153
            echo "
\t";
            // line 155
            echo "\t\t";
            // line 156
            echo "\t";
            // line 157
            echo "
\t";
            // line 159
            echo "\t\t";
            // line 160
            echo "\t\t\t";
            // line 161
            echo "\t\t";
            // line 162
            echo "\t";
            // line 163
            echo "
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "white-simple/woo_ext_products_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  503 => 163,  501 => 162,  499 => 161,  497 => 160,  495 => 159,  492 => 157,  490 => 156,  488 => 155,  485 => 153,  483 => 152,  481 => 151,  479 => 150,  477 => 149,  471 => 144,  466 => 141,  460 => 140,  455 => 137,  453 => 136,  449 => 134,  443 => 130,  437 => 128,  435 => 127,  432 => 126,  428 => 124,  426 => 123,  423 => 122,  417 => 120,  415 => 119,  409 => 115,  407 => 114,  403 => 112,  399 => 110,  384 => 108,  380 => 107,  374 => 106,  371 => 105,  354 => 104,  346 => 103,  343 => 102,  341 => 101,  337 => 99,  330 => 95,  325 => 93,  322 => 92,  320 => 91,  316 => 89,  314 => 88,  307 => 86,  303 => 84,  298 => 81,  292 => 79,  284 => 77,  281 => 76,  279 => 75,  273 => 74,  270 => 73,  267 => 71,  264 => 70,  261 => 69,  258 => 68,  256 => 67,  253 => 65,  250 => 64,  245 => 63,  242 => 62,  238 => 60,  234 => 59,  231 => 58,  229 => 57,  226 => 56,  224 => 55,  221 => 54,  217 => 52,  215 => 51,  211 => 50,  203 => 49,  200 => 48,  198 => 47,  195 => 46,  192 => 45,  190 => 44,  188 => 43,  183 => 42,  179 => 39,  177 => 38,  175 => 37,  172 => 35,  162 => 30,  159 => 29,  155 => 27,  149 => 24,  146 => 23,  144 => 22,  141 => 21,  139 => 20,  136 => 19,  125 => 16,  119 => 14,  117 => 13,  114 => 12,  112 => 11,  110 => 10,  108 => 9,  106 => 8,  104 => 7,  102 => 6,  100 => 5,  95 => 4,  92 => 2,  76 => 1,  61 => 188,  55 => 185,  49 => 182,  43 => 179,  40 => 178,  35 => 174,  31 => 173,  25 => 167,  21 => 165,  19 => 164,);
    }
}
/* {% macro show_next_product_info( nextProduct, fields_to_show, attrs_to_show, service_category_for_attrs_array, field_param ) %}*/
/* */
/* 	{#nextProduct::{{ nextProduct | d }}#}*/
/* 	<li class="post-{{ nextProduct['id'] }} ">*/
/* 	{% if fields_to_show.title is defined and fields_to_show.title %}*/
/* 		{#<h3 class="row woo_product_next_row ">#}*/
/* 			{#{% if fields_to_show.image is defined and fields_to_show.image  %}#}*/
/* 				{#&#123;&#35; data-toggle="tooltip" data-placement="top" title="{{ nextProduct['purchase_note'] }}" &#35;&#125;#}*/
/* 			{#{% endif %}#}*/
/* 			{#{{ nextProduct['post_title'] }}#}*/
/* 		{#</h3>#}*/
/* 		<h4 class="row woo_product_next_row ">*/
/* 			{% if fields_to_show.purchase_note is defined and fields_to_show.purchase_note and nextProduct['purchase_note'] != "" %}*/
/* 				<span class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="top" title="{{ nextProduct['purchase_note'] }}"></span>&nbsp;*/
/* 			{% endif %}*/
/* 			<a href="{{ nextProduct['guid'] }}" class="woo_products_title woo_products_field_value a_link">{{ nextProduct['id'] }}->{{ nextProduct['post_title'] }}</a>*/
/* 		</h4>*/
/* 	{% endif %}*/
/* */
/* 	{% if fields_to_show.image is defined and fields_to_show.image  %}*/
/* 		<div class="row ">*/
/* 			{% if fields_to_show.is_in_stock is defined and fields_to_show.is_in_stock and nextProduct['is_in_stock'] == 1 %}*/
/* 			<div class="img_banner"  >*/
/* 				<img src="{{ site_url }}/wp-content/plugins/wooExtSearch/images/in_stock.png">*/
/* 			</div>*/
/* 			{% endif %}*/
/* */
/* 			<div class="next_hostel_image" >*/
/* 				<a href="{#{ nextProduct['guid'] }#}">*/
/* 					<div class="image_border" style="background-image: url('{{ nextProduct['image_url'] }}'); width:180px; height:180px;"   id="spotlight_img_hostel_{{ nextProduct['id'] }}"></div>*/
/* 				</a>*/
/* 			</div>*/
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* 		{#<div class="star-rating" title="Rated 5 out of 5">#}*/
/* 			{#<span style="width:100%"><strong class="rating">5</strong> out of 5</span>#}*/
/* 		{#</div>#}*/
/* */
/* */
/* 		{#==================== START ==============#}*/
/* 		{% for next_attr_to_show in attrs_to_show %}*/
/* 			{#<b>next_attr_to_show</b>{{ next_attr_to_show | d }}#}*/
/* 			{#<b>next_attr_to_show</b>{{ next_attr_to_show['attr_name'] | d }}#}*/
/* 			{% set is_field_used = false %}*/
/* */
/* 			{% if next_attr_to_show['attr_name'] == "rating" and ( nextProduct.rating_average > 0 or nextProduct.comments_count > 0 ) %}*/
/* 				<div class="row woo_product_next_row">*/
/* 					<img src="{{ site_url }}/wp-content/plugins/wooExtSearch/images/stars/stars{{ nextProduct.rating_average }}.png" title="Rated {{ nextProduct.rating_average }} out of 5" class="pull-left">&nbsp;*/
/* 					 &nbsp;(<span class="rating">{{ nextProduct.comments_count }}</span> customer reviews)*/
/* 					{% set is_field_used = true %}*/
/* 				</div>*/
/* 			{% endif %}*/
/* */
/* 			{% if next_attr_to_show['attr_name'] in fields_to_show | keys and is_field_used == false %}*/
/* */
/* 				{% if nextProduct[next_attr_to_show['attr_name']]  is defined and nextProduct[next_attr_to_show['attr_name']]!= "" %}*/
/* 				<div class="row woo_product_next_row">*/
/* 					<span class="woo_products_label">{{ next_attr_to_show['attr_name'] | title }}:</span>*/
/* 					<span class="woo_products_{{ next_attr_to_show['attr_name'] }} woo_products_field_value">*/
/* 						{#next_attr_to_show]['attr_name']::{{ next_attr_to_show['attr_name'] }}#}*/
/* 						{% set service_post_id='' %}*/
/* 						{% for key_attr, next_service_category_for_attr in service_category_for_attrs_array %}*/
/* 							{% if next_attr_to_show['attr_name'] == key_attr %}*/
/* 								{% set attr_key_name = 'nsn-woo-ext-search-service_category_attr_' ~ next_attr_to_show.attr_name ~ '_' ~ ( nextProduct[next_attr_to_show.attr_name] | change_submit_key )*/
/* 								%}*/
/* 								{#attr_key_name::{{ attr_key_name }}#}*/
/* 								{% if next_service_category_for_attr[attr_key_name] is defined %}*/
/* 								{% set service_post_id= next_service_category_for_attr[attr_key_name] %}*/
/*    							    {% endif %}*/
/* */
/* 								{# [nsn-woo-ext-search-service_category_attr_brand_A4Tech] => 119 #}*/
/* 							{% endif %}*/
/* 						{% endfor %}*/
/* 						{#service_post_id::{{ service_post_id }}<br>#}*/
/* 						{% if next_attr_to_show['show_link_to_post'] == 'yes' and service_post_id != ""%}*/
/* 						<a class="a_link" href="{{ service_post_id | make_post_url }}">{{ nextProduct[ next_attr_to_show['attr_name'] ] }}</a>*/
/* 						{% else %}*/
/* 						{{ nextProduct[ next_attr_to_show['attr_name'] ] }}*/
/* 						{% endif %}*/
/* 					</span>*/
/* 				</div>*/
/* 				{% endif %}*/
/* */
/* 			{% endif %}*/
/* */
/* 		{% endfor %}*/
/* 		{#==================== END ==============#}*/
/* */
/* */
/* 	{% if fields_to_show.post_date is defined and fields_to_show.post_date %}*/
/* 		<div class="row woo_product_next_row">*/
/* 			<span class="woo_products_label">{{ "Publication" | title }}:</span>*/
/* 			<span class="woo_products_post_date woo_products_field_value">*/
/* 				{{ nextProduct.post.post_date | date_time }}*/
/* 			</span>*/
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* */
/* 	{% if fields_to_show.categories is defined and fields_to_show.categories %}*/
/* 		<div class="row woo_product_next_row">*/
/* 			<span class="woo_products_label">Categor{% if nextProduct['categories_list'] | length > 1 %}ies{% else %}y{% endif %}:</span>*/
/* 			{% for next_category_title in nextProduct['categories_list']['ret_titles'] %}*/
/* 			<span class="woo_products_categories woo_products_field_value">*/
/* 				<a href="{{ nextProduct['categories_list']['ret_slugs'][loop.index0] | make_woo_product_category_url }}" class="a_link">{{ next_category_title }}</a>*/
/* 				{% if not loop.last %}, {% endif %}*/
/* 			</span>*/
/* 			{% endfor %}*/
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* */
/* 	{% if ( fields_to_show.sale_price is defined and fields_to_show.sale_price ) or ( fields_to_show.regular_price is defined and fields_to_show.regular_price ) %}*/
/* 		<div class="row woo_product_next_row">*/
/* 			<span class="woo_products_label">Price:</span>*/
/* */
/* 			<span class="woo_products_sale_price woo_products_field_value">*/
/* 				{% if ( nextProduct['regular_price'] is defined and nextProduct['regular_price'] > 0 ) and ( fields_to_show.regular_price is defined and fields_to_show.regular_price ) %}*/
/* 					{{ nextProduct['formatted_regular_price'] }}*/
/* 				{% endif %}*/
/* */
/* 				{% if ( nextProduct['regular_price'] is defined and nextProduct['regular_price'] > 0 ) and ( fields_to_show.regular_price is defined and fields_to_show.regular_price ) and ( nextProduct['sale_price'] is defined and nextProduct['sale_price'] >0 ) and ( fields_to_show.sale_price is defined and fields_to_show.sale_price ) %}*/
/* 				&nbsp;-&nbsp;*/
/* 				{% endif %}*/
/* */
/* 				{% if ( nextProduct['sale_price'] is defined and nextProduct['sale_price'] > 0 ) and ( fields_to_show.sale_price is defined and fields_to_show.sale_price )%}*/
/* 					{{ nextProduct['formatted_sale_price'] }}*/
/* 				{% endif %}*/
/* 			</span>*/
/* */
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* */
/* 		{% if fields_to_show.is_in_stock is defined and fields_to_show.is_in_stock  %}*/
/* 			<div class="row woo_product_next_row">*/
/* 				<span class="woo_products_label">In Stock:</span>*/
/* 			<span class="woo_products_is_in_stock woo_products_field_value">*/
/* 				{% if nextProduct['is_in_stock'] %}Yes{% else %}No{% endif %}*/
/* 			</span>*/
/* 			</div>*/
/* 		{% endif %}*/
/* */
/* 	</li>*/
/* */
/* */
/* 	{#<div class="row next_hostel_descr" >#}*/
/* 		{#<h5 class="site_content_text" class="debug" >#}*/
/* 			{#{{ nextProduct.short_descr | concat_str( 100 ) |raw }}#}*/
/* 		{#</h5>#}*/
/* 	{#</div>#}*/
/* */
/* 	{#&#123;&#35;<div class="row next_hostel_stars" >&#35;&#125;#}*/
/* 		{#&#123;&#35;<img src="{{ base_url }}{{ images_dir }}/{{ current_skin_name }}/stars/stars{{ nextProduct.reviews_avg_rating }}.png"  alt="{{ nextProduct.reviews_avg_rating | show_hostel_review_stars_rating_type_label_label }}" title="{{ nextProduct.reviews_avg_rating | show_hostel_review_stars_rating_type_label_label }}" class="pull-left"><span class="pull-right">{{ nextProduct.price | show_formatted_price( "AU$", "text_money" ) | raw }}</span>&#35;&#125;#}*/
/* 	{#&#123;&#35;</div>&#35;&#125;#}*/
/* */
/* 	{#<div class="row next_hostel_btn" >#}*/
/* 		{#<a href="{{ base_url }}{{ nextProduct.name | make_woo_product_url(nextProduct) | raw }}">#}*/
/* 			{#<button class="item_details">View Details</button>#}*/
/* 		{#</a>#}*/
/* 	{#</div>#}*/
/* */
/* {% endmacro %}  {# show_next_product_info END #}*/
/* */
/* <h2 class="site_content_title margin_top_lg">*/
/* 	<span class="fa fa-building-o fa-2x middle_icon" ></span>&nbsp;Found Products ({{ products_list | length }})*/
/* </h2>*/
/* */
/* {#attrs_to_show::{{ attrs_to_show | d }}#}*/
/* {#fields_to_show::{{ fields_to_show | d }}#}*/
/* {#service_category_for_attrs_array::{{ service_category_for_attrs_array | d }}#}*/
/* {% for nextProduct in products_list %}*/
/* 	<div class="col-xs-12 col-sm-6 next_woo_product">*/
/* 		<ul class="products ul_multiline">*/
/* */
/* 			{#nextProduct::{{ nextProduct | d }}#}*/
/*             <span class="visible-xs">*/
/* 	            {{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show, service_category_for_attrs_array, "_xs" ) }}*/
/* 	        </span>*/
/* 	        <span class="visible-sm">*/
/* 	            {{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show, service_category_for_attrs_array, "_sm" ) }}*/
/* 	        </span>*/
/* 	        <span class="visible-md">*/
/* 	            {{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show, service_category_for_attrs_array, "_md" ) }}*/
/* 	        </span>*/
/* 	        <span class="visible-lg">*/
/* 	            {{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show, service_category_for_attrs_array, "_md" ) }}*/
/* 	        </span>*/
/* */
/* 		</ul>*/
/* */
/* 	</div>*/
/* {% endfor %}*/
