<?php

/* white-simple/filter_attrs_box.twig */
class __TwigTemplate_6fb06877f5b2ad294d40c726c9e4a4b29e34ff2fd9a565df89478051c44e0c5d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attrs_options_list"]) ? $context["attrs_options_list"] : $this->getContext($context, "attrs_options_list")));
        foreach ($context['_seq'] as $context["_key"] => $context["attrs_option"]) {
            // line 2
            echo "
\t";
            // line 3
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_start", array(0 => ("Filter on " . $this->getAttribute($context["attrs_option"], "attr_name", array(), "array")), 1 => "", 2 => "fa fa-search-plus"), "method");
            echo "
\t<div class=\"row\">
\t\t";
            // line 5
            $context["is_block_used"] = false;
            // line 6
            echo "\t\t";
            if (($this->getAttribute($context["attrs_option"], "attr_name", array(), "array") == "rating")) {
                // line 7
                echo "
\t\t\t<div class=\"block_padding_sm\">
\t\t\t\tSelect Rating range from 1 till 5:<br>
\t\t\t\tAA<div  id=\"div_";
                // line 10
                echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
                echo "rating_slider\"></div>BB
\t\t\t\t";
                // line 12
                echo "\t\t\t\t";
                // line 13
                echo "\t\t\t\t";
                // line 14
                echo "\t\t\t</div>
\t\t\t";
                // line 15
                $context["is_block_used"] = true;
                // line 16
                echo "
\t\t";
            }
            // line 18
            echo "
\t\t";
            // line 19
            if (($this->getAttribute($context["attrs_option"], "attr_name", array(), "array") == "sku")) {
                // line 20
                echo "
\t\t\t<div class=\"btn-group search_btn_group\" >

\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"";
                // line 25
                echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
                echo "cbx_partial_search\" class=\"col-xs-12 col-sm-5 control-label\">Partial search</label>
\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-7 \">
\t\t\t\t\t\t\t<input type=\"checkbox\" value=\"1\" id=\"";
                // line 27
                echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
                echo "cbx_partial_search\" class=\" \" checked>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"";
                // line 34
                echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
                echo "input_sku\" class=\"col-xs-12 col-sm-5 control-label\">Enter sku</label>
\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-7 padding_right_sm padding_left_sm\">
\t\t\t\t\t\t\t<input type=\"text\" value=\"ZZZX\" id=\"";
                // line 36
                echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
                echo "input_sku\" class=\"form-control\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t</div>
\t\t\t";
                // line 42
                $context["is_block_used"] = true;
                // line 43
                echo "
\t\t";
            }
            // line 45
            echo "
\t\t";
            // line 46
            if (((isset($context["is_block_used"]) ? $context["is_block_used"] : $this->getContext($context, "is_block_used")) != true)) {
                // line 47
                echo "\t\t\t";
                $context["attrs_list"] = $this->getAttribute($context["attrs_option"], "attrs_list", array(), "array");
                // line 48
                echo "\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["attrs_list"]) ? $context["attrs_list"] : $this->getContext($context, "attrs_list")));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["next_attr"]) {
                    // line 49
                    echo "\t\t\t\t<input type=\"checkbox\" id=\"";
                    echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
                    echo $this->getAttribute($context["attrs_option"], "attr_name", array(), "array");
                    echo "_next_attr_";
                    echo $this->getAttribute($context["next_attr"], "name", array());
                    echo "\" value=\"";
                    echo $this->getAttribute($context["next_attr"], "name", array());
                    echo "\" class=\"";
                    echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
                    echo "cbx_";
                    echo $this->getAttribute($context["attrs_option"], "attr_name", array(), "array");
                    echo "_selection_filters\" ";
                    if ($this->getAttribute($context["loop"], "last", array())) {
                        echo "checked ";
                    }
                    echo ">&nbsp;";
                    echo $this->getAttribute($context["next_attr"], "name", array());
                    echo " <b>(";
                    if ($this->getAttribute($context["next_attr"], "products_count", array(), "any", true, true)) {
                        echo $this->getAttribute($context["next_attr"], "products_count", array());
                    } else {
                        echo "0";
                    }
                    echo ")</b><br>
\t\t\t";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_attr'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 51
                echo "\t\t";
            }
            // line 52
            echo "\t</div>
\t";
            // line 53
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_end", array(0 => (("Filter on " . $this->getAttribute($context["attrs_option"], "attr_name", array(), "array")) . " Block End")), "method");
            echo "

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attrs_option'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "





";
    }

    public function getTemplateName()
    {
        return "white-simple/filter_attrs_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  184 => 56,  175 => 53,  172 => 52,  169 => 51,  131 => 49,  113 => 48,  110 => 47,  108 => 46,  105 => 45,  101 => 43,  99 => 42,  90 => 36,  85 => 34,  75 => 27,  70 => 25,  63 => 20,  61 => 19,  58 => 18,  54 => 16,  52 => 15,  49 => 14,  47 => 13,  45 => 12,  41 => 10,  36 => 7,  33 => 6,  31 => 5,  26 => 3,  23 => 2,  19 => 1,);
    }
}
/* {% for attrs_option in attrs_options_list %}*/
/* */
/* 	{{ library.header_block_start("Filter on " ~ attrs_option['attr_name'], "", "fa fa-search-plus") }}*/
/* 	<div class="row">*/
/* 		{% set is_block_used = false %}*/
/* 		{% if attrs_option['attr_name'] == 'rating' %}*/
/* */
/* 			<div class="block_padding_sm">*/
/* 				Select Rating range from 1 till 5:<br>*/
/* 				AA<div  id="div_{{ plugin_prefix }}rating_slider"></div>BB*/
/* 				{#<input id="{{ plugin_prefix }}rating_slider" type="text" class="span2" value="" data-slider-min="1" data-slider-max="5" data-slider-step="1" data-slider-value="[1,5]" style="width: 100%" data-slider-ticks="[1, 2, 3, 4, 5]" data-slider-ticks-labels='["1", "2", "3", "4", "5"]'/>#}*/
/* 				{#<button onclick="javascript:testValue1()">test Value 1</button><br>#}*/
/* 				{#<button onclick="javascript:testValue2()">test Value 2</button><br>#}*/
/* 			</div>*/
/* 			{% set is_block_used = true %}*/
/* */
/* 		{% endif %}*/
/* */
/* 		{% if attrs_option['attr_name'] == 'sku' %}*/
/* */
/* 			<div class="btn-group search_btn_group" >*/
/* */
/* 				<div class="row">*/
/* 					<div class="form-group">*/
/* 						<label for="{{ plugin_prefix }}cbx_partial_search" class="col-xs-12 col-sm-5 control-label">Partial search</label>*/
/* 						<div class="col-xs-12 col-sm-7 ">*/
/* 							<input type="checkbox" value="1" id="{{ plugin_prefix }}cbx_partial_search" class=" " checked>*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* */
/* 				<div class="row">*/
/* 					<div class="form-group">*/
/* 						<label for="{{ plugin_prefix }}input_sku" class="col-xs-12 col-sm-5 control-label">Enter sku</label>*/
/* 						<div class="col-xs-12 col-sm-7 padding_right_sm padding_left_sm">*/
/* 							<input type="text" value="ZZZX" id="{{ plugin_prefix }}input_sku" class="form-control">*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* */
/* 			</div>*/
/* 			{% set is_block_used = true %}*/
/* */
/* 		{% endif %}*/
/* */
/* 		{% if is_block_used != true %}*/
/* 			{% set attrs_list= attrs_option['attrs_list'] %}*/
/* 			{% for next_attr in attrs_list %}*/
/* 				<input type="checkbox" id="{{ plugin_prefix }}{{ attrs_option['attr_name'] }}_next_attr_{{ next_attr.name }}" value="{{ next_attr.name }}" class="{{ plugin_prefix }}cbx_{{ attrs_option['attr_name'] }}_selection_filters" {% if loop.last %}checked {%  endif %}>&nbsp;{{ next_attr.name }} <b>({% if next_attr.products_count is defined %}{{ next_attr.products_count }}{% else %}0{% endif %})</b><br>*/
/* 			{% endfor %}*/
/* 		{% endif %}*/
/* 	</div>*/
/* 	{{ library.header_block_end( "Filter on " ~ attrs_option['attr_name'] ~ " Block End") }}*/
/* */
/* {% endfor %}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
