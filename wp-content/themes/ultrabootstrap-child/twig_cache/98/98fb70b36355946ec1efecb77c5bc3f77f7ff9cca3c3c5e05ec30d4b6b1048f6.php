<?php

/* white-simple/existing_bookmarks_dialog.twig */
class __TwigTemplate_fe8f9d86d0ca5611cb138fa77dfcd2059cc5865a787ea1c8a0670bdd48336c78 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"btn-group  pull-right nsn_woo_ext_search_editor_btn_group \" role=\"group\" aria-label=\"group button\"><div class=\"modal fade\" id=\"";
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "show_existing_bookmarks_dialog\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalLabel\" aria-hidden=\"true\">
\t\t<div class=\"nsn_woo_ext_search_modal-dialog\">
\t\t\t<div class=\"nsn_woo_ext_search_modal-content\">
\t\t\t\t<section class=\"modal-header\">
\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">×</span><span class=\"sr-only\">Close</span></button>
\t\t\t\t\t<div class=\"nsn_woo_ext_search_modal-title\">Select Existing Bookmark Dialog </div>
\t\t\t\t</section>

\t\t\t\t<section class=\"nsn_woo_ext_search_modal-body nsn_woo_ext_search_block_padding_lg\">
\t\t\t\t\t<form role=\"form\" class=\"form-horizontal\" >

\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"col-xs-12 col-sm-offset-1 col-sm-3 control-label \" for=\"";
        // line 14
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "show_existing_select_bookmark\">Select existing
\t\t\t\t\t\t\t\t\tbookmark</label>
\t\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-6 col-sm-offset-right-2\">
\t\t\t\t\t\t\t\t\t<select name=\"";
        // line 17
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "show_existing_select_bookmark\" id=\"";
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "show_existing_select_bookmark\" class=\"form-control editable_field\" onchange=\"javascript:nsn_woo_ext_search_frontendFuncsObj.onChange_show_existing_select_bookmark(this);return false;\" >
\t\t\t\t\t\t\t\t\t\t<option value=\"\"> -Select- </option>
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t<small> Selecting bookmark all filter controls will be filled with values of selected bookmark. </small>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"col-xs-12 col-sm-offset-1 col-sm-3 control-label \" for=\"";
        // line 28
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "run_filter_search\">Run Filter at \"Select\" clicked</label>
\t\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-6 col-sm-offset-right-2\">
\t\t\t\t\t\t\t\t\t<input id=\"";
        // line 30
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "run_filter_search\" type=\"checkbox\" value=\"1\" class=\"form-control\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</form>
\t\t\t\t</section>

\t\t\t\t<section class=\"modal-footer\">
\t\t\t\t\t<div class=\"btn-group  pull-right nsn_woo_ext_search_editor_btn_group \" role=\"group\" aria-label=\"group button\">
\t\t\t\t\t\t<button id=\"button_";
        // line 40
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "remove_selected_bookmark\" onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.removeBookmarks();return false;\" class=\" btn btn-primary\" id=\"button_";
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "remove_selected_bookmark\" style=\"display: none\" role=\"button\" >Remove</button>&nbsp;&nbsp;
\t\t\t\t\t\t<button type=\"button\" id=\"saveImage\" class=\"btn btn-primary\" onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.fillBookmarkInControls();return false;\" role=\"button\">Select</button>
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary nsn_woo_ext_search_btn-cancel-action\" data-dismiss=\"modal\"  role=\"button\">Cancel</button>
\t\t\t\t\t</div>
\t\t\t\t</section>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "white-simple/existing_bookmarks_dialog.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 40,  63 => 30,  58 => 28,  42 => 17,  36 => 14,  19 => 1,);
    }
}
/* <div class="btn-group  pull-right nsn_woo_ext_search_editor_btn_group " role="group" aria-label="group button"><div class="modal fade" id="{{ plugin_prefix }}show_existing_bookmarks_dialog" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">*/
/* 		<div class="nsn_woo_ext_search_modal-dialog">*/
/* 			<div class="nsn_woo_ext_search_modal-content">*/
/* 				<section class="modal-header">*/
/* 					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>*/
/* 					<div class="nsn_woo_ext_search_modal-title">Select Existing Bookmark Dialog </div>*/
/* 				</section>*/
/* */
/* 				<section class="nsn_woo_ext_search_modal-body nsn_woo_ext_search_block_padding_lg">*/
/* 					<form role="form" class="form-horizontal" >*/
/* */
/* 						<div class="row">*/
/* 							<div class="form-group">*/
/* 								<label class="col-xs-12 col-sm-offset-1 col-sm-3 control-label " for="{{ plugin_prefix }}show_existing_select_bookmark">Select existing*/
/* 									bookmark</label>*/
/* 								<div class="col-xs-12 col-sm-6 col-sm-offset-right-2">*/
/* 									<select name="{{ plugin_prefix }}show_existing_select_bookmark" id="{{ plugin_prefix }}show_existing_select_bookmark" class="form-control editable_field" onchange="javascript:nsn_woo_ext_search_frontendFuncsObj.onChange_show_existing_select_bookmark(this);return false;" >*/
/* 										<option value=""> -Select- </option>*/
/* 									</select>*/
/* 									<small> Selecting bookmark all filter controls will be filled with values of selected bookmark. </small>*/
/* 								</div>*/
/* */
/* 							</div>*/
/* 						</div>*/
/* */
/* 						<div class="row">*/
/* 							<div class="form-group">*/
/* 								<label class="col-xs-12 col-sm-offset-1 col-sm-3 control-label " for="{{ plugin_prefix }}run_filter_search">Run Filter at "Select" clicked</label>*/
/* 								<div class="col-xs-12 col-sm-6 col-sm-offset-right-2">*/
/* 									<input id="{{ plugin_prefix }}run_filter_search" type="checkbox" value="1" class="form-control">*/
/* 								</div>*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 					</form>*/
/* 				</section>*/
/* */
/* 				<section class="modal-footer">*/
/* 					<div class="btn-group  pull-right nsn_woo_ext_search_editor_btn_group " role="group" aria-label="group button">*/
/* 						<button id="button_{{ plugin_prefix }}remove_selected_bookmark" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.removeBookmarks();return false;" class=" btn btn-primary" id="button_{{ plugin_prefix }}remove_selected_bookmark" style="display: none" role="button" >Remove</button>&nbsp;&nbsp;*/
/* 						<button type="button" id="saveImage" class="btn btn-primary" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.fillBookmarkInControls();return false;" role="button">Select</button>*/
/* 						<button type="button" class="btn btn-primary nsn_woo_ext_search_btn-cancel-action" data-dismiss="modal"  role="button">Cancel</button>*/
/* 					</div>*/
/* 				</section>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* </div>*/
