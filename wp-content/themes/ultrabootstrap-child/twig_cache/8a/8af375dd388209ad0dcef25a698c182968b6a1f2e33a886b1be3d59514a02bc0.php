<?php

/* white-simple/filter_price_box.twig */
class __TwigTemplate_5fcf189f5762607100b896984fcda6ec43cd6bdeed9e04260d25cbde71013a3b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "show_in_extended_search_prices_box", array()) == "yes")) {
            // line 2
            echo "\t";
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_start", array(0 => "Prices Filter", 1 => "", 2 => "fa fa-search-plus", 3 => ((isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix")) . "block_prices")), "method");
            echo "
\t<div class=\"row\">
\t\t<ul class=\"nsn_woo_ext_search_ul_multiline left\" >

\t\t";
            // line 6
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["prices_list"]) ? $context["prices_list"] : $this->getContext($context, "prices_list")));
            foreach ($context['_seq'] as $context["_key"] => $context["next_price"]) {
                // line 7
                echo "
\t\t\t";
                // line 8
                if ( !(($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "show_in_extended_search_hide_with_zero_products", array()) == "yes") &&  !$this->getAttribute($context["next_price"], "products_count", array(), "any", true, true))) {
                    // line 9
                    echo "\t\t\t<li>
\t\t\t\t<input type=\"checkbox\" id=\"";
                    // line 10
                    echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
                    echo "next_price_";
                    echo call_user_func_array($this->env->getFilter('float_floor')->getCallable(), array($this->getAttribute($context["next_price"], "max", array())));
                    echo "\" value=\"";
                    echo $this->getAttribute($context["next_price"], "max", array());
                    echo "\" class=\"";
                    echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
                    echo "cbx_price_selection_filters\" >



\t\t\t\t";
                    // line 14
                    if ((($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "run_search_by_click", array()) == "both") || ($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "run_search_by_click", array()) == "link"))) {
                        // line 15
                        echo "\t\t\t\t<a class=\"nsn_woo_ext_search_a_link\" onclick=\"javascript:  nsn_woo_ext_search_frontendFuncsObj.filter_elementClicked( 'price', '', '";
                        echo $this->getAttribute($context["next_price"], "max", array());
                        echo "' );\treturn false;\t\t\">
\t\t\t\t\t";
                    }
                    // line 17
                    echo "\t\t\t\t&nbsp;";
                    echo (isset($context["currency_symbol"]) ? $context["currency_symbol"] : $this->getContext($context, "currency_symbol"));
                    echo $this->getAttribute($context["next_price"], "min", array());
                    echo "&nbsp;-&nbsp;";
                    echo $this->getAttribute($context["next_price"], "max", array());
                    echo "
\t\t\t\t";
                    // line 18
                    if ($this->getAttribute($context["next_price"], "products_count", array(), "any", true, true)) {
                        echo "<b>(";
                        echo $this->getAttribute($context["next_price"], "products_count", array());
                        echo ")</b>";
                    } else {
                    }
                    // line 19
                    echo "\t\t\t\t\t";
                    if ((($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "run_search_by_click", array()) == "both") || ($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "run_search_by_click", array()) == "link"))) {
                        // line 20
                        echo "\t\t\t\t</a>
\t\t\t\t";
                    }
                    // line 22
                    echo "

\t\t\t<li>
\t\t\t";
                }
                // line 26
                echo "
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_price'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "
\t\t</ul>
\t</div>
\t";
            // line 31
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_end", array(0 => "Prices Filter Block End"), "method");
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "white-simple/filter_price_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 31,  97 => 28,  90 => 26,  84 => 22,  80 => 20,  77 => 19,  70 => 18,  62 => 17,  56 => 15,  54 => 14,  41 => 10,  38 => 9,  36 => 8,  33 => 7,  29 => 6,  21 => 2,  19 => 1,);
    }
}
/* {% if woo_ext_search_options.show_in_extended_search_prices_box == 'yes' %}*/
/* 	{{ library.header_block_start("Prices Filter", "", "fa fa-search-plus", plugin_prefix ~ "block_prices") }}*/
/* 	<div class="row">*/
/* 		<ul class="nsn_woo_ext_search_ul_multiline left" >*/
/* */
/* 		{% for next_price in prices_list %}*/
/* */
/* 			{% if not ( woo_ext_search_options.show_in_extended_search_hide_with_zero_products == "yes" and next_price.products_count is not defined ) %}*/
/* 			<li>*/
/* 				<input type="checkbox" id="{{ plugin_prefix }}next_price_{{ next_price.max | float_floor }}" value="{{ next_price.max }}" class="{{ plugin_prefix }}cbx_price_selection_filters" >*/
/* */
/* */
/* */
/* 				{% if woo_ext_search_options.run_search_by_click == 'both' or woo_ext_search_options.run_search_by_click == 'link' %}*/
/* 				<a class="nsn_woo_ext_search_a_link" onclick="javascript:  nsn_woo_ext_search_frontendFuncsObj.filter_elementClicked( 'price', '', '{{ next_price.max }}' );	return false;		">*/
/* 					{% endif %}*/
/* 				&nbsp;{{ currency_symbol }}{{ next_price.min }}&nbsp;-&nbsp;{{	next_price.max }}*/
/* 				{% if next_price.products_count is defined %}<b>({{ next_price.products_count }})</b>{% else %}{% endif %}*/
/* 					{% if woo_ext_search_options.run_search_by_click == 'both' or woo_ext_search_options.run_search_by_click == 'link' %}*/
/* 				</a>*/
/* 				{% endif %}*/
/* */
/* */
/* 			<li>*/
/* 			{% endif %}*/
/* */
/* 		{% endfor %}*/
/* */
/* 		</ul>*/
/* 	</div>*/
/* 	{{ library.header_block_end("Prices Filter Block End") }}*/
/* {% endif %}*/
/* */
