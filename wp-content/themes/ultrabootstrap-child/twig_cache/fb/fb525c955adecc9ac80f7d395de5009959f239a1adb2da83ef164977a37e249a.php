<?php

/* white-simple/woo_ext_products_list.twig */
class __TwigTemplate_b9c4cfc54210aeb8450a396fc9aca3cd5990fdc4fbfecb0d63ad299cbb8cf331 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 198
        echo "  ";
        // line 199
        echo "
<span id=\"nsn_woo_ext_search_product_title\">&nbsp;</span>
<h2 class=\"site_content_title \"><center>
\t\t";
        // line 202
        if ((twig_length_filter($this->env, (isset($context["products_list"]) ? $context["products_list"] : null)) == 0)) {
            // line 203
            echo "\t\tNo products found</span>
    ";
        } else {
            // line 205
            echo "\t\t\tFound ";
            echo (isset($context["total_products_count"]) ? $context["total_products_count"] : null);
            echo " Product";
            if (((isset($context["total_products_count"]) ? $context["total_products_count"] : null) > 1)) {
                echo "s";
            }
            // line 206
            echo "\t\t\t";
            if (((isset($context["total_products_count"]) ? $context["total_products_count"] : null) != twig_length_filter($this->env, (isset($context["products_list"]) ? $context["products_list"] : null)))) {
                // line 207
                echo "\t\t\t\t(";
                echo twig_length_filter($this->env, (isset($context["products_list"]) ? $context["products_list"] : null));
                echo " product";
                if ((twig_length_filter($this->env, (isset($context["products_list"]) ? $context["products_list"] : null)) > 1)) {
                    echo "s";
                }
                echo " on  page ";
                echo (isset($context["page"]) ? $context["page"] : null);
                echo ")
\t\t\t";
            }
            // line 209
            echo "\t\t";
        }
        // line 210
        echo "\t</center>
</h2>

";
        // line 215
        echo "<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t";
        // line 216
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products_list"]) ? $context["products_list"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["nextProduct"]) {
            // line 217
            echo "
\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row col-xs-12 col-sm-6\">
\t\t\t<div class=\"nsn_woo_ext_search_next_woo_product\">
\t\t\t\t<ul class=\"nsn_woo_ext_search_ul_multiline\">

            <span class=\"visible-xs\">
\t            ";
            // line 223
            echo $this->getAttribute($this, "show_next_product_info", array(0 => $context["nextProduct"], 1 => (isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), 2 => (isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : null), 3 => (isset($context["service_category_for_attrs_array"]) ? $context["service_category_for_attrs_array"] : null), 4 => "_xs"), "method");
            echo "
\t        </span>
\t        <span class=\"visible-sm\">
\t            ";
            // line 226
            echo $this->getAttribute($this, "show_next_product_info", array(0 => $context["nextProduct"], 1 => (isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), 2 => (isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : null), 3 => (isset($context["service_category_for_attrs_array"]) ? $context["service_category_for_attrs_array"] : null), 4 => "_sm"), "method");
            echo "
\t        </span>
\t        <span class=\"visible-md\">
\t            ";
            // line 229
            echo $this->getAttribute($this, "show_next_product_info", array(0 => $context["nextProduct"], 1 => (isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), 2 => (isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : null), 3 => (isset($context["service_category_for_attrs_array"]) ? $context["service_category_for_attrs_array"] : null), 4 => "_md"), "method");
            echo "
\t        </span>
\t        <span class=\"visible-lg\">
\t            ";
            // line 232
            echo $this->getAttribute($this, "show_next_product_info", array(0 => $context["nextProduct"], 1 => (isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), 2 => (isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : null), 3 => (isset($context["service_category_for_attrs_array"]) ? $context["service_category_for_attrs_array"] : null), 4 => "_md"), "method");
            echo "
\t        </span>

\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>

\t\t";
            // line 239
            if (($this->getAttribute($context["loop"], "index0", array()) % 2 == 1)) {
                // line 240
                echo "\t\t\t<span class=\"visible-sm\">
\t\t<div class=\"clearfix visible-sm-block\"></div>
    </span>
\t\t\t<span class=\"visible-md\">
\t\t<div class=\"clearfix visible-md-block\"></div>
    </span>
\t\t\t<span class=\"visible-lg\">
\t\t<div class=\"clearfix visible-lg-block\"></div>
    </span>
\t\t";
            }
            // line 250
            echo "

\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['nextProduct'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 253
        echo "</div>

";
        // line 255
        if (((isset($context["navigationHTML"]) ? $context["navigationHTML"] : null) != "")) {
            // line 256
            echo "\t<div class=\"row nsn_woo_ext_search_block_padding_sm\">
\t\t";
            // line 257
            echo (isset($context["navigationHTML"]) ? $context["navigationHTML"] : null);
            echo "
\t</div>
";
        }
    }

    // line 1
    public function getshow_next_product_info($__nextProduct__ = null, $__fields_to_show__ = null, $__attrs_to_show__ = null, $__service_category_for_attrs_array__ = null, $__field_param__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "nextProduct" => $__nextProduct__,
            "fields_to_show" => $__fields_to_show__,
            "attrs_to_show" => $__attrs_to_show__,
            "service_category_for_attrs_array" => $__service_category_for_attrs_array__,
            "field_param" => $__field_param__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 2
            echo "
\t";
            // line 4
            echo "\t<li class=\"post-";
            echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "id", array(), "array");
            echo " \">
\t\t";
            // line 5
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "title", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "title", array()))) {
                // line 6
                echo "\t\t\t<h4 class=\"row nsn_woo_ext_search_woo_product_next_row site_content_subtitle\">
\t\t\t\t";
                // line 7
                if ((($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "purchase_note", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "purchase_note", array())) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "purchase_note", array(), "array") != ""))) {
                    // line 8
                    echo "\t\t\t\t\t<span class=\"fa fa-info-circle pull-left\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "purchase_note", array(), "array");
                    echo "\"></span>&nbsp;
\t\t\t\t";
                }
                // line 10
                echo "\t\t\t\t<a href=\"";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "guid", array(), "array");
                echo "\" class=\"woo_products_title nsn_woo_ext_search_woo_products_field_value nsn_woo_ext_search_a_link\">";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "id", array(), "array");
                echo "->";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "post_title", array(), "array");
                echo "</a>
\t\t\t</h4>
\t\t";
            }
            // line 13
            echo "
\t\t";
            // line 14
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "image", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "image", array()))) {
                // line 15
                echo "\t\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t\t";
                // line 16
                if ((($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "is_in_stock", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "is_in_stock", array())) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "is_in_stock", array(), "array") == 1))) {
                    // line 17
                    echo "\t\t\t\t\t<div class=\"nsn_woo_ext_search_img_banner\"  >
\t\t\t\t\t\t<img src=\"";
                    // line 18
                    echo (isset($context["plugin_images"]) ? $context["plugin_images"] : null);
                    echo "in_stock.png\">
\t\t\t\t\t</div>
\t\t\t\t";
                }
                // line 21
                echo "
\t\t\t\t<div class=\"next_product_image\" >
\t\t\t\t\t<a href=\"";
                // line 23
                echo "\">
\t\t\t\t\t\t<div class=\"nsn_woo_ext_search_image_border\" style=\"background-image: url('";
                // line 24
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "image_url", array(), "array");
                echo "'); width:180px; height:180px;\"   id=\"spotlight_img_product_";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "id", array(), "array");
                echo "\"></div>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t";
            }
            // line 29
            echo "
\t\t";
            // line 31
            echo "\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["next_attr_to_show"]) {
                // line 32
                echo "\t\t\t";
                // line 33
                echo "\t\t\t";
                $context["is_field_used"] = false;
                // line 34
                echo "
\t\t\t";
                // line 36
                echo "\t\t\t\t";
                // line 37
                echo "\t\t\t\t\t";
                // line 38
                echo "\t\t\t\t\t";
                // line 39
                echo "\t\t\t\t\t";
                // line 40
                echo "\t\t\t\t";
                // line 41
                echo "\t\t\t";
                // line 42
                echo "
\t\t\t";
                // line 43
                if ((twig_in_filter($this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), twig_get_array_keys_filter((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null))) && ((isset($context["is_field_used"]) ? $context["is_field_used"] : null) == false))) {
                    // line 44
                    echo "
\t\t\t\t";
                    // line 45
                    if (($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), array(), "array") != ""))) {
                        // line 46
                        echo "\t\t\t\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t\t\t\t<span class=\"woo_products_label\">";
                        // line 47
                        echo twig_title_string_filter($this->env, $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"));
                        echo ":</span>
\t\t\t\t\t<span class=\"woo_products_";
                        // line 48
                        echo $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array");
                        echo " nsn_woo_ext_search_woo_products_field_value\">
\t\t\t\t\t\t";
                        // line 50
                        echo "\t\t\t\t\t\t";
                        $context["service_post_id"] = "";
                        // line 51
                        echo "\t\t\t\t\t\t";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["service_category_for_attrs_array"]) ? $context["service_category_for_attrs_array"] : null));
                        foreach ($context['_seq'] as $context["key_attr"] => $context["next_service_category_for_attr"]) {
                            // line 52
                            echo "\t\t\t\t\t\t\t";
                            if (($this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array") == $context["key_attr"])) {
                                // line 53
                                echo "\t\t\t\t\t\t\t\t";
                                $context["attr_key_name"] = ((("nsn-woo-ext-search-service_category_attr_" . $this->getAttribute($context["next_attr_to_show"], "attr_name", array())) . "_") . call_user_func_array($this->env->getFilter('change_submit_key')->getCallable(), array($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), $this->getAttribute($context["next_attr_to_show"], "attr_name", array()), array(), "array"))));
                                // line 55
                                echo "\t\t\t\t\t\t\t\t";
                                if ($this->getAttribute($context["next_service_category_for_attr"], (isset($context["attr_key_name"]) ? $context["attr_key_name"] : null), array(), "array", true, true)) {
                                    // line 56
                                    echo "\t\t\t\t\t\t\t\t\t";
                                    $context["service_post_id"] = $this->getAttribute($context["next_service_category_for_attr"], (isset($context["attr_key_name"]) ? $context["attr_key_name"] : null), array(), "array");
                                    // line 57
                                    echo "\t\t\t\t\t\t\t\t";
                                }
                                // line 58
                                echo "\t\t\t\t\t\t\t";
                            }
                            // line 59
                            echo "\t\t\t\t\t\t";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['key_attr'], $context['next_service_category_for_attr'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 60
                        echo "\t\t\t\t\t\t";
                        if ((($this->getAttribute($context["next_attr_to_show"], "show_attribute_link_to_post", array(), "array") == "yes") && ((isset($context["service_post_id"]) ? $context["service_post_id"] : null) != ""))) {
                            // line 61
                            echo "\t\t\t\t\t\t\t<a class=\"nsn_woo_ext_search_a_link\" href=\"";
                            echo call_user_func_array($this->env->getFilter('make_post_url')->getCallable(), array((isset($context["service_post_id"]) ? $context["service_post_id"] : null)));
                            echo "\">";
                            echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), array(), "array");
                            echo "</a>
\t\t\t\t\t\t";
                        } else {
                            // line 63
                            echo "\t\t\t\t\t\t\t";
                            echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), array(), "array");
                            echo "
\t\t\t\t\t\t";
                        }
                        // line 65
                        echo "\t\t\t\t\t</span>
\t\t\t\t\t</div>
\t\t\t\t";
                    }
                    // line 68
                    echo "
\t\t\t";
                }
                // line 70
                echo "
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_attr_to_show'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 72
            echo "\t\t";
            // line 73
            echo "

\t\t";
            // line 75
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "post_date", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "post_date", array()))) {
                // line 76
                echo "\t\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t\t<span class=\"woo_products_label\">";
                // line 77
                echo twig_title_string_filter($this->env, "Publication");
                echo ":</span>
\t\t\t<span class=\"woo_products_post_date nsn_woo_ext_search_woo_products_field_value\">
\t\t\t\t";
                // line 79
                echo call_user_func_array($this->env->getFilter('date_time')->getCallable(), array($this->getAttribute($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "post", array()), "post_date", array())));
                echo "
\t\t\t</span>
\t\t\t</div>
\t\t";
            }
            // line 83
            echo "

\t\t";
            // line 85
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "categories", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "categories", array()))) {
                // line 86
                echo "\t\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t\t<span class=\"woo_products_label\">Categor";
                // line 87
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "categories_list", array(), "array")) > 1)) {
                    echo "ies";
                } else {
                    echo "y";
                }
                echo ":</span>
\t\t\t\t";
                // line 88
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "categories_list", array(), "array"), "ret_titles", array(), "array"));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["next_category_title"]) {
                    // line 89
                    echo "\t\t\t\t\t<span class=\"woo_products_categories nsn_woo_ext_search_woo_products_field_value\">
\t\t\t\t<a href=\"";
                    // line 90
                    echo call_user_func_array($this->env->getFilter('make_woo_product_category_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "categories_list", array(), "array"), "ret_slugs", array(), "array"), $this->getAttribute($context["loop"], "index0", array()), array(), "array")));
                    echo "\" class=\"nsn_woo_ext_search_a_link\">";
                    echo $context["next_category_title"];
                    echo "</a>
\t\t\t\t\t\t";
                    // line 91
                    if ( !$this->getAttribute($context["loop"], "last", array())) {
                        echo ", ";
                    }
                    // line 92
                    echo "\t\t\t</span>
\t\t\t\t";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_category_title'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 94
                echo "\t\t\t</div>
\t\t";
            }
            // line 96
            echo "

\t\t";
            // line 98
            if ((($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sale_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sale_price", array())) || ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "regular_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "regular_price", array())))) {
                // line 99
                echo "\t\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t\t<span class=\"woo_products_label\">Price:</span>

\t\t\t<span class=\"woo_products_sale_price nsn_woo_ext_search_woo_products_field_value\">
\t\t\t\t";
                // line 103
                $context["is_price_shown"] = false;
                // line 104
                echo "\t\t\t\t";
                if ((($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "regular_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "regular_price", array(), "array") > 0)) && ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "regular_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "regular_price", array())))) {
                    // line 105
                    echo "\t\t\t\t\t";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "formatted_regular_price", array(), "array");
                    echo "
\t\t\t\t\t";
                    // line 106
                    $context["is_price_shown"] = true;
                    // line 107
                    echo "\t\t\t\t";
                }
                // line 108
                echo "
\t\t\t\t";
                // line 109
                if ((((($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "regular_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "regular_price", array(), "array") > 0)) && ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "regular_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "regular_price", array()))) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "sale_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "sale_price", array(), "array") > 0))) && ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sale_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sale_price", array())))) {
                    // line 110
                    echo "\t\t\t\t\t&nbsp;-&nbsp;
\t\t\t\t\t";
                    // line 111
                    $context["is_price_shown"] = true;
                    // line 112
                    echo "\t\t\t\t";
                }
                // line 113
                echo "
\t\t\t\t";
                // line 114
                if ((($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "sale_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "sale_price", array(), "array") > 0)) && ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sale_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sale_price", array())))) {
                    // line 115
                    echo "\t\t\t\t\t";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "formatted_sale_price", array(), "array");
                    echo "
\t\t\t\t\t";
                    // line 116
                    $context["is_price_shown"] = true;
                    // line 117
                    echo "\t\t\t\t";
                }
                // line 118
                echo "\t\t\t</span>

\t\t\t";
                // line 120
                if (((isset($context["is_price_shown"]) ? $context["is_price_shown"] : null) == false)) {
                    // line 121
                    echo "\t\t\t\t<b>Is not set yet</b>
\t\t\t";
                }
                // line 123
                echo "
\t\t\t</div>
\t\t";
            }
            // line 126
            echo "

\t\t";
            // line 128
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "is_in_stock", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "is_in_stock", array()))) {
                // line 129
                echo "\t\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t\t<span class=\"woo_products_label\">In Stock:</span>
\t\t\t<span class=\"woo_products_is_in_stock nsn_woo_ext_search_woo_products_field_value\">
\t\t\t\t";
                // line 132
                if ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "is_in_stock", array(), "array")) {
                    echo "Yes";
                } else {
                    echo "No";
                }
                // line 133
                echo "\t\t\t</span>
\t\t\t</div>
\t\t";
            }
            // line 136
            echo "


\t\t";
            // line 139
            if (((($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "tags", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "tags", array())) && $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "tags_list", array(), "array", true, true)) && (twig_length_filter($this->env, $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "tags_list", array(), "array")) > 0))) {
                // line 140
                echo "\t\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t\t<span class=\"woo_products_label\">Tags:</span>
\t\t\t\t";
                // line 142
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "tags_list", array(), "array"));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["next_tag"]) {
                    // line 143
                    echo "\t\t\t\t\t<span class=\"woo_products_tags nsn_woo_ext_search_woo_products_field_value\">
\t\t\t\t\t\t";
                    // line 144
                    echo $this->getAttribute($context["next_tag"], "name", array());
                    if ( !$this->getAttribute($context["loop"], "last", array())) {
                        echo ", ";
                    }
                    // line 145
                    echo "\t\t\t\t\t</span>
\t\t\t\t";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_tag'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 147
                echo "\t\t\t</div>
\t\t";
            }
            // line 149
            echo "
\t\t";
            // line 150
            if ((($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sku", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sku", array())) && $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "sku", array(), "array", true, true))) {
                // line 151
                echo "\t\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t\t<span class=\"woo_products_label\">Sku:</span>
\t\t\t\t<span class=\"woo_products_sku nsn_woo_ext_search_woo_products_field_value\">
\t\t\t\t\t";
                // line 154
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "sku", array(), "array");
                echo "
\t\t\t\t</span>
\t\t\t</div>
\t\t";
            }
            // line 158
            echo "
\t\t";
            // line 160
            echo "\t\t";
            // line 161
            echo "\t\t";
            // line 162
            echo "\t\t";
            // line 164
            echo "\t\t";
            if (((($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "rating", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "rating", array())) && $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "rating_average", array(), "array", true, true)) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "rating_average", array(), "array") > 0))) {
                // line 165
                echo "\t\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t\t<span class=\"woo_products_label\">Rating:</span>
\t\t\t\t<span class=\"woo_products_rating nsn_woo_ext_search_woo_products_field_value\">
\t\t\t\t\t";
                // line 169
                echo "
\t\t\t\t\t<img src=\"";
                // line 170
                echo (isset($context["plugin_images"]) ? $context["plugin_images"] : null);
                echo "stars/stars";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "rating_average", array());
                echo ".png\" title=\"Rated ";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "rating_average", array());
                echo " out of 5\" class=\"\" style=\"padding-bottom: 4px;\">&nbsp;
\t\t\t\t\t";
                // line 171
                if (($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "comments_count", array()) > 0)) {
                    // line 172
                    echo "\t\t\t\t\t\t&nbsp;(<span class=\"rating\">";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "comments_count", array());
                    echo "</span> customer reviews)
\t\t\t\t\t";
                }
                // line 174
                echo "\t\t\t\t\t";
                // line 175
                echo "\t\t\t\t\t";
                // line 176
                echo "\t\t\t\t</span>
\t\t\t</div>
\t\t";
            } else {
                // line 179
                echo "
\t\t\t";
                // line 180
                if (((($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "rating", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "rating", array())) && $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "comments_count", array(), "array", true, true)) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "comments_count", array(), "array") > 0))) {
                    // line 181
                    echo "\t\t\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t\t\t<span class=\"woo_products_label\">Reviews:</span>
\t\t\t\t<span class=\"woo_products_reviews nsn_woo_ext_search_woo_products_field_value\">
\t\t\t\t\t";
                    // line 185
                    echo "
\t\t\t\t\t<span class=\"reviews\">";
                    // line 186
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "comments_count", array());
                    echo "</span> customer reviews

\t\t\t\t</span>
\t\t\t\t</div>


\t\t\t";
                }
                // line 193
                echo "\t\t";
            }
            // line 194
            echo "

\t</li>

";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "white-simple/woo_ext_products_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  687 => 194,  684 => 193,  674 => 186,  671 => 185,  666 => 181,  664 => 180,  661 => 179,  656 => 176,  654 => 175,  652 => 174,  646 => 172,  644 => 171,  636 => 170,  633 => 169,  628 => 165,  625 => 164,  623 => 162,  621 => 161,  619 => 160,  616 => 158,  609 => 154,  604 => 151,  602 => 150,  599 => 149,  595 => 147,  580 => 145,  575 => 144,  572 => 143,  555 => 142,  551 => 140,  549 => 139,  544 => 136,  539 => 133,  533 => 132,  528 => 129,  526 => 128,  522 => 126,  517 => 123,  513 => 121,  511 => 120,  507 => 118,  504 => 117,  502 => 116,  497 => 115,  495 => 114,  492 => 113,  489 => 112,  487 => 111,  484 => 110,  482 => 109,  479 => 108,  476 => 107,  474 => 106,  469 => 105,  466 => 104,  464 => 103,  458 => 99,  456 => 98,  452 => 96,  448 => 94,  433 => 92,  429 => 91,  423 => 90,  420 => 89,  403 => 88,  395 => 87,  392 => 86,  390 => 85,  386 => 83,  379 => 79,  374 => 77,  371 => 76,  369 => 75,  365 => 73,  363 => 72,  356 => 70,  352 => 68,  347 => 65,  341 => 63,  333 => 61,  330 => 60,  324 => 59,  321 => 58,  318 => 57,  315 => 56,  312 => 55,  309 => 53,  306 => 52,  301 => 51,  298 => 50,  294 => 48,  290 => 47,  287 => 46,  285 => 45,  282 => 44,  280 => 43,  277 => 42,  275 => 41,  273 => 40,  271 => 39,  269 => 38,  267 => 37,  265 => 36,  262 => 34,  259 => 33,  257 => 32,  252 => 31,  249 => 29,  239 => 24,  236 => 23,  232 => 21,  226 => 18,  223 => 17,  221 => 16,  218 => 15,  216 => 14,  213 => 13,  202 => 10,  196 => 8,  194 => 7,  191 => 6,  189 => 5,  184 => 4,  181 => 2,  165 => 1,  157 => 257,  154 => 256,  152 => 255,  148 => 253,  132 => 250,  120 => 240,  118 => 239,  108 => 232,  102 => 229,  96 => 226,  90 => 223,  82 => 217,  65 => 216,  62 => 215,  57 => 210,  54 => 209,  42 => 207,  39 => 206,  32 => 205,  28 => 203,  26 => 202,  21 => 199,  19 => 198,);
    }
}
/* {% macro show_next_product_info( nextProduct, fields_to_show, attrs_to_show, service_category_for_attrs_array, field_param ) %}*/
/* */
/* 	{#nextProduct::{{ nextProduct | d }}#}*/
/* 	<li class="post-{{ nextProduct['id'] }} ">*/
/* 		{% if fields_to_show.title is defined and fields_to_show.title %}*/
/* 			<h4 class="row nsn_woo_ext_search_woo_product_next_row site_content_subtitle">*/
/* 				{% if fields_to_show.purchase_note is defined and fields_to_show.purchase_note and nextProduct['purchase_note'] != "" %}*/
/* 					<span class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="top" title="{{ nextProduct['purchase_note'] }}"></span>&nbsp;*/
/* 				{% endif %}*/
/* 				<a href="{{ nextProduct['guid'] }}" class="woo_products_title nsn_woo_ext_search_woo_products_field_value nsn_woo_ext_search_a_link">{{ nextProduct['id'] }}->{{ nextProduct['post_title'] }}</a>*/
/* 			</h4>*/
/* 		{% endif %}*/
/* */
/* 		{% if fields_to_show.image is defined and fields_to_show.image  %}*/
/* 			<div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 				{% if fields_to_show.is_in_stock is defined and fields_to_show.is_in_stock and nextProduct['is_in_stock'] == 1 %}*/
/* 					<div class="nsn_woo_ext_search_img_banner"  >*/
/* 						<img src="{{ plugin_images }}in_stock.png">*/
/* 					</div>*/
/* 				{% endif %}*/
/* */
/* 				<div class="next_product_image" >*/
/* 					<a href="{#{ nextProduct['guid'] }#}">*/
/* 						<div class="nsn_woo_ext_search_image_border" style="background-image: url('{{ nextProduct['image_url'] }}'); width:180px; height:180px;"   id="spotlight_img_product_{{ nextProduct['id'] }}"></div>*/
/* 					</a>*/
/* 				</div>*/
/* 			</div>*/
/* 		{% endif %}*/
/* */
/* 		{#==================== START ==============#}*/
/* 		{% for next_attr_to_show in attrs_to_show %}*/
/* 			{#next_attr_to_show::{{ next_attr_to_show | d }}#}*/
/* 			{% set is_field_used = false %}*/
/* */
/* 			{#{% if next_attr_to_show['attr_name'] == "rating" and ( nextProduct.rating_average > 0 or nextProduct.comments_count > 0 ) %}#}*/
/* 				{#<div class="row nsn_woo_ext_search_woo_product_next_row">#}*/
/* 					{#<img src="{{ site_url }}/wp-content/plugins/woo-ext-search/images/stars/stars{{ nextProduct.rating_average }}.png" title="Rated {{ nextProduct.rating_average }} out of 5" class="pull-left">&nbsp;#}*/
/* 					{#&nbsp;(<span class="rating">{{ nextProduct.comments_count }}</span> customer reviews)#}*/
/* 					{#{% set is_field_used = true %}#}*/
/* 				{#</div>#}*/
/* 			{#{% endif %}#}*/
/* */
/* 			{% if next_attr_to_show['attr_name'] in fields_to_show | keys and is_field_used == false %}*/
/* */
/* 				{% if nextProduct[next_attr_to_show['attr_name']]  is defined and nextProduct[next_attr_to_show['attr_name']]!= "" %}*/
/* 					<div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 						<span class="woo_products_label">{{ next_attr_to_show['attr_name'] | title }}:</span>*/
/* 					<span class="woo_products_{{ next_attr_to_show['attr_name'] }} nsn_woo_ext_search_woo_products_field_value">*/
/* 						{#next_attr_to_show]['attr_name']::{{ next_attr_to_show['attr_name'] }}#}*/
/* 						{% set service_post_id='' %}*/
/* 						{% for key_attr, next_service_category_for_attr in service_category_for_attrs_array %}*/
/* 							{% if next_attr_to_show['attr_name'] == key_attr %}*/
/* 								{% set attr_key_name = 'nsn-woo-ext-search-service_category_attr_' ~ next_attr_to_show.attr_name ~ '_' ~ ( nextProduct[next_attr_to_show.attr_name] | change_submit_key )*/
/* 								%}*/
/* 								{% if next_service_category_for_attr[attr_key_name] is defined %}*/
/* 									{% set service_post_id= next_service_category_for_attr[attr_key_name] %}*/
/* 								{% endif %}*/
/* 							{% endif %}*/
/* 						{% endfor %}*/
/* 						{% if next_attr_to_show['show_attribute_link_to_post'] == 'yes' and service_post_id != ""%}*/
/* 							<a class="nsn_woo_ext_search_a_link" href="{{ service_post_id | make_post_url }}">{{ nextProduct[ next_attr_to_show['attr_name'] ] }}</a>*/
/* 						{% else %}*/
/* 							{{ nextProduct[ next_attr_to_show['attr_name'] ] }}*/
/* 						{% endif %}*/
/* 					</span>*/
/* 					</div>*/
/* 				{% endif %}*/
/* */
/* 			{% endif %}*/
/* */
/* 		{% endfor %}*/
/* 		{#==================== END ==============#}*/
/* */
/* */
/* 		{% if fields_to_show.post_date is defined and fields_to_show.post_date %}*/
/* 			<div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 				<span class="woo_products_label">{{ "Publication" | title }}:</span>*/
/* 			<span class="woo_products_post_date nsn_woo_ext_search_woo_products_field_value">*/
/* 				{{ nextProduct.post.post_date | date_time }}*/
/* 			</span>*/
/* 			</div>*/
/* 		{% endif %}*/
/* */
/* */
/* 		{% if fields_to_show.categories is defined and fields_to_show.categories %}*/
/* 			<div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 				<span class="woo_products_label">Categor{% if nextProduct['categories_list'] | length > 1 %}ies{% else %}y{% endif %}:</span>*/
/* 				{% for next_category_title in nextProduct['categories_list']['ret_titles'] %}*/
/* 					<span class="woo_products_categories nsn_woo_ext_search_woo_products_field_value">*/
/* 				<a href="{{ nextProduct['categories_list']['ret_slugs'][loop.index0] | make_woo_product_category_url }}" class="nsn_woo_ext_search_a_link">{{ next_category_title }}</a>*/
/* 						{% if not loop.last %}, {% endif %}*/
/* 			</span>*/
/* 				{% endfor %}*/
/* 			</div>*/
/* 		{% endif %}*/
/* */
/* */
/* 		{% if ( fields_to_show.sale_price is defined and fields_to_show.sale_price ) or ( fields_to_show.regular_price is defined and fields_to_show.regular_price ) %}*/
/* 			<div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 				<span class="woo_products_label">Price:</span>*/
/* */
/* 			<span class="woo_products_sale_price nsn_woo_ext_search_woo_products_field_value">*/
/* 				{% set is_price_shown= false %}*/
/* 				{% if ( nextProduct['regular_price'] is defined and nextProduct['regular_price'] > 0 ) and ( fields_to_show.regular_price is defined and fields_to_show.regular_price ) %}*/
/* 					{{ nextProduct['formatted_regular_price'] }}*/
/* 					{% set is_price_shown= true %}*/
/* 				{% endif %}*/
/* */
/* 				{% if ( nextProduct['regular_price'] is defined and nextProduct['regular_price'] > 0 ) and ( fields_to_show.regular_price is defined and fields_to_show.regular_price ) and ( nextProduct['sale_price'] is defined and nextProduct['sale_price'] >0 ) and ( fields_to_show.sale_price is defined and fields_to_show.sale_price ) %}*/
/* 					&nbsp;-&nbsp;*/
/* 					{% set is_price_shown= true %}*/
/* 				{% endif %}*/
/* */
/* 				{% if ( nextProduct['sale_price'] is defined and nextProduct['sale_price'] > 0 ) and ( fields_to_show.sale_price is defined and fields_to_show.sale_price )%}*/
/* 					{{ nextProduct['formatted_sale_price'] }}*/
/* 					{% set is_price_shown= true %}*/
/* 				{% endif %}*/
/* 			</span>*/
/* */
/* 			{% if is_price_shown == false %}*/
/* 				<b>Is not set yet</b>*/
/* 			{% endif %}*/
/* */
/* 			</div>*/
/* 		{% endif %}*/
/* */
/* */
/* 		{% if fields_to_show.is_in_stock is defined and fields_to_show.is_in_stock  %}*/
/* 			<div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 				<span class="woo_products_label">In Stock:</span>*/
/* 			<span class="woo_products_is_in_stock nsn_woo_ext_search_woo_products_field_value">*/
/* 				{% if nextProduct['is_in_stock'] %}Yes{% else %}No{% endif %}*/
/* 			</span>*/
/* 			</div>*/
/* 		{% endif %}*/
/* */
/* */
/* */
/* 		{% if fields_to_show.tags is defined and fields_to_show.tags and nextProduct['tags_list'] is defined and nextProduct['tags_list'] | length > 0 %}*/
/* 			<div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 				<span class="woo_products_label">Tags:</span>*/
/* 				{% for next_tag in nextProduct['tags_list'] %}*/
/* 					<span class="woo_products_tags nsn_woo_ext_search_woo_products_field_value">*/
/* 						{{ next_tag.name }}{% if not loop.last %}, {% endif %}*/
/* 					</span>*/
/* 				{% endfor %}*/
/* 			</div>*/
/* 		{% endif %}*/
/* */
/* 		{% if fields_to_show.sku is defined and fields_to_show.sku and nextProduct['sku'] is defined %}*/
/* 			<div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 				<span class="woo_products_label">Sku:</span>*/
/* 				<span class="woo_products_sku nsn_woo_ext_search_woo_products_field_value">*/
/* 					{{ nextProduct['sku'] }}*/
/* 				</span>*/
/* 			</div>*/
/* 		{% endif %}*/
/* */
/* 		{#plugin_images::{{ plugin_images }}<br>#}*/
/* 		{#plugin_dir::{{ plugin_dir }}<br>#}*/
/* 		{#plugin_url::{{ plugin_url }}<br>#}*/
/* 		{# [rating_average] => 3*/
/*     [star_rating] => 60#}*/
/* 		{% if fields_to_show.rating is defined and fields_to_show.rating and nextProduct['rating_average'] is defined and nextProduct['rating_average'] > 0 %}*/
/* 			<div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 				<span class="woo_products_label">Rating:</span>*/
/* 				<span class="woo_products_rating nsn_woo_ext_search_woo_products_field_value">*/
/* 					{#{{ nextProduct['rating_average'] }}   <br>#}*/
/* */
/* 					<img src="{{ plugin_images }}stars/stars{{ nextProduct.rating_average }}.png" title="Rated {{ nextProduct.rating_average }} out of 5" class="" style="padding-bottom: 4px;">&nbsp;*/
/* 					{% if nextProduct.comments_count > 0 %}*/
/* 						&nbsp;(<span class="rating">{{ nextProduct.comments_count }}</span> customer reviews)*/
/* 					{% endif %}*/
/* 					{#<br>nextProduct[rating_average]::{{ nextProduct['rating_average'] }}#}*/
/* 					{#<br>nextProduct[star_rating]::{{ nextProduct['star_rating'] }}#}*/
/* 				</span>*/
/* 			</div>*/
/* 		{% else %}*/
/* */
/* 			{% if fields_to_show.rating is defined and fields_to_show.rating and nextProduct['comments_count'] is defined and nextProduct['comments_count'] > 0 %}*/
/* 				<div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 					<span class="woo_products_label">Reviews:</span>*/
/* 				<span class="woo_products_reviews nsn_woo_ext_search_woo_products_field_value">*/
/* 					{#{{ nextProduct['reviews_average'] }}   <br>#}*/
/* */
/* 					<span class="reviews">{{ nextProduct.comments_count }}</span> customer reviews*/
/* */
/* 				</span>*/
/* 				</div>*/
/* */
/* */
/* 			{% endif %}*/
/* 		{% endif %}*/
/* */
/* */
/* 	</li>*/
/* */
/* {% endmacro %}  {# show_next_product_info END #}*/
/* */
/* <span id="nsn_woo_ext_search_product_title">&nbsp;</span>*/
/* <h2 class="site_content_title "><center>*/
/* 		{% if products_list | length == 0 %}*/
/* 		No products found</span>*/
/*     {% else %}*/
/* 			Found {{ total_products_count }} Product{% if total_products_count > 1 %}s{% endif %}*/
/* 			{% if total_products_count != products_list | length %}*/
/* 				({{ products_list | length }} product{% if products_list | length > 1 %}s{% endif %} on  page {{ page }})*/
/* 			{% endif %}*/
/* 		{% endif %}*/
/* 	</center>*/
/* </h2>*/
/* */
/* {#fields_to_show::{{ fields_to_show | d }}#}*/
/* {#attrs_to_show::{{ attrs_to_show | d }}#}*/
/* <div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 	{% for nextProduct in products_list %}*/
/* */
/* 		<div class="row nsn_woo_ext_search_woo_product_next_row col-xs-12 col-sm-6">*/
/* 			<div class="nsn_woo_ext_search_next_woo_product">*/
/* 				<ul class="nsn_woo_ext_search_ul_multiline">*/
/* */
/*             <span class="visible-xs">*/
/* 	            {{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show, service_category_for_attrs_array, "_xs" ) }}*/
/* 	        </span>*/
/* 	        <span class="visible-sm">*/
/* 	            {{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show, service_category_for_attrs_array, "_sm" ) }}*/
/* 	        </span>*/
/* 	        <span class="visible-md">*/
/* 	            {{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show, service_category_for_attrs_array, "_md" ) }}*/
/* 	        </span>*/
/* 	        <span class="visible-lg">*/
/* 	            {{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show, service_category_for_attrs_array, "_md" ) }}*/
/* 	        </span>*/
/* */
/* 				</ul>*/
/* 			</div>*/
/* 		</div>*/
/* */
/* 		{% if loop.index0 is odd %}*/
/* 			<span class="visible-sm">*/
/* 		<div class="clearfix visible-sm-block"></div>*/
/*     </span>*/
/* 			<span class="visible-md">*/
/* 		<div class="clearfix visible-md-block"></div>*/
/*     </span>*/
/* 			<span class="visible-lg">*/
/* 		<div class="clearfix visible-lg-block"></div>*/
/*     </span>*/
/* 		{% endif %}*/
/* */
/* */
/* 	{% endfor %}*/
/* </div>*/
/* */
/* {% if navigationHTML != "" %}*/
/* 	<div class="row nsn_woo_ext_search_block_padding_sm">*/
/* 		{{ navigationHTML }}*/
/* 	</div>*/
/* {% endif %}*/
/* */
