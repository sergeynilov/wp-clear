<?php if (empty($_POST['uploaded_file_path'])) {  ?>
    <h2 style="color: #fff;"><?php _e( 'Invalid request', 'wooexim-import' ); ?></h2>
    <?php return;  ?>
<?php }
echo '<pre>$_POST[current_row_index]::'.print_r( ( !empty($_POST['current_row_index']) ? $_POST['current_row_index'] : '' ) ,true).'</pre>';
$post_data = array(
	'current_row_index' => $_POST['current_row_index'],
	'uploaded_file_path' => $_POST['uploaded_file_path'],
	'source_upload_dir_path' => $_POST['source_upload_dir_path'],
	'header_row' => $_POST['header_row'],
	'cbx_selected_attributes' => $_POST['cbx_selected_attributes'],
	'cbx_selected_products' => $_POST['cbx_selected_products'],
	'cbx_selected_users' => $_POST['cbx_selected_users'],
	'limit' => intval($_POST['limit']),
	'map_to' => $_POST['map_to'],
	'custom_field_name' => $_POST['custom_field_name'],
	'custom_field_visible' => $_POST['custom_field_visible'],
	'product_image_set_featured' => $_POST['product_image_set_featured'],
	'product_image_skip_duplicates' => $_POST['product_image_skip_duplicates'],
	'post_meta_key' => $_POST['post_meta_key'],
	'user_locale' => $_POST['user_locale'],
	'import_csv_separator' => $_POST['import_csv_separator'],
	'import_csv_hierarchy_separator' => $_POST['import_csv_hierarchy_separator']
);
//echo '<pre>$post_data::'.print_r($post_data,true).'</pre>';
//die("-1 XXZ  0ZA");
?>
<script type="text/javascript">

    function var_dump(oElem, from_line, till_line) {
        var sStr = '';
        if (typeof(oElem) == 'string' || typeof(oElem) == 'number')     {
            sStr = oElem;
        } else {
            var sValue = '';
            for (var oItem in oElem) {
                sValue = oElem[oItem];
                if (typeof(oElem) == 'innerHTML' || typeof(oElem) == 'outerHTML') {
                    sValue = sValue.replace(/</g, '&lt;').replace(/>/g, '&gt;');
                }
                sStr += 'obj.' + oItem + ' = ' + sValue + '\n';
            }
        }
        //alert( "from_line::"+(typeof from_line) )
        if ( typeof from_line == "number" && typeof till_line == "number" ) {
            return sStr.substr( from_line, till_line );
        }
        if ( typeof from_line == "number" ) {
            return sStr.substr( from_line );
        }
        return sStr;
    }


    jQuery(document).ready(function($){

        $("#show_debug").click(function(){
            $("#debug").show();
            $(this).hide();
        });

        doAjaxImport(<?php echo $post_data['limit']; ?>, 0);

        function doAjaxImport(limit, offset) {
//            alert( "offset::"+offset+",  "+"    uploaded_file_path::"+var_dump( <?php //echo json_encode($post_data['uploaded_file_path'] )  ?>// ) )
            var data = {
                "action": "wooexim-import-ajax",
                "current_row_index": <?php echo json_encode($post_data['current_row_index']); ?>,
                "uploaded_file_path": <?php echo json_encode($post_data['uploaded_file_path']); ?>,
                "source_upload_dir_path": <?php echo json_encode($post_data['source_upload_dir_path']); ?>,
                "cbx_selected_attributes": <?php echo json_encode($post_data['cbx_selected_attributes']); ?>,
                "header_row": <?php echo json_encode($post_data['header_row']); ?>,
                "cbx_selected_products": <?php echo json_encode($post_data['cbx_selected_products']); ?>,
                "cbx_selected_users": <?php echo json_encode($post_data['cbx_selected_users']); ?>,
                "limit": limit,
                "offset": offset,
                "map_to": '<?php echo (serialize($post_data['map_to'])); ?>',
                "custom_field_name": '<?php echo (serialize($post_data['custom_field_name'])); ?>',
                "custom_field_visible": '<?php echo (serialize($post_data['custom_field_visible'])); ?>',
                "product_image_set_featured": '<?php echo (serialize($post_data['product_image_set_featured'])); ?>',
                "product_image_skip_duplicates": '<?php echo (serialize($post_data['product_image_skip_duplicates'])); ?>',
                "post_meta_key": '<?php echo (serialize($post_data['post_meta_key'])); ?>',
                "user_locale": '<?php echo (serialize($post_data['user_locale'])); ?>',
                "import_csv_separator": '<?php echo (serialize($post_data['import_csv_separator'])); ?>',
                "import_csv_hierarchy_separator": '<?php echo (serialize($post_data['import_csv_hierarchy_separator'])); ?>'
            };

            //ajaxurl is defined by WordPress
            $.post(ajaxurl, data, ajaxImportCallback);
        }

        function ajaxImportCallback(response_text) {
//            alert( "ajaxImportCallback  response_text::"+response_text )

            $("#debug").append($(document.createElement("p")).text(response_text));

            var response = jQuery.parseJSON(response_text);

            $("#insert_count").text(response.insert_count + " (" + response.insert_percent +"%)");
            $("#remaining_count").text(response.remaining_count);
            $("#row_count").text( response.row_count + ( response.skipped_products_count> 0 ? ", "+response.skipped_products_count + ", products skipped as unchecked" : "" ) );

            if ( response.attributes_added_row_count > 0 ) {
                $("#attributes_added_row_count").text( response.attributes_added_row_count + ( response.skipped_attributes_count > 0 ? response.skipped_attributes_count + ", attributes skipped as unchecked" : "" ) );
            }

            if ( response.users_added_row_count > 0 ) {
                $("#attributes_added_row_count").text( response.users_added_row_count );
            }

/*
 $attributes_added_row_count= $ret['attributes_added_row_count'];
 $attributes_attrs_added_row_count= $ret['attributes_attrs_added_row_count'];

 */
            //show inserted rows
            for(var row_num in response.inserted_rows) {
                var tr = $(document.createElement("tr"));

                if(response.inserted_rows[row_num]['success'] == true) {
                    if(response.inserted_rows[row_num]['has_errors'] == true) {
                        tr.addClass("error");
                    } else {
                        tr.addClass("success");
                    }
                } else {
                    tr.addClass("fail");
                }

                var post_link = $(document.createElement("a"));
                post_link.attr("target", "_blank");
                post_link.attr("href", "<?php echo get_admin_url(); ?>post.php?post=" + response.inserted_rows[row_num]['post_id'] + "&action=edit");
                post_link.text(response.inserted_rows[row_num]['post_id']);

                tr.append($(document.createElement("td")).append($(document.createElement("span")).addClass("icon")));
                tr.append($(document.createElement("td")).text(response.inserted_rows[row_num]['row_id']));
                tr.append($(document.createElement("td")).append(post_link));
                tr.append($(document.createElement("td")).text(response.inserted_rows[row_num]['name']));
                tr.append($(document.createElement("td")).text(response.inserted_rows[row_num]['sku']));
                tr.append($(document.createElement("td")).text(response.inserted_rows[row_num]['price']));

                var result_messages = "";
                if(response.inserted_rows[row_num]['has_messages'] == true) {
                    result_messages += response.inserted_rows[row_num]['messages'].join("\n") + "\n";
                }
                if(response.inserted_rows[row_num]['has_errors'] == true) {
                    result_messages += response.inserted_rows[row_num]['errors'].join("\n") + "\n";
                } else {
                    result_messages += "No errors.";
                }
                tr.append($(document.createElement("td")).text(result_messages));

                tr.appendTo("#inserted_rows tbody");
            }

            //show error messages
            for(var message in response.error_messages) {
                $(document.createElement("li")).text(response.error_messages[message]).appendTo(".import_error_messages");
            }

            //move on to the next set!
            if(parseInt(response.remaining_count) > 0) {
                doAjaxImport(response.limit, response.new_offset);
            } else {
                $("#import_status").addClass("complete");
            }
        }
    });
</script>

<div class="wooexim_wrapper wrap">
    <div id="icon-tools" class="icon32"><br /></div>
    <div style="background: #9b5c8f;min-height: 92px;padding: 10px;color: #fff;">
		<img style="border: 1px solid #e3e3e3;padding: 5px;float: left;margin-right: 10px; "src="<?php echo WOOEXIM_PATH . 'img/thumb.jpg'; ?>">
		
		<h2 style="color: #fff;"><?php _e( 'WOOEXIM &raquo; Import Product &raquo; Results', 'wooexim-import' ); ?></h2>
		<p style="color: #fff;line-height: 0.5;"><?php _e( 'Developed by <a style="color: #fff;" href="http://WooEXIM.com" target="_blank">WooEXIM.com</a> Version: 1.0.0', 'wooexim-import' ); ?></p>
		<p style="color: #fff;line-height: 0.5;"><?php _e( 'Quick and easy plugin for WooCommerce product export-import.', 'wooexim-import' ); ?></p>
	</div>

    <ul class="import_error_messages" style="color:red;font-weight:bold;">
    </ul>

    <div id="import_status">
        <div id="import_in_progress">
            <img src="<?php echo WOOEXIM_PATH; ?>img/ajax-loader.gif"
                alt="<?php _e( 'Importing. Please do not close this window or click your browser\'s stop button.', 'wooexim-import' ); ?>"
                title="<?php _e( 'Importing. Please do not close this window or click your browser\'s stop button.', 'wooexim-import' ); ?>">

            <strong><?php _e( 'Importing. Please do not close this window or click your browser\'s stop button.', 'wooexim-import' ); ?></strong>
        </div>
        <div id="import_complete">
            <img src="<?php echo WOOEXIM_PATH; ?>img/complete.png"
                alt="<?php _e( 'Import complete!', 'wooexim-import' ); ?>"
                title="<?php _e( 'Import complete!', 'wooexim-import' ); ?>">
            <strong><?php _e( 'Import Complete! Results below.', 'wooexim-import' ); ?></strong>
        </div>

        <table>
            <tbody>


                <tr>
                    <th><?php _e( 'Processed', 'wooexim-import' ); ?></th>
                    <td id="insert_count">0</td>
                </tr>

                <tr>
                    <th><?php _e( 'Attributes', 'wooexim-import' ); ?></th>
                    <td id="attributes_added_row_count"><?php echo ( !empty($post_data['attributes_added_row_count']) ? $post_data['attributes_added_row_count'] : 0 ); ?></td>
                </tr>

                <tr>
                    <th><?php _e( 'Users', 'wooexim-import' ); ?></th>
                    <td id="users_added_row_count"><?php echo ( !empty($post_data['users_added_row_count']) ? $post_data['users_added_row_count'] : 0 ); ?></td>
                </tr>

                <tr>
                    <th><?php _e( 'Remainin', 'wooexim-import' ); ?>g</th>
                    <td id="remaining_count"><?php echo ( !empty($post_data['row_count']) ? $post_data['row_count'] : 0 ); ?></td>
                </tr>
                <tr>
                    <th><?php _e( 'Total', 'wooexim-import' ); ?></th>
                    <td id="row_count"><?php echo ( !empty($post_data['row_count']) ? $post_data['row_count'] : 0 ); ?></td>
                </tr>


            </tbody>
        </table>
    </div>

    <table id="inserted_rows" class="wp-list-table widefat fixed pages" cellspacing="0">
        <thead>
            <tr>
                <th style="width: 30px;"></th>
                <th style="width: 80px;"><?php _e( 'CSV Row', 'wooexim-import' ); ?></th>
                <th style="width: 80px;"><?php _e( 'New Post ID', 'wooexim-import' ); ?></th>
                <th><?php _e( 'Name', 'wooexim-import' ); ?></th>
                <th><?php _e( 'SKU', 'wooexim-import' ); ?></th>
                <th style="width: 120px;"><?php _e( 'Price', 'wooexim-import' ); ?></th>
                <th><?php _e( 'Result', 'wooexim-import' ); ?></th>
            </tr>
        </thead>
        <tbody><!-- rows inserted via AJAX --></tbody>
    </table>

    <p><a id="show_debug" href="#" class="button"><?php _e( 'Show Raw AJAX Responses', 'wooexim-import' ); ?></a></p>
    <div id="debug"><!-- server responses get logged here --></div>
</div>