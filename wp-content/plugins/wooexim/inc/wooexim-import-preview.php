<?php
// locale settings
ini_set("auto_detect_line_endings", true);
setlocale(LC_ALL, $_POST['user_locale']);
define("max_column_length_to_show", 512);

if (!file_exists("DebToFileClear")) {
    function DebToFileClear($contents, $IsClearText = false, $FileName = '')
    {
        //return;
        //echo '<pre>$contents::'.print_r($contents,true).'</pre>';
        try {
            if (empty($FileName)) {
                $FileName = ABSPATH . DIRECTORY_SEPARATOR . "log" . DIRECTORY_SEPARATOR . "logging_deb_preview.txt";
            }
            $IsClearText = false;
            $fd = fopen($FileName, ($IsClearText ? "w+" : "a+"));
            fwrite($fd, $contents . chr(13));
            fclose($fd);
            return true;
        } catch (Exception $lException) {
            return false;
        }
    }
} // if (!file_exists("DebToFileClear")) {

//get separator options
DebToFileClear("-110 _POST::" . print_r($_POST,true),false);
DebToFileClear("-110 _FILES::" . print_r($_FILES,true),false);

$import_csv_hierarchy_separator = isset($_POST['import_csv_hierarchy_separator']) && strlen($_POST['import_csv_hierarchy_separator']) == 1 ? $_POST['import_csv_hierarchy_separator'] : '/';
$import_csv_separator = isset($_POST['import_csv_separator']) && strlen($_POST['import_csv_separator']) == 1 ? $_POST['import_csv_separator'] : ',';
$search_existing_by = !empty($_POST['search_existing_by']) ? $_POST['search_existing_by'] : ',';
//echo '<pre>$search_existing_by::'.print_r($search_existing_by,true).'</pre>';

$error_messages = array();
$file_path= '';
$source_upload_dir_path= '';
if(isset($_POST['import_csv_url']) && strlen($_POST['import_csv_url']) > 0) {

	$file_path = trim($_POST['import_csv_url']);
    DebToFileClear('-111 $file_path::' . print_r($file_path,true),false);
    $source_upload_dir_path= get_upload_dir_path( $file_path );
    DebToFileClear('-112 $source_upload_dir_path::' . print_r($source_upload_dir_path,true),false);

} elseif(isset($_FILES['import_csv']['tmp_name'])) {
    DebToFileClear('-112 INSIDE $file_path::' . print_r($file_path,true),false);

	if(function_exists('wp_upload_dir')) {
		$upload_dir = wp_upload_dir();
		$upload_dir = $upload_dir['basedir'].'/WOOEXIM_Import';
	} else {
		$upload_dir = dirname(__FILE__).'/uploads';
	}

	if(!file_exists($upload_dir)) {
		$old_umask = umask(0);
		mkdir($upload_dir, 0755, true);
		umask($old_umask);
	}
	if(!file_exists($upload_dir)) {
		$error_messages[] = sprintf( __( 'Could not create upload directory %s.', 'wooexim-import' ), $upload_dir );
	}

	//gets uploaded file extension for security check.
	$uploaded_file_ext = strtolower(pathinfo($_FILES['import_csv']['name'], PATHINFO_EXTENSION));

	//full path to uploaded file. slugifys the file name in case there are weird characters present.
	$uploaded_file_path = $upload_dir.'/'.sanitize_title(basename($_FILES['import_csv']['name'],'.'.$uploaded_file_ext)).'.'.$uploaded_file_ext;

	if($uploaded_file_ext != 'csv') {
		$error_messages[] = sprintf( __( 'The file extension %s is not allowed.', 'wooexim-import' ), $uploaded_file_ext );

	} else {

		if(move_uploaded_file($_FILES['import_csv']['tmp_name'], $uploaded_file_path)) {
			$file_path = $uploaded_file_path;

		} else {
			$error_messages[] = sprintf( __( '%s returned false.', 'wooexim-import' ), '<code>' . move_uploaded_file() . '</code>' );
		}
	}
}
$row_count= 0;
$similar_checked_products_count= 0;
$nonsimilar_unchecked_products_count= 0;
if($file_path) {
	//now that we have the file, grab contents
    DebToFileClear('-123 $file_path::' . print_r($file_path,true),false);
	$handle = fopen($file_path, 'r' );
    DebToFileClear('-1234 $handle::' . print_r($handle,true),false);
	$import_data = array();

	if ( $handle !== FALSE ) {
		while ( ( $line = fgetcsv($handle, 0, $import_csv_separator) ) !== FALSE ) {
			$import_data[] = $line;
		}
		fclose( $handle );

	} else {
		$error_messages[] = __( 'Could not open file "'.$file_path.'". Try to remove from filename/path spaces and other special chars ', 'wooexim-import' );
	}

	if(intval($_POST['header_row']) == 1 && sizeof($import_data) > 0)
		$header_row = array_shift($import_data);

	$row_count = sizeof($import_data);
	if($row_count == 0)
		$error_messages[] = __( 'No data to import.', 'wooexim-import' );

}


//'mapping_hints' should be all lower case
//(a strtolower is performed on header_row when checking)
$col_mapping_options = array(

	'do_not_import' => array(
		'label' => __( 'Do Not Import', 'wooexim-import' ),
		'mapping_hints' => array()),

	'optgroup_general' => array(
		'optgroup' => true,
		'label' => 'General'),

	'post_title' => array(
		'label' => __( 'Name', 'wooexim-import' ),
		'mapping_hints' => array('title', 'product name')),
	'_sku' => array(
		'label' => __( 'SKU', 'wooexim-import' ),
		'mapping_hints' => array()),
	'post_content' => array(
		'label' => __( 'Description', 'wooexim-import' ),
		'mapping_hints' => array('desc', 'content')),
	'post_excerpt' => array(
		'label' => __( 'Short Description', 'wooexim-import' ),
		'mapping_hints' => array('short desc', 'excerpt')),

	'optgroup_status' => array(
		'optgroup' => true,
		'label' => 'Status and Visibility'),

	'post_status' => array(
		'label' => __( 'Status (Valid: publish/draft/trash/[more in Codex])', 'wooexim-import' ),
		'mapping_hints' => array('status', 'product status', 'post status')),
	'menu_order' => array(
		'label' => __( 'Menu Order', 'wooexim-import' ),
		'mapping_hints' => array('menu order')),
	'_visibility' => array(
		'label' => __( 'Visibility (Valid: visible/catalog/search/hidden)', 'wooexim-import' ),
		'mapping_hints' => array('visibility', 'visible')),
	'_featured' => array(
		'label' => __( 'Featured (Valid: yes/no)', 'wooexim-import' ),
		'mapping_hints' => array('featured')),
	'_stock' => array(
		'label' => __( 'Stock', 'wooexim-import' ),
		'mapping_hints' => array('qty', 'quantity')),
	'_stock_status' => array(
		'label' => __( 'Stock Status (Valid: instock/outofstock)', 'wooexim-import' ),
		'mapping_hints' => array('stock status', 'in stock')),
	'_backorders' => array(
		'label' => __( 'Backorders (Valid: yes/no/notify)', 'wooexim-import' ),
		'mapping_hints' => array('backorders')),
	'_manage_stock' => array(
		'label' => __( 'Manage Stock (Valid: yes/no)', 'wooexim-import' ),
		'mapping_hints' => array('manage stock')),
	'comment_status' => array(
		'label' => __( 'Comment/Review Status (Valid: open/closed)', 'wooexim-import' ),
		'mapping_hints' => array('comment status')),


    'ping_status' => array(
		'label' => __( 'Pingback/Trackback Status (Valid: open/closed)', 'wooexim-import' ),
		'mapping_hints' => array('ping status', 'pingback status', 'pingbacks', 'trackbacks', 'trackback status')),




    'result_comments_list' => array(
        'label' => __( 'Comments listing', 'wooexim-import' ),
        'mapping_hints' => array('result_comments_list')),
    'result_comment_rating_average' => array(
        'label' => __( 'Comment rating average', 'wooexim-import' ),
        'mapping_hints' => array('result_comment_rating_average')),
// 'result_comments_list', 'result_comment_rating_average'


    'optgroup_pricing' => array(
		'optgroup' => true,
		'label' => 'Pricing, Tax, and Shipping'),

	'_regular_price' => array(
		'label' => __( 'Regular Price', 'wooexim-import' ),
		'mapping_hints' => array('price', '_price', 'msrp')),
	'_sale_price' => array(
		'label' => __( 'Sale Price', 'wooexim-import' ),
		'mapping_hints' => array()),
	'_tax_status' => array(
		'label' => __( 'Tax Status (Valid: taxable/shipping/none)', 'wooexim-import' ),
		'mapping_hints' => array('tax status', 'taxable')),
	'_tax_class' => array(
		'label' => __( 'Tax Class', 'wooexim-import' ),
		'mapping_hints' => array()),
	'product_shipping_class_by_id' => array(
		'label' => __( 'Shipping Class By ID (Separated by "|")', 'wooexim-import' ),
		'mapping_hints' => array()),
	'product_shipping_class_by_name' => array(
		'label' => __( 'Shipping Class By Name (Separated by "|")', 'wooexim-import' ),
		'mapping_hints' => array('product_shipping_class', 'shipping_class', 'product shipping class', 'shipping class')),
	'_weight' => array(
		'label' => __( 'Weight', 'wooexim-import' ),
		'mapping_hints' => array('wt')),
	'_length' => array(
		'label' => __( 'Length', 'wooexim-import' ),
		'mapping_hints' => array('l')),
	'_width' => array(
		'label' => __( 'Width', 'wooexim-import' ),
		'mapping_hints' => array('w')),
	'_height' => array(
		'label' => __( 'Height', 'wooexim-import' ),
		'mapping_hints' => array('h')),

	'optgroup_product_types' => array(
		'optgroup' => true,
		'label' => 'Special Product Types'),

	'_downloadable' => array(
		'label' => __( 'Downloadable (Valid: yes/no)', 'wooexim-import' ),
		'mapping_hints' => array('downloadable')),
	'_virtual' => array(
		'label' => __( 'Virtual (Valid: yes/no)', 'wooexim-import' ),
		'mapping_hints' => array('virtual')),
	'_product_type' => array(
		'label' => __( 'Product Type (Valid: simple/variable/grouped/external)', 'wooexim-import' ),
		'mapping_hints' => array('product type', 'type')),
	'_button_text' => array(
		'label' => __( 'Button Text (External Product Only)', 'wooexim-import' ),
		'mapping_hints' => array('button text')),
	'_product_url' => array(
		'label' => __( 'Product URL (External Product Only)', 'wooexim-import' ),
		'mapping_hints' => array('product url', 'url')),
	'_file_paths' => array(
		'label' => __( 'File Path (Downloadable Product Only)', 'wooexim-import' ),
		'mapping_hints' => array('file path', 'file', 'file_path', 'file paths')),
	'_download_expiry' => array(
		'label' => __( 'Download Expiration (in Days)', 'wooexim-import' ),
		'mapping_hints' => array('download expiration', 'download expiry')),
	'_download_limit' => array(
		'label' => __( 'Download Limit (Number of Downloads)', 'wooexim-import' ),
		'mapping_hints' => array('download limit', 'number of downloads')),

	'optgroup_taxonomies' => array(
		'optgroup' => true,
		'label' => 'Categories and Tags'),

	'product_cat_by_name' => array(
		'label' => __( 'Categories By Name (Separated by "|")', 'wooexim-import' ),
		'mapping_hints' => array('category', 'categories', 'product category', 'product categories', 'product_cat')),
	'product_cat_by_id' => array(
		'label' => __( 'Categories By ID (Separated by "|")', 'wooexim-import' ),
		'mapping_hints' => array()),
	'product_tag_by_name' => array(
		'label' => __( 'Tags By Name (Separated by "|")', 'wooexim-import' ),
		'mapping_hints' => array('tag', 'tags', 'product tag', 'product tags', 'product_tag')),
	'product_tag_by_id' => array(
		'label' => __( 'Tags By ID (Separated by "|")', 'wooexim-import' ),
		'mapping_hints' => array()),

	'optgroup_custom' => array(
		'optgroup' => true,
		'label' => 'Custom Attributes and Post Meta'),

	'custom_field' => array(
		'label' => __( 'Custom Field / Product Attribute (Set Name Below)', 'wooexim-import' ),
		'mapping_hints' => array('custom field', 'custom')),



	'product_attributes_list' => array(
		'label' => __( 'Product Attribute\'s List', 'wooexim-import' ),
		'mapping_hints' => array('product_attributes_list', 'product_attributes_list')),



	'post_meta' => array(
		'label' => __( 'Post Meta', 'wooexim-import' ),
		'mapping_hints' => array('postmeta')),

	'optgroup_images' => array(
		'optgroup' => true,
		'label' => 'Product Images'),

	'product_image_by_url' => array(
		'label' => __( 'Images (By URL, Separated by "|")', 'wooexim-import' ),
		'mapping_hints' => array(/*'image', 'images', 'image url', 'image urls', 'product image url', 'product image urls', 'product images'*/ "do_not_import")),
	'product_image_by_path' => array(
		'label' => __( 'Images (By Local File Path, Separated by "|")', 'wooexim-import' ),
//		'label' => __( 'Images By Local File Path', 'wooexim-import' ),
		'mapping_hints' => array( 'image path', 'image paths', 'product image path', 'product_image_by_path', 'product_image_by_path'))
);
?>

<div class="wooexim_wrapper wrap">
    <div id="icon-tools" class="icon32"><br /></div>
    <div style="background: #9b5c8f;min-height: 92px;padding: 10px;color: #fff;">
		<img style="border: 1px solid #e3e3e3;padding: 5px;float: left;margin-right: 10px; "src="<?php echo WOOEXIM_PATH . 'img/thumb.jpg'; ?>">
		
		<h2 style="color: #fff;"><?php _e( 'WOOEXIM &raquo; Import Product &raquo; Preview', 'wooexim-import' ); ?></h2>
		<p style="color: #fff;line-height: 0.5;"><?php _e( 'Developed by <a style="color: #fff;" href="http://WooEXIM.com" target="_blank">WooEXIM.com</a> Version: 1.0.0', 'wooexim-import' ); ?></p>
		<p style="color: #fff;line-height: 0.5;"><?php _e( 'Quick and easy plugin for WooCommerce product export-import.', 'wooexim-import' ); ?></p>
	</div>

    <?php if(sizeof($error_messages) > 0): ?>
        <ul class="import_error_messages">
            <?php foreach($error_messages as $message):?>
                <li><b><?php echo $message; ?></b></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>

    <?php if($row_count > 0): ?>
        <form enctype="multipart/form-data" method="post" action="<?php echo get_admin_url().'admin.php?page=wooexim-import&action=result'; ?>">
            <script type="text/javascript">


                jQuery(document).ready(function($){
                    $("select.map_to").change(function(){

                        if($(this).val() == 'custom_field') {
                            $(this).closest('th').find('.custom_field_settings').show(400);
                        } else {
                            $(this).closest('th').find('.custom_field_settings').hide(400);
                        }

                        if($(this).val() == 'product_image_by_url' || $(this).val() == 'product_image_by_path') {
                            $(this).closest('th').find('.product_image_settings').show(400);
                        } else {
                            $(this).closest('th').find('.product_image_settings').hide(400);
                        }

                        if($(this).val() == 'post_meta') {
                            $(this).closest('th').find('.post_meta_settings').show(400);
                        } else {
                            $(this).closest('th').find('.post_meta_settings').hide(400);
                        }
                    });

                    //to show the appropriate settings boxes.
                    $("select.map_to").trigger('change');

                    $(window).resize(function(){
                        $("#import_data_preview").addClass("fixed").removeClass("super_wide");
                        $("#import_data_preview").css("width", "100%");

                        var cell_width = $("#import_data_preview tbody tr:first td:last").width();
                        if(cell_width < 60) {
                            $("#import_data_preview").removeClass("fixed").addClass("super_wide");
                            $("#import_data_preview").css("width", "auto");
                        }
                    });

                    //set table layout
                    $(window).trigger('resize');
                    nsn_wooxim_export_import_admin_cbx_product_onChange()
                    nsn_wooxim_export_import_admin_cbx_attribute_onChange()
					nsn_wooxim_export_import_admin_cbx_user_onChange()
                });


                function var_dump(oElem, from_line, till_line) {
                    var sStr = '';
                    if (typeof(oElem) == 'string' || typeof(oElem) == 'number')     {
                        sStr = oElem;
                    } else {
                        var sValue = '';
                        for (var oItem in oElem) {
                            sValue = oElem[oItem];
                            if (typeof(oElem) == 'innerHTML' || typeof(oElem) == 'outerHTML') {
                                sValue = sValue.replace(/</g, '&lt;').replace(/>/g, '&gt;');
                            }
                            sStr += 'obj.' + oItem + ' = ' + sValue + '\n';
                        }
                    }
                    //alert( "from_line::"+(typeof from_line) )
                    if ( typeof from_line == "number" && typeof till_line == "number" ) {
                        return sStr.substr( from_line, till_line );
                    }
                    if ( typeof from_line == "number" ) {
                        return sStr.substr( from_line );
                    }
                    return sStr;
                }


            </script>
            <input type="hidden" name="uploaded_file_path" value="<?php echo htmlspecialchars($file_path); ?>">
            <input type="hidden" name="current_row_index" value="0">
            <input type="hidden" name="source_upload_dir_path" value="<?php echo htmlspecialchars($source_upload_dir_path); ?>">
            <input type="hidden" name="import_csv_separator" value="<?php echo htmlspecialchars($import_csv_separator); ?>">
            <input type="hidden" name="import_csv_hierarchy_separator" value="<?php echo htmlspecialchars($import_csv_hierarchy_separator); ?>">
            <input type="hidden" name="header_row" value="<?php echo $_POST['header_row']; ?>">
            <input type="hidden" name="user_locale" value="<?php echo htmlspecialchars($_POST['user_locale']); ?>">
            <input type="hidden" name="row_count" value="<?php echo $row_count; ?>">
            <input type="hidden" name="limit" value="5">
            <input type="hidden" name="cbx_selected_attributes" id="cbx_selected_attributes" value="">
            <input type="hidden" name="cbx_selected_products" id="cbx_selected_products" value="">
            <input type="hidden" name="cbx_selected_users" id="cbx_selected_users" value="">

            <p>
                <button class="button-primary" type="submit"><?php _e( 'Import', 'wooexim-import' ); ?></button>
            </p>


            <?php
//            $attributes_row_number= -1;
            reset($import_data);
//            echo '<pre>$import_data[0]::'.print_r($import_data[0],true).'</pre>';
//            echo '<pre>$import_data[1]::'.print_r($import_data[1],true).'</pre>';
//            $attributes_row = !empty( $import_data[0] ) ? $import_data[0] : '';
//            $attributes_row_number= 0;
//            echo '<pre>$attributes_row::'.print_r($attributes_row,true).'</pre>';

            $zero_attributes_import_data_serialized= false;
            if ( !empty($import_data[0][0]) and !empty($import_data[0][1]) ) {
                if ($import_data[0][0] == 'attributes_list') {
                    $serialized_attributes_data = @unserialize($import_data[0][1]);
                    if ( $serialized_attributes_data ) { // 1st row is serialized attributes - show it
//                                die("-1 XXZ$serialized_attributes_data");
                        echo showAttributesSelection( $serialized_attributes_data );
                        $zero_attributes_import_data_serialized= true;

//                unset($import_data[0]);
                    } // if ( $serialized_attributes_data ) { // 1st row is serialized attributes - show it
                }
            }
//            $serialized_attributes_data = @unserialize( !empty($attributes_row[0]) ? $attributes_row[0] :'' );


            $zero_users_import_data_serialized= true;
            $serialized_users_data = @unserialize( !empty($import_data[1][1]) ? $import_data[1][1] :'' );
//            echo '<pre>$serialized_users_data::'.print_r($serialized_users_data,true).'</pre>';
            if ( $serialized_users_data ) { // 1st row is serialized attributes - show it
//                echo '<pre>$serialized_users_data::'.print_r($serialized_users_data,true).'</pre>';
//                                die("-1 XXZ serialized_users_data");
                echo showUsersSelection( $serialized_users_data );
                $zero_users_import_data_serialized= true;

            }
            if ( $zero_attributes_import_data_serialized ) {
                unset($import_data[0]);
            }
            if ( $zero_users_import_data_serialized ) {
                unset($import_data[ ($zero_attributes_import_data_serialized) ? 0 : 1 ]);
            }

            DebToFileClear("-1101 import_data:: 2" . print_r($import_data,true),false);

//                                        die("-1 XXZ -222");
            ?>

            <table id="import_data_preview" class="wp-list-table widefat fixed pages" cellspacing="2" cellpadding="2" style="border: 2px  dotted #008000">
                <thead>
                    <?php if(intval($_POST['header_row']) == 1): ?>
                        <tr class="header_row">
                            <th colspan="<?php echo sizeof($header_row); ?>"><?php _e( 'CSV Header Row', 'wooexim-import' ); ?><?php echo ' with <b>'. count($import_data) .'</b> products'; ?></th>
                        </tr>

                        <tr class="">
                            <th colspan="20">
                                <h3>Please check, if in database there are already similar posts(by given on prior page search criteria). These similar posts are not checked by default.<br> Check them, if you want to export to system and to overwrite existing products.</h3>
                            </th>
                        </tr>

                        <tr class="header_row">
                            <th>#</th>
                            <?php foreach($header_row as $col): ?>
                                <th><?php echo htmlspecialchars($col); ?></th>
                            <?php endforeach; ?>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <th>
                            <input type="checkbox" id="cbx_products_check_all" checked onchange="javascript:nsn_wooxim_export_import_admin_cbx_products_check_all_onChange()">
                        </th>
                        <?php
                            reset($import_data);
                            $first_row = current($import_data);
//                        echo '<pre>$first_row::'.print_r($first_row,true).'</pre>';
//                        echo '<pre>$col_mapping_options::'.print_r($first_row,true).'</pre>';

                        /* 	'product_image_by_path' => array(
		'label' => __( 'Images (By Local File Path, Separated by "|")', 'wooexim-import' ),
		'mapping_hints' => array( 'product_image_by_path'))
 */

                            foreach($first_row as $key => $col):
                        ?>
                            <th>
                                <div class="map_to_settings">
                                    <?php _e( 'Map to:', 'wooexim-import' ); ?> <select name="map_to[<?php echo $key; ?>]" class="map_to">
                                        <optgroup>
                                        <?php foreach($col_mapping_options as $value => $meta): ?>
                                            <?php $is_debug= ( $value == 'product_image_by_path') ?>
                                            <?php if(array_key_exists('optgroup', $meta) && $meta['optgroup'] === true): ?>
                                                </optgroup>
                                                <optgroup label="<?php echo $meta['label']; ?>">
                                            <?php else: ?>
                                                <option value="<?php echo $value; ?>" <?php
                                                    if(intval($_POST['header_row']) == 1) {
                                                        //pre-select this value if the header_row
                                                        //matches the label, value, or any of the hints.
                                                        $header_value = strtolower($header_row[$key]);
                                                        if ( $is_debug ) {
//                                                            echo '<pre>$header_value::'.print_r($header_value,true).'</pre>';
//                                                            echo '<pre>$value::'.print_r($value,true).'</pre>';
                                                            DebToFileClear("-9853 header_value::" . print_r($header_value,true),false);
                                                            DebToFileClear("-9853 value::" . print_r($value,true),false);
//                                                            die("-1 XXZ----");
                                                        }
                                                        if( /* $is_debug or */ (  $header_value == strtolower($value) ||
                                                            $header_value == strtolower($meta['label']) ||
                                                            in_array($header_value, $meta['mapping_hints'])  ) )  {
                                                            if ( $is_debug ) {
                                                                DebToFileClear("-9853 INSIDE value::" . print_r($value,true),false);
                                                            }

                                                            echo 'selected="selected"';
                                                        }
                                                    }
                                                ?>><?php echo $meta['label']; ?></option>
                                            <?php endif;?>
                                        <?php endforeach; ?>
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="custom_field_settings field_settings">
                                    <h4><?php _e( 'Custom Field Settings', 'wooexim-import' ); ?></h4>
                                    <p>
                                        <label for="custom_field_name_<?php echo $key; ?>"><?php _e( 'Name', 'wooexim-import' ); ?></label>
                                        <input type="text" name="custom_field_name[<?php echo $key; ?>]" id="custom_field_name_<?php echo $key; ?>" value="<?php echo $header_row[$key]; ?>" />
                                    </p>
                                    <p>
                                        <input type="checkbox" name="custom_field_visible[<?php echo $key; ?>]" id="custom_field_visible_<?php echo $key; ?>" value="1" checked="checked" />
                                        <label for="custom_field_visible_<?php echo $key; ?>"><?php _e( 'Visible?', 'wooexim-import' ); ?></label>
                                    </p>
                                </div>
                                <div class="product_image_settings field_settings">
                                    <h4><?php _e( 'Image Settings', 'wooexim-import' ); ?></h4>
                                    <p>
                                        <input type="checkbox" name="product_image_set_featured[<?php echo $key; ?>]" id="product_image_set_featured_<?php echo $key; ?>" value="1" checked="checked" />
                                        <label for="product_image_set_featured_<?php echo $key; ?>"><?php _e( 'Set First Image as Featured', 'wooexim-import' ); ?></label>
                                    </p>
                                    <p>
                                        <input type="checkbox" name="product_image_skip_duplicates[<?php echo $key; ?>]" id="product_image_skip_duplicates_<?php echo $key; ?>" value="1" checked="checked" />
                                        <label for="product_image_skip_duplicates_<?php echo $key; ?>"><?php _e( 'Skip Duplicate Images', 'wooexim-import' ); ?></label>
                                    </p>
                                </div>
                                <div class="post_meta_settings field_settings">
                                    <h4><?php _e( 'Post Meta Settings', 'wooexim-import' ); ?></h4>
                                    <p>
                                        <label for="post_meta_key_<?php echo $key; ?>"><?php _e( 'Meta Key', 'wooexim-import' ); ?></label>
                                        <input type="text" name="post_meta_key[<?php echo $key; ?>]" id="post_meta_key_<?php echo $key; ?>" value="<?php echo $header_row[$key]; ?>" />
                                    </p>
                                </div>
                            </th>
                        <?php endforeach; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php $row_index= 1;
                    foreach($import_data as $row_id => $row): ?>

                        <tr>
                            <td>

                                <?php if ( $search_existing_by!= '' ) : ?>
                                    <?php $similar_posts_links= search_similar_posts( $first_row, $row, $search_existing_by, true );
                                    if ( empty($similar_posts_links) ) {
                                        $nonsimilar_unchecked_products_count++;
                                    } else {
                                        $similar_checked_products_count++;
                                    }
                                    if ( $row_index==1 ) $similar_posts_links= '';// TODO?>

                                <?php echo $row_index ?>)&nbsp;<input type="checkbox" id="cbx_products_item_<?php echo $row_index ?>" name="cbx_products_item_<?php echo $row_index ?>" <?php echo (empty($similar_posts_links)?"checked":"") ?> value="<?php echo $row_index ?>" onchange="javascript:nsn_wooxim_export_import_admin_cbx_product_onChange()" class="nsn_wooxim_export_import_admin_cbx_products">
                                <?php endif;?>

                            </td>
                            <?php  $col_index= 0; foreach($row as $col): ?>
                                <td>
                                    <?php
                                    if (strlen($col)> max_column_length_to_show) {
                                        echo htmlspecialchars( substr($col, 0, max_column_length_to_show - 1 ) ).'...';
                                    } else {
                                        echo htmlspecialchars($col);
                                    }
                                    ?>
                                    <?php if ( !empty($similar_posts_links) and $col_index == 0 ) : ?>
                                        <?php echo $similar_posts_links;?>
                                    <?php endif;?>
                                </td>
                            <?php $col_index++;  endforeach; ?>
                        </tr>

                    <?php $row_index++;
                    endforeach; ?>

                    <?php if (!empty($search_existing_by)) : ?>
                    <tr class="header_row">
                        <th colspan="<?php echo sizeof($header_row); ?>"><u><?php echo 'This data set is with <b>'. count($import_data) .'</b> products, with <b>' . $nonsimilar_unchecked_products_count .'</b> new products, which will be inserted into the system and <b>'. $similar_checked_products_count .'</b> similar
products, which are not checked for import. If you check them, they will overwrite existing products.'; ?></u></th>
                    </tr>
                    <?php endif; ?>

                </tbody>
            </table>
        </form>
    <?php endif; ?>
</div>
<?php

function showAttributesSelection($serialized_attributes_data){
//    echo '<pre> showAttributesSelection($serialized_attributes_data);::'.print_r($serialized_attributes_data,true).'</pre>';
    if ( !is_array($serialized_attributes_data) or count($serialized_attributes_data) == 0 ) {
        return ' No attributes specifiled !';
    }
    $ret_str= '<table style="border: 1px solid gray">';
    $ret_str.= '<tr><td colspan="2"><h2>Select product\'s attributes to import</h2></td></tr>';
    $existing_attributes_list= get_products_attrs_list();
//    echo '<pre>$existing_attributes_list::'.print_r($existing_attributes_list,true).'</pre>';
//    die("-1 XXZ");
    foreach( $serialized_attributes_data as $next_key=>$next_attribute ) {
        $is_next_attribute_exists= false;
        $attribute_exists_text= '';
        foreach( $existing_attributes_list as $next_key=>$next_existing_attribute ) {
            if ( $next_attribute['attribute_name'] == $next_existing_attribute['attribute_name'] ) {
                $is_next_attribute_exists= true;
                $attribute_exists_text= '<br><span style="color:red;">In database there is already attribute with "'.$next_attribute['attribute_name'].'" name. It will not be exported!</span>';
            }
        }

        $checked_attr_html= ( $is_next_attribute_exists ? '' : '<input id="cbx_attribute_'.$next_attribute['attribute_name'].'" type="checkbox" class="class_cbx_attribute" value="'.$next_attribute['attribute_name'].'" checked onchange="javascript:nsn_wooxim_export_import_admin_cbx_attribute_onChange();" >&nbsp;' );

        $ret_str.= '<tr><td>'.$checked_attr_html.'<b>'.$next_attribute['attribute_label'].'<b>('.$next_attribute['attribute_name'].')</b><br> <small>with values</small>'.$attribute_exists_text.'</td><td>';
        if ( isset($next_attribute['attrs']) and is_array($next_attribute['attrs']) ) {
            $attrs_str= '';
            foreach( $next_attribute['attrs'] as $next_attr_struct ) {
                $attrs_str.= '<small>'.$next_attr_struct['name'] . $next_attr_struct['slug'].'</small>, ';
            }
            $attrs_str= trimRightSubString($attrs_str,', ');
            $ret_str.= $attrs_str.'</td></tr>';
        }
        $ret_str.= '</td></tr>';
        $ret_str.= '<tr><td>&nbsp;</td></tr>';
    }
    $ret_str.= '</table>';
    return $ret_str;
}

//////////////
function showUsersSelection($serialized_users_data) {
//    echo '<pre> showUsersSelection($serialized_users_data);::'.print_r($serialized_users_data,true).'</pre>';
    if ( !is_array($serialized_users_data) or count($serialized_users_data) == 0 ) {
        return ' No users specifiled !';
    }
    $ret_str= '<table style="border: 1px solid gray">';
    $ret_str.= '<tr><td colspan="2"><h2>Select users to import</h2></td></tr>';
    $existing_users_list= get_users_list();
//        $ret_users_list[]= array( 'ID' => $next_user->ID, /*!!!*/'user_login' => $next_user->user_login, 'display_name' => $next_user->display_name, /*!!!*/'user_email' => $next_user->user_email, 'user_url' => $next_user->user_url, 'user_registered' => $next_user->user_registered, /*!!!*/'user_nicename' => $next_user->user_nicename, 'user_status' => $next_user->user_status, 'roles' => $next_user_roles  );
//
//    echo '<pre>$existing_users_list::'.print_r($existing_users_list,true).'</pre>';
//    die("-1 XXZ");
    foreach( $serialized_users_data as $next_key=>$next_serialized_user ) {
        $is_next_user_exists= false;
        $user_exists_text= '';
        foreach( $existing_users_list as $next_key=>$next_existing_user ) {
            if ( $next_serialized_user['user_email'] == $next_existing_user['user_email'] ) {
                $is_next_user_exists= true;
                $user_exists_text= '<br><span style="color:red;">In database there is already user with "'.$next_serialized_user['user_email'].'" name. It will not be exported!</span>';
            }
        }

//        $checked_attr_html= ( $is_next_user_exists ? '' : '<input id="cbx_user_'.$next_serialized_user['ID'].'" type="checkbox" class="class_cbx_user" value="'.$next_serialized_user['ID'].'" checked onchange="javascript:nsn_wooxim_export_import_admin_cbx_user_onChange();" >&nbsp;' );
        $checked_attr_html= '';//( $is_next_user_exists ? '' : '<input id="cbx_user_'.$next_serialized_user['ID'].'" type="checkbox" class="class_cbx_user" value="'.$next_serialized_user['ID'].'" checked onchange="javascript:nsn_wooxim_export_import_admin_cbx_user_onChange();" >&nbsp;' );

        $ret_str.= '<tr><td>'.$checked_attr_html.'<b>'.$next_serialized_user['user_login'].', '.$next_serialized_user['user_email'] . ', '.$next_serialized_user['user_nicename']. ', '.$next_serialized_user['roles'].'<b>(ID='.$next_serialized_user['ID'].')</b>'.$user_exists_text.'</td><td>';
        $ret_str.= '</td></tr>';
        $ret_str.= '<tr><td>&nbsp;</td></tr>';
    }
    $ret_str.= '</table>';
    return $ret_str;
}

function getRightSubstring($S, $count)
{
    return substr($S,strlen($S)-$count,$count);
}

function trimRightSubString($S, $Substr)
{
    $Res = preg_match('/(.*?)(' . preg_quote($Substr, "/") . ')$/si', $S, $A);
    if (!empty($A[1])) return $A[1];
    return $S;
}

function search_similar_posts( $first_row, $current_row, $search_existing_by, $html_colors= false ) {
//    echo '<pre>search_similar_posts  $first_row::'.print_r($first_row,true).'</pre>';
//    echo '<pre>search_similar_posts  $current_row::'.print_r($current_row,true).'</pre>';
//    echo '<pre>search_similar_posts  $search_existing_by::'.print_r($search_existing_by,true).'</pre>';
    if ( $search_existing_by == 'search_by_sku' ) {
        $current_sku= !empty($current_row[7]) ? $current_row[7] : ''; // TODO
        if (empty($current_sku)) return '';
        $existing_post_query = array(
            'meta_key' => '_sku',
            'meta_query' => array(
                array(
                    'key'=>'_sku',
                    'value'=> $current_sku,
                    'compare' => '='
                )
            ),
            'post_type' => 'product');
        $existing_posts = get_posts($existing_post_query);
        if (empty($existing_posts) or !is_array($existing_posts)) return '';
        $ret_str= '';
        foreach( $existing_posts as $next_key=>$next_existing_post) {
            $post_href= home_url().'/wp-admin/post.php?post='.$next_existing_post->ID.'&action=edit';
            $ret_str.= '<a href="'.$post_href.'">'.$next_existing_post->ID.'->'.$next_existing_post->post_title.'('.$current_sku.')'.'</a>, ';
        }
        return '<br><small style="color:red;">Similar post(s):<br></small>'.($html_colors?'<span style="color: red; font-weight: bold">':'').trimRightSubString($ret_str,', ') .($html_colors?'</span>':'');
    }

    if ( $search_existing_by == 'search_by_post_title' ) {
        $current_title= !empty($current_row[0]) ? $current_row[0] : ''; // TODO
//        echo '<pre>$current_title::'.print_r($current_title,true).'</pre>';
        if (empty($current_title)) return '';
        $existing_post_query = array(
            's'         => $current_title,
            'post_type' => 'product',
            'sentence' => true
        );
        $existing_posts = get_posts($existing_post_query);
//        echo '<pre>$existing_posts::'.print_r($existing_posts,true).'</pre>';
//        die("-1 XXZ =1");
        if (empty($existing_posts) or !is_array($existing_posts)) return '';
        $ret_str= '';
        foreach( $existing_posts as $next_key=>$next_existing_post) {
            $post_href= home_url().'/wp-admin/post.php?post='.$next_existing_post->ID.'&action=edit';
            $ret_str.= '<a href="'.$post_href.'">'.$next_existing_post->ID.'->'.$next_existing_post->post_title.'</a>, ';
        }
        return '<br><small><span style="color: red;">Similar post(s):<br></span></small>'.($html_colors?'<span style=" font-weight: bold">':'').trimRightSubString($ret_str,', ') .($html_colors?'</span>':'');
    }


//    die("-1 XXZ");
    return '';
}

function get_products_attrs_list($attribute_name= '', $get_attrs= false) {
//        echo '<pre>$attribute_name::'.print_r($attribute_name,true).'</pre>';
    $attribute_taxonomies = wc_get_attribute_taxonomies();  //  Get attribute taxonomies.
    $ret_products_attrs_list = array();
    if ( $attribute_taxonomies ) :
        foreach ($attribute_taxonomies as $key=>$tax) :
            if (taxonomy_exists(wc_attribute_taxonomy_name($tax->attribute_name))) :
                $next_tax_struct= get_object_vars( $tax );
                $attrs_objects_list= get_terms( wc_attribute_taxonomy_name($tax->attribute_name), 'orderby=name&hide_empty=0' );
                $attrs_list= array();
                foreach( $attrs_objects_list as $next_attr_object ) {
                    $attrs_list[]= get_object_vars($next_attr_object);
                }
                $next_tax_struct['attrs'] = $attrs_list;
                $ret_products_attrs_list[] = $next_tax_struct;
            endif;
        endforeach;
    endif;
    if( !empty($attribute_name) ) {
        foreach( $ret_products_attrs_list as $next_key=> $next_products_attr ) {
            if ( $next_products_attr['attribute_name']  ==  $attribute_name ) {
//                    echo '<pre>+++$next_products_attr::'.print_r($next_products_attr,true).'</pre>';
                if ( $get_attrs and !empty($next_products_attr['attrs']) and is_array($next_products_attr['attrs']) ) {
                    $ret_array= array();
                    foreach($next_products_attr['attrs'] as $next_attr) {
                        $ret_array[] = array('ID'=>$next_attr['term_id'], 'name'=>$next_attr['name'], 'slug'=>$next_attr['slug']);
                    }
//                        echo '<pre>$::'.print_r($ret_array,true).'</pre>';
//                        die("-1 XXZZZZZZZ ");
                    return $ret_array;
                }
                return $next_products_attr;
            }
        }
        //and !empty($ret_products_attrs_list[$attribute_name]) ) {
        return ( !empty($ret_products_attrs_list[$attribute_name]) ? $ret_products_attrs_list[$attribute_name] : array() );
    }
    return $ret_products_attrs_list;
}

function get_upload_dir_path( $file_path ) {
    // -111 $file_path::http://local-wp-clear.com/wp-content/uploads/WOOEXIM_Import/DATA_FOR_UPLOAD/wooexim_export_2016_09_24%2006_09_08.csv
    $upload_dir = wp_upload_dir(); // 		$upload_dir = $upload_dir['basedir'].'/WOOEXIM_Import';
    DebToFileClear('-Z1 get_upload_dir_path $upload_dir::' . print_r($upload_dir,true),false);
/* -Z1 get_upload_dir_path $upload_dir::Array
(
    [path] => /mnt/_work_sdb8/wwwroot/wp-clear/wp-content/uploads/2016/10
    [url] => http://local-wp-clear.com/wp-content/uploads/2016/10
    [subdir] => /2016/10
    [basedir] => /mnt/_work_sdb8/wwwroot/wp-clear/wp-content/uploads
    [baseurl] => http://local-wp-clear.com/wp-content/uploads
    [error] =>
)
 */

    $pos = strrpos($file_path, "/");
    if ($pos === false) { // note: three equal signs
        return false;
    }
    $dir_path= left($file_path,$pos);
    DebToFileClear('-Z2 get_upload_dir_path $dir_path::' . print_r($dir_path,true),false);

    $dir_path= str_replace( $upload_dir['baseurl'], $upload_dir['basedir'], $dir_path );
    DebToFileClear('-Z3 get_upload_dir_path $dir_path::' . print_r($dir_path,true),false);
    return $dir_path;
}
function right($str, $length) {
    return substr($str, -$length);
}

function left($str, $length) {
    return substr($str, 0, $length);
}

function get_users_list() {
    $ret_users_list= array();
    $users_list= get_users( array( 'orderby'    => 'ID', 'order'        => 'ASC' ) );
    foreach( $users_list as $next_key=>$next_user ) {
        $next_user_roles= '';
        foreach( $next_user->roles as $next_role ) {
            $next_user_roles.= $next_role.'|';
        }
        $ret_users_list[]= array( 'ID' => $next_user->ID, /*!!!*/'user_login' => $next_user->user_login, 'display_name' => $next_user->display_name, /*!!!*/'user_email' => $next_user->user_email, 'user_url' => $next_user->user_url, 'user_registered' => $next_user->user_registered, /*!!!*/'user_nicename' => $next_user->user_nicename, 'user_status' => $next_user->user_status, 'roles' => $next_user_roles  );
    }
    return $ret_users_list;
}

?>