<?php 
// Import Functions
ini_set("auto_detect_line_endings", true);
if (!function_exists("nsn_preg_split")) {
    function nsn_preg_split($splitter, $string_items, $skip_empty = true)
    {
        $ret_array = array();
        $a = preg_split($splitter, $string_items);
        foreach ($a as $next_key => $next_value) {
            if ($skip_empty and empty($next_value)) continue;
            $ret_array[] = $next_value;
        }
        return $ret_array;
    }
}

if (!file_exists("DebToFileClear")) {
    function DebToFileClear($contents, $IsClearText = false, $FileName = '')
    {
        //return;
        //echo '<pre>$contents::'.print_r($contents,true).'</pre>';
        try {
            if (empty($FileName)) {
                $FileName = ABSPATH . DIRECTORY_SEPARATOR . "log" . DIRECTORY_SEPARATOR . "logging_deb.txt";
            }
            $IsClearText = false;
            $fd = fopen($FileName, ($IsClearText ? "w+" : "a+"));
            fwrite($fd, $contents . chr(13));
            fclose($fd);
            return true;
        } catch (Exception $lException) {
            return false;
        }
    }
} // if (!file_exists("DebToFileClear")) {

$post_data = array(
	'current_row_index' =>  ( !empty($_POST['current_row_index']) ? (int)$_POST['current_row_index'] : 0 ),
	'uploaded_file_path' => $_POST['uploaded_file_path'],
	'source_upload_dir_path' => $_POST['source_upload_dir_path'],
	'header_row' => $_POST['header_row'],
	'limit' => $_POST['limit'],
	'offset' => $_POST['offset'],
	'cbx_selected_products' => ( !empty($_POST['cbx_selected_products']) ? $_POST['cbx_selected_products'] : '' ),
	'cbx_selected_users' => ( !empty($_POST['cbx_selected_users']) ? $_POST['cbx_selected_users'] : '' ),
	'cbx_selected_attributes' => ( !empty($_POST['cbx_selected_attributes']) ? $_POST['cbx_selected_attributes'] : '' ),
	'map_to' => maybe_unserialize(stripslashes($_POST['map_to'])),
	'custom_field_name' => maybe_unserialize(stripslashes($_POST['custom_field_name'])),
	'custom_field_visible' => maybe_unserialize(stripslashes($_POST['custom_field_visible'])),
	'product_image_set_featured' => maybe_unserialize(stripslashes($_POST['product_image_set_featured'])),
	'product_image_skip_duplicates' => maybe_unserialize(stripslashes($_POST['product_image_skip_duplicates'])),
	'post_meta_key' => maybe_unserialize(stripslashes($_POST['post_meta_key'])),
	'user_locale' => maybe_unserialize(stripslashes($_POST['user_locale'])),
	'import_csv_separator' => maybe_unserialize(stripslashes($_POST['import_csv_separator'])),
	'import_csv_hierarchy_separator' => maybe_unserialize(stripslashes($_POST['import_csv_hierarchy_separator']))
);
$current_row_index= $post_data['current_row_index'];
$cbx_selected_products_list= nsn_preg_split('/,/', $post_data['cbx_selected_products'], true);
$cbx_selected_attributes_list= nsn_preg_split('/,/', $post_data['cbx_selected_attributes'], true);
$cbx_selected_users_list= nsn_preg_split('/,/', $post_data['cbx_selected_users'], true);

DebToFileClear("-10 _POST::" . print_r($_POST,true),false);
DebToFileClear("-11 post_data::" . print_r($post_data,true),false);
DebToFileClear("-12 cbx_selected_products_list::" . print_r($cbx_selected_products_list,true),false);
DebToFileClear("-13 cbx_selected_attributes_list::" . print_r($cbx_selected_attributes_list,true),false);
DebToFileClear("-132 cbx_selected_users_list::" . print_r($cbx_selected_users_list,true),false);
DebToFileClear("-1313 current_row_index::" . print_r($current_row_index,true),false);
/*     [cbx_selected_attributes] => color,discount,
    [header_row] => 1
    [cbx_selected_products] => 1,2, */
//return;
if(isset($post_data['uploaded_file_path'])) {

	setlocale(LC_ALL, $post_data['user_locale']);

	$error_messages = array();

	//now that we have the file, grab contents
	$temp_file_path = $post_data['uploaded_file_path'];  //     [uploaded_file_path] => http://local-wp-clear.com/wp-content/uploads/WOOEXIM_Import/DATA_FOR_UPLOAD/wooexim_export_2016_09_24%2006_09_08.csv
	$source_upload_dir_path = $post_data['source_upload_dir_path'];

    //     [uploaded_file_path] => /mnt/_work_sdb8/wwwroot/wp-clear/wp-content/uploads/WOOEXIM_Import/wooexim_export_2016_09_20-04_02_06.csv
    // wooexim_export_2016_09_20 04_02_06_uploaded
    $a= preg_split('/\.csv/',$temp_file_path);
    $temp_file_path_uploaded_dir= '';
    DebToFileClear("-110 a::" . print_r($a,true),false);
    if (count($a)== 2) {
        $temp= $a[0].'_uploaded/';
        DebToFileClear("-110 temp::" . print_r($temp,true),false);
        DebToFileClear("-110 file_exists temp::" . print_r(file_exists($temp),true),false);
        DebToFileClear("-110 is_dir temp::" . print_r(is_dir($temp),true),false);
        if (  /*file_exists($temp) and */ is_dir($temp)  ) {
            DebToFileClear("-110 INSIDE::" . print_r($temp,true),false);
            $temp_file_path_uploaded_dir = $a[0];
        }

    }
    DebToFileClear("-110 temp_file_path_uploaded_dir::" . print_r($temp_file_path_uploaded_dir,true),false);

    $handle = fopen( $temp_file_path, 'r' );
	$import_data = array();

	if ( $handle !== FALSE ) {
		while ( ( $line = fgetcsv($handle, 0, $post_data['import_csv_separator']) ) !== FALSE ) {
			$import_data[] = $line;
		}
		fclose( $handle );
	} else {
		$error_messages[] = __( 'Could not open CSV file.', 'wooexim-import' );
	}

	if(sizeof($import_data) == 0) {
		$error_messages[] = __( 'No data found in CSV file.', 'wooexim-import' );
	}

	//discard header row from data set, if we have one
	if(intval($post_data['header_row']) == 1) array_shift($import_data);

	//slice down our data based on limit and offset params
	$limit = intval($post_data['limit']);
	$offset = intval($post_data['offset']);
	DebToFileClear("-11 offset::" . print_r($offset,true),false);

	//total size of data to import (not just what we're doing on this pass)
	$row_count = sizeof($import_data);
    $attributes_added_row_count= 0;
    $users_added_row_count= 0;
    $attributes_attrs_added_row_count= 0;




	global $wpdb;
	$import_data_rows_to_remove_array= array();
//	$serialized_attributes_data= '';
//	$serialized_users_data= '';
	$row_index= 0;
	$new_users_ids_list = array();
	if ( $offset == 0 ) { // search for defined attributes and users ONLY IN 1ST import part
		foreach ($import_data as $row_id => $next_row) {   // run all rows 1 one by other to search for defined attributes and users

			if (!empty($next_row[0]) and !empty($next_row[1])) {
				if ($next_row[0] == 'attributes_list') {
					$serialized_attributes_data = @unserialize($next_row[1]);
					if ($serialized_attributes_data) { // 1st row are serialized attributes - add them
						$ret = create_attributes($serialized_attributes_data, $cbx_selected_attributes_list);
//					$are_serialized_data = true; //     return array( 'success'=>true,'error_message'=>'', 'attributes_added'=> $attributes_added, 'attributes_attrs_added'=> $attributes_attrs_added ); // attributes were created succesfully
						$import_data_rows_to_remove_array[] = $row_index;
						DebToFileClear('-12301 $ret::' . print_r($ret, true), false);
						if (!empty($ret['success'])) { // attributes were created succesfully
							DebToFileClear('-12301 -1 $ret::' . print_r($ret, true), false);
							$attributes_added_row_count = $ret['attributes_added'];
							$attributes_attrs_added_row_count = $ret['attributes_attrs_added'];
						}

						if (empty($ret['success'])) { // attributes were NOT created succesfully, HAS SOME ERROR
							DebToFileClear('-12301 -2 $ret::' . print_r($ret, true), false);

							echo json_encode(array(
								'current_row_index' => 0,
								'remaining_count' => 0,
								'row_count' => 0,
								'attributes_row_count' => 0,
								'skipped_products_count' => $skipped_products_count,
								'skipped_attributes_count' => $skipped_attributes_count,
								'insert_count' => 0,
								'insert_percent' => 0,
								'inserted_rows' => 0,
								'error_messages' => array('Error while attributes loading : ' . $ret['error_message'] . ". Import flow stopped!"),
								'limit' => 0,
								'new_offset' => 0
							));
							return;

						}   // if (empty($ret['success'])) { // attributes were NOT created succesfully, HAS SOME ERROR
					}
				} //if ( $next_row[0] == 'attributes_list' ) {

				if ($next_row[0] == 'users_list') {
					$serialized_users_data = @unserialize($next_row[1]);
					if ($serialized_users_data) { // 1st row are serialized users - add them
						$ret = create_users($serialized_users_data, $cbx_selected_users_list);
//					$are_serialized_data = true; //     return array( 'success'=>true,'error_message'=>'', 'users_added'=> $users_added // users were created succesfully
						$import_data_rows_to_remove_array[] = $row_index;
						DebToFileClear('-12301 USERS $ret::' . print_r($ret, true), false);
						if (!empty($ret['success'])) { // users were created succesfully
							DebToFileClear('-12301 -1 USERS $ret::' . print_r($ret, true), false);
							$users_added_row_count = $ret['users_added'];
							$new_users_ids_list = $ret['new_users_ids_list'];
                            update_option('wooexim_import_new_users_ids_list', $new_users_ids_list);
//							$_SESSION['new_users_ids_list']= $new_users_ids_list;
						}

						if (empty($ret['success'])) { // users were NOT created succesfully, HAS SOME ERROR
							DebToFileClear('-12301 -2 USERS $ret::' . print_r($ret, true), false);

							echo json_encode(array(
								'current_row_index' => 0,
								'remaining_count' => 0,
								'row_count' => 0,
								'users_added_row_count' => 0,
								'insert_count' => 0,
								'insert_percent' => 0,
								'inserted_rows' => 0,
								'error_messages' => array('Error while users loading : ' . $ret['error_message'] . ". Import flow stopped!"),
								'limit' => 0,
								'new_offset' => 0
							));
							return;

						}   // if (empty($ret['success'])) { // users were NOT created succesfully, HAS SOME ERROR
					}
				} //if ( $next_row[0] == 'users_list' ) {


				$row_index++;
			} // if ( !empty($next_row[0]) and !empty($next_row[1]) ) {
			if (!empty($serialized_attributes_data) and !empty($serialized_users_data)) { // defined attributes and users we found
				break;
			}
		} // foreach($import_data as $row_id => $next_row) {   // run all rows 1 one by other to search for defined attributes and users
	} // if ( $offset == 0 ) { // search for defined attributes and users ONLY IN 1ST import part
	else { // if ( $offset > 0 ) {
//		$new_users_ids_list= !empty($_SESSION['new_users_ids_list']) ? $_SESSION['new_users_ids_list'] : array();
        $new_users_ids_list= get_option( 'wooexim_import_new_users_ids_list', array() );

        DebToFileClear('RRRRR::' . print_r($new_users_ids_list, true), false);

	}




	DebToFileClear("-1090 import_data_rows_to_remove_array::" . print_r($import_data_rows_to_remove_array,true),false);

	reset($import_data);
	foreach( $import_data_rows_to_remove_array as $next_import_data_row_to_remove ) {
	    unset( $import_data[$next_import_data_row_to_remove] );
	}

	DebToFileClear("-1100 import_data::" . print_r($import_data,true),false);
	if($limit > 0 || $offset > 0) {
		$import_data = array_slice($import_data, $offset , ($limit > 0 ? $limit : null), true);
	}

	//a few stats about the current operation to send back to the browser.
	$rows_remaining = ($row_count - ($offset + $limit)) > 0 ? ($row_count - ($offset + $limit)) : 0;
	$insert_count = ($row_count - $rows_remaining);
	$insert_percent = number_format(($insert_count / $row_count) * 100, 1);

	//array that will be sent back to the browser with info about what we inserted.
	$inserted_rows = array();
    DebToFileClear("-12 post_data::" . print_r($inserted_rows,true),false);

	//this is where the fun begins
//    $row_index= $current_row_index;
    $row_index= 0;
    $skipped_products_count= 0;
    $skipped_attributes_count= 0;

	foreach($import_data as $row_id => $next_row) {   // run all rows 1 one by other
        $row_index++;
        if (!in_array($row_index, $cbx_selected_products_list)) {
            $skipped_products_count++;
            DebToFileClear('-1201 ROW WAS SKIPPED $row_id::' . print_r($row_id,true).'  $row_index::' . print_r($row_index,true),false);
            continue; // next row was not checked to import
        }

        DebToFileClear('-1201 NOT SKIPPED $row_id::' . print_r($row_id,true).'  $row_index::' . print_r($row_index,true),false);
//        DebToFileClear('-122 $row::' . print_r($row,true),false);
//        DebToFileClear('-123 $row[0]::' . print_r($row[0],true),false);

//        $are_serialized_data = false;



        DebToFileClear('BEFORE  $offset::'.$offset.'  $new_users_ids_list::' . print_r($new_users_ids_list,true),false);
//        if ( $are_serialized_data ) { // these are $serialized common data - we do not add them as products rows
//            continue;
//        }

        //unset new_post_id
		$new_post_id = null;

		//array of imported post data
		$new_post = array();

		//set some defaults in case the post doesn't exist
		$new_post_defaults = array();
		$new_post_defaults['post_type'] = 'product';
		$new_post_defaults['post_status'] = 'publish';
		$new_post_defaults['post_title'] = '';
		$new_post_defaults['post_content'] = '';
		$new_post_defaults['menu_order'] = 0;

		//array of imported post_meta
		$new_post_meta = array();

		//default post_meta to use if the post doesn't exist
		$new_post_meta_defaults = array();
		$new_post_meta_defaults['_visibility'] = 'visible';
		$new_post_meta_defaults['_featured'] = 'no';
		$new_post_meta_defaults['_weight'] = 0;
		$new_post_meta_defaults['_length'] = 0;
		$new_post_meta_defaults['_width'] = 0;
		$new_post_meta_defaults['_height'] = 0;
		$new_post_meta_defaults['_sku'] = '';
		$new_post_meta_defaults['_stock'] = '';
		$new_post_meta_defaults['_sale_price'] = '';
		$new_post_meta_defaults['_sale_price_dates_from'] = '';
		$new_post_meta_defaults['_sale_price_dates_to'] = '';
		$new_post_meta_defaults['_tax_status'] = 'taxable';
		$new_post_meta_defaults['_tax_class'] = '';
		$new_post_meta_defaults['_purchase_note'] = '';
		$new_post_meta_defaults['_downloadable'] = 'no';
		$new_post_meta_defaults['_virtual'] = 'no';
		$new_post_meta_defaults['_backorders'] = 'no';

		//stores tax and term ids so we can associate our product with terms and taxonomies
		//this is a multidimensional array
		//format is: array( 'tax_name' => array(1, 3, 4), 'another_tax_name' => array(5, 9, 23) )
		$new_post_terms = array();

		//a list of woocommerce "custom fields" to be added to product.
		$new_post_custom_fields = array();
		$new_post_custom_field_count = 0;

		//a list of image URLs to be downloaded.
		$new_post_image_urls = array();

		//a list of image paths to be added to the database.
		//$new_post_image_urls will be added to this array later as paths once they are downloaded.
		$new_post_image_paths = array();
		$result_comments_list = array();

		//keep track of any errors or messages generated during post insert or image downloads.
		$new_post_errors = array();
		$new_post_messages = array();

		//track whether or not the post was actually inserted.
		$new_post_insert_success = false;
        $product_attributes_list= array();

		foreach($next_row as $key => $col) { // run all columns in 1 row
			$map_to = $post_data['map_to'][$key];

			//skip if the column is blank.
			//useful when two CSV cols are mapped to the same product field.
			//you would do this to merge two columns in your CSV into one product field.
			if(strlen($col) == 0) {
				continue;
			}

			//validate col value if necessary
            DebToFileClear("-13 map_to::" . print_r($map_to,true),false);
			switch($map_to) {
				case '_downloadable':
				case '_virtual':
				case '_manage_stock':
				case '_featured':
					if(!in_array($col, array('yes', 'no'))) continue;
					break;

				case 'comment_status':
				case 'ping_status':
					if(!in_array($col, array('open', 'closed'))) continue;
					break;

				case '_visibility':
					if(!in_array($col, array('visible', 'catalog', 'search', 'hidden'))) continue;
					break;

				case '_stock_status':
					if(!in_array($col, array('instock', 'outofstock'))) continue;
					break;

				case '_backorders':
					if(!in_array($col, array('yes', 'no', 'notify'))) continue;
					break;

				case '_tax_status':
					if(!in_array($col, array('taxable', 'shipping', 'none'))) continue;
					break;

				case '_product_type':
					if(!in_array($col, array('simple', 'variable', 'grouped', 'external'))) continue;
					break;
			}

			//prepare the col value for insertion into the database
            /* 				case 'product_image_by_url':
					$image_urls = explode('|', $col);
                    DebToFileClear("-14 image_urls::" . print_r($image_urls,true),false);
					if(is_array($image_urls)) {
						$new_post_image_urls = array_merge($new_post_image_urls, $image_urls);
					}
					break;

				case 'product_image_by_path': */
            if ( $map_to == 'product_image_by_url' ) {
                $map_to= 'product_image_by_path';
            }
			switch($map_to) {

				//post fields
				case 'post_title':
				case 'post_content':
				case 'post_excerpt':
				case 'post_status':
				case 'comment_status':
				case 'ping_status':
					$new_post[$map_to] = $col;
					break;

				//integer post fields
				case 'menu_order':
					//remove any non-numeric chars
					$col_value = preg_replace("/[^0-9]/", "", $col);
					if($col_value == "") continue;

					$new_post[$map_to] = $col_value;
					break;

				//integer postmeta fields
				case '_stock':
				case '_download_expiry':
				case '_download_limit':
					//remove any non-numeric chars
					$col_value = preg_replace("/[^0-9]/", "", $col);
					if($col_value == "") continue;

					$new_post_meta[$map_to] = $col_value;
					break;

				//float postmeta fields
				case '_weight':
				case '_length':
				case '_width':
				case '_height':
				case '_regular_price':
				case '_sale_price':
					//remove any non-numeric chars except for '.'
					$col_value = preg_replace("/[^0-9.]/", "", $col);
					if($col_value == "") continue;

					$new_post_meta[$map_to] = $col_value;
					break;

				//sku
				case '_sku':
					$col_value = trim($col);
					if($col_value == "") continue;

					$new_post_meta[$map_to] = $col_value;
					break;

				//file_path(s)
				case '_file_path':
				case '_file_paths':
					if(!is_array($new_post_meta['_file_paths'])) $new_post_meta['_file_paths'] = array();

					$new_post_meta['_file_paths'][md5($col)] = $col;
					break;

				//all other postmeta fields
				case '_tax_status':
				case '_tax_class':
				case '_visibility':
				case '_featured':
				case '_downloadable':
				case '_virtual':
				case '_stock_status':
				case '_backorders':
				case '_manage_stock':
				case '_button_text':
				case '_product_url':
					$new_post_meta[$map_to] = $col;
					break;

				case 'post_meta':
					$new_post_meta[$post_data['post_meta_key'][$key]] = $col;
					break;

				case '_product_type':
					//product_type is saved as both post_meta and via a taxonomy.
					$new_post_meta[$map_to] = $col;

					$term_name = $col;
					$tax = 'product_type';
					$term = get_term_by('name', $term_name, $tax, 'ARRAY_A');

					//if we got a term, save the id so we can associate
					if(is_array($term)) {
						$new_post_terms[$tax][] = intval($term['term_id']);
					}

					break;

				case 'product_cat_by_name':
				case 'product_tag_by_name':
				case 'product_shipping_class_by_name':
					$tax = str_replace('_by_name', '', $map_to);
					$term_paths = explode('|', $col);
					foreach($term_paths as $term_path) {

						$term_names = explode($post_data['import_csv_hierarchy_separator'], $term_path);
						$term_ids = array();

						for($depth = 0; $depth < count($term_names); $depth++) {

							$term_parent = ($depth > 0) ? $term_ids[($depth - 1)] : '';
							$term = term_exists($term_names[$depth], $tax, $term_parent);

							//if term does not exist, try to insert it.
							if( $term === false || $term === 0 || $term === null) {
								$insert_term_args = ($depth > 0) ? array('parent' => $term_ids[($depth - 1)]) : array();
								$term = wp_insert_term($term_names[$depth], $tax, $insert_term_args);
								delete_option("{$tax}_children");
							}

							if(is_array($term)) {
								$term_ids[$depth] = intval($term['term_id']);
							} else {
								//uh oh.
								$new_post_errors[] = "Couldn't find or create {$tax} with path {$term_path}.";
								break;
							}
						}

						//if we got a term at the end of the path, save the id so we can associate
						if(array_key_exists(count($term_names) - 1, $term_ids)) {
							$new_post_terms[$tax][] = $term_ids[(count($term_names) - 1)];
						}
					}
					break;

				case 'product_cat_by_id':
				case 'product_tag_by_id':
				case 'product_shipping_class_by_id':
					$tax = str_replace('_by_id', '', $map_to);
					$term_ids = explode('|', $col);
					foreach($term_ids as $term_id) {
						//$term = get_term_by('id', $term_id, $tax, 'ARRAY_A');
						$term = term_exists($term_id, $tax);

						//if we got a term, save the id so we can associate
						if(is_array($term)) {
							$new_post_terms[$tax][] = intval($term['term_id']);
						} else {
							$new_post_errors[] = "Couldn't find {$tax} with ID {$term_id}.";
						}

					}
					break;

				case 'custom_field':
                    DebToFileClear("custom_field -150 col::" . print_r($col,true),false);
					$field_name = $post_data['custom_field_name'][$key];
					$field_slug = sanitize_title($field_name);
					$visible = intval($post_data['custom_field_visible'][$key]);

					$new_post_custom_fields[$field_slug] = array (
						"name" => $field_name,
						"value" => $col,
						"position" => $new_post_custom_field_count++,
						"is_visible" => $visible,
						"is_variation" => 0,
						"is_taxonomy" => 0
					);
					break;

				case 'product_image_by_url':
					$image_urls = explode('|', $col);
                    DebToFileClear("-14 image_urls::" . print_r($image_urls,true),false);
					if(is_array($image_urls)) {
						$new_post_image_urls = array_merge($new_post_image_urls, $image_urls);
					}
					break;

				case 'product_image_by_path':

                    $image_paths = explode('|', $col);
                    DebToFileClear("-15 image_paths::" . print_r($image_paths,true),false);
                    DebToFileClear('-151 $source_upload_dir_path::' . print_r($source_upload_dir_path,true),false);

					if(is_array($image_paths)) {
						foreach($image_paths as $image_path) {
                            DebToFileClear('-1512 $image_path::' . print_r($image_path,true),false);
                            if ( !file_exists($image_path) and !is_dir($image_path) ) {
                                $image_path= $source_upload_dir_path . DIRECTORY_SEPARATOR . $image_path;
                            }
                            DebToFileClear('-1512 $image_path::' . print_r($image_path,true),false);
							$new_post_image_paths[] = array(
								'path' => $temp_file_path_uploaded_dir.$image_path,
								'source' => /* $temp_file_path_uploaded_dir. */ $image_path
							);
						}
					}
					break;

				case 'product_attributes_list':
                    DebToFileClear("-151 col::" . print_r($col,true),false);
                    $product_attributes_list= unserialize($col);
                    DebToFileClear("-1511 product_attributes_list::" . print_r($product_attributes_list,true),false);

//					$image_paths = explode('|', $col);
//                    DebToFileClear("-152 image_paths::" . print_r($image_paths,true),false);
//					if(is_array($image_paths)) {
//						foreach($image_paths as $image_path) {
//							$new_post_image_paths[] = array(
//								'path' => $temp_file_path_uploaded_dir.$image_path,
//								'source' => /* $temp_file_path_uploaded_dir. */ $image_path
//							);
//						}
//					}

					break;


                case 'result_comments_list':
					DebToFileClear("-1401 col::" . print_r($col,true),false);

                    $result_comments_list = unserialize($col);
//                    $result_comments_list = @unserialize($col);
					DebToFileClear("-1402 result_comments_list::" . print_r($result_comments_list,true),false);
					if ( empty($result_comments_list) ) $result_comments_list= array();
					DebToFileClear("-1402!!! result_comments_list::" . print_r($result_comments_list,true),false);

//                    $image_paths = explode('|', $col);
//                    DebToFileClear("-15 image_paths::" . print_r($image_paths,true),false);
//                    DebToFileClear('-151 $source_upload_dir_path::' . print_r($source_upload_dir_path,true),false);
//
//                    if(is_array($image_paths)) {
//                        foreach($image_paths as $image_path) {
//                            DebToFileClear('-1512 $image_path::' . print_r($image_path,true),false);
//                            if ( !file_exists($image_path) and !is_dir($image_path) ) {
//                                $image_path= $source_upload_dir_path . DIRECTORY_SEPARATOR . $image_path;
//                            }
//                            DebToFileClear('-1512 $image_path::' . print_r($image_path,true),false);
//                            $new_post_image_paths[] = array(
//                                'path' => $temp_file_path_uploaded_dir.$image_path,
//                                'source' => /* $temp_file_path_uploaded_dir. */ $image_path
//                            );
//                        }
//                    }
                    break;


                case 'result_comment_rating_average':
//                    $image_paths = explode('|', $col);
//                    DebToFileClear("-15 image_paths::" . print_r($image_paths,true),false);
//                    DebToFileClear('-151 $source_upload_dir_path::' . print_r($source_upload_dir_path,true),false);
//
//                    if(is_array($image_paths)) {
//                        foreach($image_paths as $image_path) {
//                            DebToFileClear('-1512 $image_path::' . print_r($image_path,true),false);
//                            if ( !file_exists($image_path) and !is_dir($image_path) ) {
//                                $image_path= $source_upload_dir_path . DIRECTORY_SEPARATOR . $image_path;
//                            }
//                            DebToFileClear('-1512 $image_path::' . print_r($image_path,true),false);
//                            $new_post_image_paths[] = array(
//                                'path' => $temp_file_path_uploaded_dir.$image_path,
//                                'source' => /* $temp_file_path_uploaded_dir. */ $image_path
//                            );
//                        }
//                    }
                    break;



            }

		} // foreach($next_row as $key => $col) { // run all columns in 1 row
//break;



		//set some more post_meta and parse things as appropriate

		//set price to sale price if we have one, regular price otherwise
		$new_post_meta['_price'] = array_key_exists('_sale_price', $new_post_meta) ? ( !empty($new_post_meta['_sale_price']) ? $new_post_meta['_sale_price'] : 0 ) : ( !empty($new_post_meta['_regular_price']) ? $new_post_meta['_regular_price'] : 0 );

		//check and set some inventory defaults
		if(array_key_exists('_stock', $new_post_meta)) {

			//set _manage_stock to yes if not explicitly set by CSV
			if(!array_key_exists('_manage_stock', $new_post_meta)) {
				$new_post_meta['_manage_stock'] = 'yes';
			}
			//set _stock_status based on _stock if not explicitly set by CSV
			if(!array_key_exists('_stock_status', $new_post_meta)) {
				//set to instock if _stock is > 0, otherwise set to outofstock
				$new_post_meta['_stock_status'] = (intval($new_post_meta['_stock']) > 0) ? 'instock' : 'outofstock';
			}

		} else {

			//set _manage_stock to no if not explicitly set by CSV
			if(!array_key_exists('_manage_stock', $new_post_meta)) $new_post_meta['_manage_stock'] = 'no';
		}
        DebToFileClear("-16 new_post_meta::" . print_r($new_post_meta,true),false);

		//try to find a product with a matching SKU
		$existing_product = null;
		if(array_key_exists('_sku', $new_post_meta) && !empty($new_post_meta['_sku']) > 0) {
			$existing_post_query = array(
				'numberposts' => 1,
				'meta_key' => '_sku',
				'meta_query' => array(
					array(
						'key'=>'_sku',
						'value'=> $new_post_meta['_sku'],
						'compare' => '='
					)
				),
				'post_type' => 'product');
			$existing_posts = get_posts($existing_post_query);
            DebToFileClear("-161 existing_post_query::" . print_r($existing_post_query,true),false);
			if(is_array($existing_posts) && sizeof($existing_posts) > 0) {
				$existing_product = array_shift($existing_posts);
			}
		}

        DebToFileClear("-17 existing_product::" . print_r($existing_product,true),false);
        if(strlen($new_post['post_title']) > 0 || $existing_product !== null) {

			$new_post['post_title']= $new_post['post_title'];  // . ' : ' . count($result_comments_list);
			//insert/update product
			if($existing_product !== null) {
				$new_post_messages[] = sprintf( __( 'Updating product with ID %s.', 'wooexim-import' ), $existing_product->ID );

				$new_post['ID'] = $existing_product->ID;
				$new_post_id = wp_update_post($new_post);
                DebToFileClear("-1701 new_post::" . print_r($new_post,true),false);
			} else {

				//merge in default values since we're creating a new product from scratch
				$new_post = array_merge($new_post_defaults, $new_post);
				$new_post_meta = array_merge($new_post_meta_defaults, $new_post_meta);

                DebToFileClear("-1702 new_post::" . print_r($new_post,true),false);
				$new_post_id = wp_insert_post($new_post, true);
			}
            DebToFileClear("-171 new_post_id::" . print_r($new_post_id,true),false);
            DebToFileClear("-172 new_post::" . print_r($new_post,true),false);

            $wp_error_text=            is_wp_error($new_post_id);
            DebToFileClear("-173 wp_error_text::" . print_r($wp_error_text,true),false);
			if($wp_error_text) {
				$new_post_errors[] = sprintf( __( 'Couldn\'t insert product with name %s.', 'wooexim-import' ), $new_post['post_title'] );
			} elseif($new_post_id == 0) {
				$new_post_errors[] = sprintf( __( 'Couldn\'t update product with ID %s.', 'wooexim-import' ), $new_post['ID'] );
			} else {
				//insert successful!
				$new_post_insert_success = true;

				//set post_meta on inserted post
				foreach($new_post_meta as $meta_key => $meta_value) {
					add_post_meta($new_post_id, $meta_key, $meta_value, true) or
						update_post_meta($new_post_id, $meta_key, $meta_value);
				}

				//set _product_attributes postmeta to the custom fields array. WP will serialize it for us.
				//first, work on existing attributes
				if($existing_product !== null) {
					$existing_product_attributes = get_post_meta($new_post_id, '_product_attributes', true);
					if(is_array($existing_product_attributes)) {
						//set the 'position' value for all *new* attributes.
						$max_position = 0;
						foreach($existing_product_attributes as $field_slug => $field_data) {
							$max_position = max(intval($field_data['position']), $max_position);
						}
						foreach($new_post_custom_fields as $field_slug => $field_data) {
							if(!array_key_exists($field_slug, $existing_product_attributes)) {
								$new_post_custom_fields[$field_slug]['position'] = ++$max_position;
							}
						}
						$new_post_custom_fields = array_merge($existing_product_attributes, $new_post_custom_fields);
					}
				}
				add_post_meta($new_post_id, '_product_attributes', $new_post_custom_fields, true) or
					update_post_meta($new_post_id, '_product_attributes', $new_post_custom_fields);


				//set post terms on inserted post
				foreach($new_post_terms as $tax => $term_ids) {
					wp_set_object_terms($new_post_id, $term_ids, $tax);
				}


				DebToFileClear("-147 new_post[post_title]::" . print_r($new_post['post_title'],true),false);
				DebToFileClear("-148 new_post_id::" . print_r($new_post_id,true),false);
				DebToFileClear("-149 result_comments_list::" . print_r($result_comments_list,true),false);
				if ( count($result_comments_list) > 0 ) { // 1st row are serialized attributes - add them
					DebToFileClear("-15 new_users_ids_list::" . print_r($new_users_ids_list,true),false);
					upload_comments( $new_post_id, $result_comments_list, $new_users_ids_list );
				}

				//figure out where the uploads folder lives
				$wp_upload_dir = wp_upload_dir();

				//grab product images
                DebToFileClear("-19 new_post_image_urls::" . print_r($new_post_image_urls,true),false);
				foreach($new_post_image_urls as $image_index => $image_url) {

					//convert space chars into their hex equivalent.
					//thanks to github user 'becasual' for submitting this change
					$image_url = str_replace(' ', '%20', trim($image_url));

					//do some parsing on the image url so we can take a look at
					//its file extension and file name
					$parsed_url = parse_url($image_url);
					$pathinfo = pathinfo($parsed_url['path']);
                    DebToFileClear("-190  pathinfo::" . print_r($pathinfo,true),false);

					//If our 'image' file doesn't have an image file extension, skip it.
					$allowed_extensions = array('jpg', 'jpeg', 'gif', 'png');
					$image_ext = !empty($pathinfo['extension']) ? strtolower($pathinfo['extension']) : '';
                    DebToFileClear("-191  image_ext::" . print_r($image_ext,true),false);
					if(!in_array($image_ext, $allowed_extensions)) {
						$new_post_errors[] = sprintf( __( 'A valid file extension wasn\'t found in %s. Extension found was %s. Allowed extensions are: %s.', 'wooexim-import' ), $image_url, $image_ext, implode( ',', $allowed_extensions ) );
						continue;
					}

					//figure out where we're putting this thing.
					$dest_filename = wp_unique_filename( $wp_upload_dir['path'], $pathinfo['basename'] );
					$dest_path = $wp_upload_dir['path'] . '/' . $dest_filename;
					$dest_url = $wp_upload_dir['url'] . '/' . $dest_filename;

					//download the image to our local server.
					// if allow_url_fopen is enabled, we'll use that. Otherwise, we'll try cURL
					if(ini_get('allow_url_fopen')) {
						//attempt to copy() file show error on failure.
                        DebToFileClear("-20 image_url::" . print_r($image_url,true),false);
                        DebToFileClear("-21 dest_path::" . print_r($dest_path,true),false);
						if( ! @copy($image_url, $dest_path)) {
							$http_status = $http_response_header[0];
							$new_post_errors[] = sprintf( __( '%s encountered while attempting to download %s', 'wooexim-import' ), $http_status, $image_url );
						}

					} elseif(function_exists('curl_init')) {
						$ch = curl_init($image_url);
						$fp = fopen($dest_path, "wb");

						$options = array(
							CURLOPT_FILE => $fp,
							CURLOPT_HEADER => 0,
							CURLOPT_FOLLOWLOCATION => 1,
							CURLOPT_TIMEOUT => 60); // in seconds

						curl_setopt_array($ch, $options);
						curl_exec($ch);
						$http_status = intval(curl_getinfo($ch, CURLINFO_HTTP_CODE));
						curl_close($ch);
						fclose($fp);

						//delete the file if the download was unsuccessful
						if($http_status != 200) {
							unlink($dest_path);
							$new_post_errors[] = sprintf( __( 'HTTP status %s encountered while attempting to download %s', 'wooexim-import' ), $http_status, $image_url );
						}
					} else {
						//well, damn. no joy, as they say.
						$error_messages[] = sprintf( __( 'Looks like %s is off and %s is not enabled. No images were imported.', 'wooexim-import' ), '<code>allow_url_fopen</code>', '<code>cURL</code>'  );
						break;
					}

					//make sure we actually got the file.
					if(!file_exists($dest_path)) {
						$new_post_errors[] = sprintf( __( 'Couldn\'t download file %s.', 'wooexim-import' ), $image_url );
						continue;
					}

					//whew. are we there yet?
					$new_post_image_paths[] = array(
						'path' => $dest_path,
						'source' => $image_url
					);
				}
                DebToFileClear("-22 new_post_image_paths::" . print_r($new_post_image_paths,true),false);

				$image_gallery_ids = array();

				foreach($new_post_image_paths as $image_index => $dest_path_info) {

					//check for duplicate images, only for existing products
					if($existing_product !== null && intval($post_data['product_image_skip_duplicates'][$key]) == 1) {
						$existing_attachment_query = array(
							'numberposts' => 1,
							'meta_key' => '_import_source',
							'post_status' => 'inherit',
							'post_parent' => $existing_product->ID,
							'meta_query' => array(
								array(
									'key'=>'_import_source',
									'value'=> $dest_path_info['source'],
									'compare' => '='
								)
							),
							'post_type' => 'attachment');
						$existing_attachments = get_posts($existing_attachment_query);
						if(is_array($existing_attachments) && sizeof($existing_attachments) > 0) {
							//we've already got this file.
							$new_post_messages[] = sprintf( __( 'Skipping import of duplicate image %s.', 'wooexim-import' ), $dest_path_info['source'] );
							continue;
						}
					}

                    DebToFileClear("-221 dest_path_info::" . print_r($dest_path_info,true),false);


					//make sure we actually got the file.
					if(!file_exists($dest_path_info['path'])) {
						$new_post_errors[] = sprintf( __( 'Couldn\'t find local file %s.', 'wooexim-import' ), $dest_path_info['path'] );
						continue;
					}

					$dest_url = str_ireplace(ABSPATH, home_url('/'), $dest_path_info['path']);
					$path_parts = pathinfo($dest_path_info['path']);

					//add a post of type 'attachment' so this item shows up in the WP Media Library.
					//our imported product will be the post's parent.
					$wp_filetype = wp_check_filetype($dest_path_info['path']);
					$attachment = array(
						'guid' => $dest_url,
						'post_mime_type' => $wp_filetype['type'],
						'post_title' => preg_replace('/\.[^.]+$/', '', $path_parts['filename']),
						'post_content' => '',
						'post_status' => 'inherit'
					);
					$attachment_id = wp_insert_attachment( $attachment, $dest_path_info['path'], $new_post_id );
					// you must first include the image.php file
					// for the function wp_generate_attachment_metadata() to work
					require_once(ABSPATH . 'wp-admin/includes/image.php');
					$attach_data = wp_generate_attachment_metadata( $attachment_id, $dest_path_info['path'] );
					wp_update_attachment_metadata( $attachment_id, $attach_data );

					//keep track of where the attachment came from so we don't import duplicates later
					add_post_meta($attachment_id, '_import_source', $dest_path_info['source'], true) or
						update_post_meta($attachment_id, '_import_source', $dest_path_info['source']);

					//set the image as featured if it is the first image in the set AND
					//the user checked the box on the preview page.
					if($image_index == 0 && intval($post_data['product_image_set_featured'][$key]) == 1) {
						update_post_meta($new_post_id, '_thumbnail_id', $attachment_id);
					} else {
						$image_gallery_ids[] = $attachment_id;
					}
				}

                DebToFileClear("-23 image_gallery_ids::" . print_r($image_gallery_ids,true),false);
				if(count($image_gallery_ids) > 0) {
					update_post_meta($new_post_id, '_product_image_gallery', implode(',', $image_gallery_ids));
				}


                $product_attributes_values = Array();
                foreach( $product_attributes_list as $next_key => $next_product_attribute) { // all attributes of product $new_post_id

                    if ( !in_array($next_key, $cbx_selected_attributes_list) ) {
                        continue; // $next_attribute was was not checked
                    }


                    $term_taxonomy_ids = wp_set_object_terms( $new_post_id, $next_product_attribute, 'pa_' . $next_key, true );
//                    DebToFileClear("-15110 term_taxonomy_ids::" . print_r($term_taxonomy_ids,true),false);
//                    DebToFileClear("-15111 next_key::" . print_r($next_key,true),false);
//                    DebToFileClear("-15111 next_product_attribute::" . print_r($next_product_attribute,true),false);
//
                    $product_attributes_values['pa_' . $next_key] = Array(
                        'name'=> 'pa_' . $next_key,
                        'value'=> $next_product_attribute,
                        'is_visible' => '1',
                        'is_variation' => '1',
                        'is_taxonomy' => '1'
                    );
                } // foreach( $product_attributes_list as $next_key => $next_product_attribute) { // all attributes of product $new_post_id
//                DebToFileClear("-15112 thedata::" . print_r($product_attributes_values,true),false);
                if ( count($product_attributes_values) > 0 ) {
                    update_post_meta($new_post_id, '_product_attributes', $product_attributes_values);
                }

//                $new_post_id
			}

		} else {
			$new_post_errors[] = __( 'Skipped import of product without a name', 'wooexim-import' );
		}

		//this is returned back to the results page.
		//any fields that should show up in results should be added to this array.
		$inserted_rows[] = array(
			'row_id' => $row_id,
			'post_id' => $new_post_id ? $new_post_id : '',
			'name' => $new_post['post_title'] ? $new_post['post_title'] : '',
			'sku' => $new_post_meta['_sku'] ? $new_post_meta['_sku'] : '',
			'price' => $new_post_meta['_price'] ? $new_post_meta['_price'] : '',
			'has_errors' => (sizeof($new_post_errors) > 0),
			'errors' => $new_post_errors,
			'has_messages' => (sizeof($new_post_messages) > 0),
			'messages' => $new_post_messages,
			'success' => $new_post_insert_success
		);
	} // foreach($import_data as $row_id => $next_row) {   // run all rows 1 one by other


}

echo json_encode(array(
	'current_row_index' => $current_row_index,
	'remaining_count' => $rows_remaining,
	'row_count' => ( $row_count - count($import_data_rows_to_remove_array) ),
	'attributes_added_row_count' => $attributes_added_row_count,
	'users_added_row_count' => $users_added_row_count,
	'attributes_attrs_added_row_count' => $attributes_attrs_added_row_count,
	'insert_count' => ( $insert_count  - count($import_data_rows_to_remove_array) ),
	'insert_percent' => $insert_percent,
	'inserted_rows' => $inserted_rows,

    'skipped_products_count' => $skipped_products_count,
    'skipped_attributes_count' => $skipped_attributes_count,
    'new_users_ids_list' => $new_users_ids_list,

	'error_messages' => $error_messages,
	'limit' => $limit,
	'new_offset' => ($limit + $offset + count($import_data_rows_to_remove_array) )
));

function create_attributes($serialized_attributes_list, $cbx_selected_attributes_list)
{
//    return array( 'success'=>true,'error_message'=>'', 'attributes_added'=> 0, 'attributes_attrs_added'=> 0 ); // for debugging
    global $wpdb;
    $attributes_added= 0;
    $attributes_attrs_added= 0;
    if ($serialized_attributes_list !== false) {
        DebToFileClear('-12302 ::' . print_r(1,true),false);
        if ( !empty($serialized_attributes_list) and is_array($serialized_attributes_list) ) {
            foreach( $serialized_attributes_list as $next_key=>$next_attribute ) { // add all attributes in array
                DebToFileClear('-123 1 $next_attribute::' . print_r($next_attribute,true),false);
                if ( !in_array($next_attribute['attribute_name'], $cbx_selected_attributes_list) ) {
                    continue; // $next_attribute was was not checked
                }

                $wc_attribute_taxonomies= get_option('_transient_wc_attribute_taxonomies',array());
                DebToFileClear('-12303 $wc_attribute_taxonomies::' . print_r($wc_attribute_taxonomies,true),false);

                if ( taxonomy_exists( 'pa_'.$next_attribute['attribute_name'] ) ) {
                    return array('success'=>false,'error_message'=>'Attribute taxoconomy '. 'pa_' . $next_attribute['attribute_name'] . ' already exists !'); // Exit by error with message
                }

                    $next_attribute_attrs= array();
                if ( isset($next_attribute['attrs']) and is_array($next_attribute['attrs']) ) {
                    $next_attribute_attrs= $next_attribute['attrs'];
                }
                if ( isset($next_attribute['attribute_id']) ) {
                    unset($next_attribute['attribute_id']);
                    unset($next_attribute['attrs']);
                }
//                DebToFileClear('-123 2 $next_attribute::' . print_r($next_attribute,true),false);
                $wpdb->insert($wpdb->prefix . 'woocommerce_attribute_taxonomies', $next_attribute);  // step 1

                $last_woocommerce_attribute_taxonomies_id = $wpdb->insert_id;
                if ($last_woocommerce_attribute_taxonomies_id) {
                    $attributes_added++;
                }
                $next_attribute['attribute_id']= $last_woocommerce_attribute_taxonomies_id;
//                DebToFileClear('-123 $last_woocommerce_attribute_taxonomies_id::' . print_r($last_woocommerce_attribute_taxonomies_id,true),false);

                $wc_attribute_taxonomies[]=  (object)$next_attribute;
//                DebToFileClear('-123 BEFORE WRITING  $wc_attribute_taxonomies::' . print_r($wc_attribute_taxonomies,true),false);
                $update_option_ret= update_option( '_transient_wc_attribute_taxonomies', $wc_attribute_taxonomies );   // step 2
//                DebToFileClear('-124 $update_option_ret::' . print_r($update_option_ret, true), false);
//                        DebToFileClear('-124 $inserted_term::' . print_r($inserted_term, true), false);

                if ( isset($next_attribute_attrs) and is_array($next_attribute_attrs) ) {
                    foreach( $next_attribute_attrs as $next_attr_key=>$next_attr_struct ) {
                        DebToFileClear('-120 $next_attr_struct::' . print_r($next_attr_struct, true), false);

                        $wpdb->insert( $wpdb->prefix . 'terms', array( 'name'=>$next_attr_struct['name'], 'slug'=>$next_attr_struct['slug'], 'term_group' => 0 ) ) ;  // step 3
                        $last_terms_id = $wpdb->insert_id;
                        DebToFileClear('-1202 $last_terms_id::' . print_r($last_terms_id, true), false);

                        $wpdb->insert( $wpdb->prefix . 'term_taxonomy', array(  'term_id'=> $last_terms_id, 'taxonomy'=> 'pa_' . $next_attribute['attribute_name'], 'description'=> '', 'parent'=> 0, 'count'=> 0  ) ); // step 4


                        $last_term_taxonomy_id = $wpdb->insert_id;
                        if ($last_term_taxonomy_id) {
                            $attributes_attrs_added++;
                        }
                        $wpdb->insert( $wpdb->prefix . 'termmeta', array( 'term_id'=> $last_terms_id, 'meta_key'=>'order_pa_'.$next_attribute['attribute_name'], 'meta_value'=> 0 ) );  // step 1
                        $last_termmeta_id = $wpdb->insert_id;
                    }
                }

                flush_rewrite_rules();
            } // foreach( $serialized_attributes_list as $next_key=>$next_attribute ) { // add all attributes in array

        }
    }  // if ($serialized_attributes_list !== false) {
    return array( 'success'=>true,'error_message'=>'', 'attributes_added'=> $attributes_added, 'attributes_attrs_added'=> $attributes_attrs_added ); // attributes were created succesfully
} // function create_attributes($serialized_attributes_list)

function create_users($serialized_users_list, $cbx_selected_users_list)
{
	$users_added = 0;
	$temp_new_users_ids_list= array();
	DebToFileClear('create_users -1 $cbx_selected_users_list' . print_r($cbx_selected_users_list, true), false);

	if ($serialized_users_list !== false) {
		DebToFileClear('create_users -123 ::' . print_r(1, true), false);
		if (!empty($serialized_users_list) and is_array($serialized_users_list)) {

			foreach ($serialized_users_list as $next_key => $next_serialized_user) { // add all users in array
				if (!in_array($next_serialized_user['ID'], $cbx_selected_users_list)) {
					DebToFileClear('SKIPPED -123 1 $next_serialized_user::' . print_r($next_serialized_user, true), false);
					continue;// skip unchecked users
			    }
				DebToFileClear('NOT SKIPPED -123 1 $next_serialized_user::' . print_r($next_serialized_user, true), false);
				$user_login= $next_serialized_user['user_login'];
				$user_email= $next_serialized_user['user_email'];
				if ( username_exists($user_login) == null and email_exists($user_email) == null ) {

					$password = '111111';
					$newly_created_user_id = wp_create_user($user_login, $password, $user_email);
					DebToFileClear('create_users $newly_created_user_id ::' . print_r($newly_created_user_id, true), false);
					if ( is_wp_error( $newly_created_user_id ) ){
						return array('success'=>false,'error_message'=>'Error creating user '. ', with error ' . $newly_created_user_id ); // Exit by error with message
					}
					$users_added++;
					wp_update_user(
						array(
							'ID' => $newly_created_user_id,
							'nickname' =>  $next_serialized_user['display_name'],
							'user_url' =>  $next_serialized_user['user_url'],
							'user_status' => 0
						)
					);
					$temp_new_users_ids_list[]= array('old_user_id'=> $next_serialized_user['ID'], 'new_user_id'=> $newly_created_user_id);
					$newly_created_user = new WP_User($newly_created_user_id);
//					DebToFileClear('create_users $newly_created_user ::' . print_r($newly_created_user, true), false);
					$newly_created_user->set_role($next_serialized_user['roles']);
				} // end if
			}
		}
	}
	DebToFileClear('create_users $temp_new_users_ids_list ::' . print_r($temp_new_users_ids_list, true), false);
	return array( 'success'=>true, 'error_message'=>'', 'users_added'=> $users_added, 'new_users_ids_list'=> $temp_new_users_ids_list ); // users were created succesfully
} // function create_users($serialized_users_list)

/*     public static function insert_test_users( $users_count= 10, $users_role= 'contributor' ) {
        $ret_users_id= '';
        for($i= 1; $i<= $users_count; $i++) {
            $username= 'test_user_'.$i.'_';
            $user_email= $username . '@mail.com';
            if (null == username_exists($user_email)) {

                $password = '111111';
                $user_id = wp_create_user($username, $password, $user_email);
//                echo '<pre>$user_id::'.print_r($user_id,true).'</pre>';
                if ( !$user_id ) return false;

                // Set the nickname
                wp_update_user(
                    array(
                        'ID' => $user_id,
                        'nickname' =>  $username.'_nick',
                        'user_url' =>  'http://' . $username . '_url.com',
                        'user_status' => 0
                    )
                );

                // Set the role
                $user = new WP_User($user_id);
                $user->set_role($users_role);

                // Email the user
//                wp_mail($user_email, 'Welcome!', 'Your Password: ' . $password);

            } // end if
        }
    }
 */

function upload_comments( $dest_post_id, $result_comments_list, $temp_new_users_ids_list ) {
    $new_inserted_comment_ids= array();
    DebToFileClear('-upload_comments $dest_post_id::' . print_r($dest_post_id,true),false);
    DebToFileClear('-upload_comments $temp_new_users_ids_list::' . print_r($temp_new_users_ids_list,true),false);
    DebToFileClear('-upload_comments $result_comments_list::' . print_r($result_comments_list,true),false);
//    echo '<pre>upload_comments  $dest_post_id::'.print_r($dest_post_id,true).'</pre>';
/* a:14:{s:10:"comment_ID";s:3:"722";s:14:"comment_author";s:16:"wp-plugings-user";s:20:"comment_author_email";s:14:"admin@mail.com";s:18:"comment_author_url";s:0:"";s:17:"comment_author_IP";s:9:"127.0.0.1";s:12:"comment_date";s:19:"2016-10-07 10:12:55";s:16:"comment_date_gmt";s:19:"2016-10-07 10:12:55";s:15:"comment_content";s:516:"Review # 1 for product 'X7 F5 */
	if ( empty($result_comments_list) ) {
		DebToFileClear('-upload_comments  EMPTY $dest_post_id::' . print_r($dest_post_id,true),false);

	}

    foreach( $result_comments_list as $next_key=>$next_result_comment ) {
        $comment_parent_id= '';
        foreach( $new_inserted_comment_ids as $next_inserted_comment ) {
            if ( (int)$next_inserted_comment['src_comment_id'] == (int)$next_result_comment['comment_parent'] ) {
                $comment_parent_id= $next_inserted_comment['dest_comment_id'];
            }
        }

		$comment_user_id= '';
		if ( !empty($temp_new_users_ids_list) and is_array($temp_new_users_ids_list) ) {
			foreach ( $temp_new_users_ids_list as $next_temp_new_users_id ) {
				if ( (int)$next_result_comment['user_id'] == (int)$next_temp_new_users_id['old_user_id']) {
					DebToFileClear('-upload_comments INSIDE   $next_temp_new_users_id::' . print_r($next_temp_new_users_id,true),false);
					$comment_user_id = $next_temp_new_users_id['old_user_id'];
				}
			}
		}
        $data = array(
            'comment_post_ID' => $dest_post_id,
            'comment_author' => $next_result_comment['comment_author'],
            'comment_author_email' => $next_result_comment['comment_author_email'],
            'comment_author_url' => $next_result_comment['comment_author_url'],
            'comment_content' => $next_result_comment['comment_content'],
            'comment_type' => $next_result_comment['comment_type'],
            'comment_parent' => $comment_parent_id,
            'user_id' => $comment_user_id,
            'comment_author_IP' => $next_result_comment['comment_author_IP'],
            'comment_agent' => $next_result_comment['comment_agent'],
            'comment_date' => $next_result_comment['comment_date'],
            'comment_approved' => $next_result_comment['comment_approved'],
        );


//            echo '<pre>$data::'.print_r($data,true).'</pre>';
        $new_comment_id= wp_insert_comment($data);
		DebToFileClear('-upload_comments RES $new_comment_id::' . print_r($new_comment_id,true),false);
//		$ret_comment= add_comment_meta($new_comment_id, 'rating', $rating_count);
		$ret_child_comment= add_comment_meta($new_comment_id,'rating',$next_result_comment['rating']);

		DebToFileClear('-upload_comments RES $ret_child_comment::' . print_r($ret_child_comment,true),false);
/*//            echo '<pre>$data::'.print_r($data,true).'</pre>';
            $comment_id= wp_insert_comment($data);
//            echo '<pre>$comment_id::'.print_r($comment_id,true).'</pre>';
            $rating_count= rand(0,5);
            if ( $rating_count > 0 and $add_rating ) {
                $ret_comment= add_comment_meta($comment_id, 'rating', $rating_count);
//                echo '<pre>$ret_comment::'.print_r($ret_comment,true).'</pre>';

            }

*/
        if ( $new_comment_id ) {
            $new_inserted_comment_ids[] = array('src_comment_id' => $next_result_comment['comment_ID'], 'dest_comment_id' => $new_comment_id );
        }

		WC_Product::sync_rating_count($dest_post_id);
		WC_Product::sync_rating_count($dest_post_id);
//        echo '<pre>$new_comment_id::'.print_r($new_comment_id,true).'</pre>';
//        return;
    }
    /*             $data = array(
                'comment_post_ID' => $post_id,
                'comment_author' => ( !empty($current_user->user_nicename) ? $current_user->user_nicename: 'admin' ),
                'comment_author_email' => ( !empty($current_user->user_email) ? $current_user->user_email: 'admin@admin.com' ),
                'comment_author_url' => '',
                'comment_content' => ' Review # '.$i." for product '".$post->post_title."' : ".nsnWooToolsData::loremIpsumText(),
                'comment_type' => '',
                'comment_parent' => 0,
                'user_id' => ( !empty($current_user->ID) ? $current_user->ID : 1 ),
                'comment_author_IP' => (!empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR']: '' ),
                'comment_agent' => (!empty($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT']: '' ),
                'comment_date' => $time,
                'comment_approved' => 1,
            );


//            echo '<pre>$data::'.print_r($data,true).'</pre>';
            $comment_id= wp_insert_comment($data);
//            echo '<pre>$comment_id::'.print_r($comment_id,true).'</pre>';
            $rating_count= rand(0,5);
            if ( $rating_count > 0 ) {
                $ret_comment= add_comment_meta($comment_id, 'rating', $rating_count);
                echo '<pre>$ret_comment::'.print_r($ret_comment,true).'</pre>';

            }

 */
}


?>
