<?php 
/*
Plugin Name: WOOEXIM - WooCommerce Export Import Plugin
Plugin URI: http://wooexim.com
Description: Export products, orders, categories, customers and coupons with all the meta informations of your store out of WooCommerce into a CSV Spreadsheet file. And Import products from any CSV file.
Version: 1.0.0
Author: WOOEXIM.COM
Author URI: http://wooexim.com
License: GPL2
*/

define ( 'WOOEXIM_PATH' , plugin_dir_url( __FILE__ ) );
define ( 'WOOEXIM_INC_APP_PATH' , plugin_dir_url( __FILE__ )."inc/" );
define ( 'WOOEXIM_INC_PATH' , "inc/" );
define ( 'WOOEXIM_EXPORT_DIR' , '' );
$url = get_admin_url().'admin.php?page=wooexim-export';
define ( 'WOOEXIM_EXPORT_ADMIN_URL' , $url );
$path = wp_upload_dir();
$wooexim_export = substr($path['path'], 0, -7) . "WOOEXIM_EXPORT";
if( ! is_dir( $wooexim_export ) )
	mkdir( $wooexim_export );
define ( 'WOOEXIM_EXPORT_PATH' , $wooexim_export );


define ( 'WOOEXIM_DOWNLOAD_PATH' ,substr($path['url'], 0, -7) . "WOOEXIM_EXPORT/" );

class WOOEXIM_Import {

    protected $m_plugin_url;
	public function __construct() {
        add_action( 'init',  array( &$this, 'translations' ) , 1 );
        add_action('admin_menu', array(&$this, 'admin_menu'));
        add_action('wp_ajax_wooexim-import-ajax', array(&$this, 'render_ajax_action'));

//		add_action( 'init', array( 'WOOEXIM_Import', 'translations' ), 1 );
//		add_action('admin_menu', array('WOOEXIM_Import', 'admin_menu'));
//		add_action('wp_ajax_wooexim-import-ajax', array('WOOEXIM_Import', 'render_ajax_action'));

        if ( is_admin() ) { // That is admin page
            add_action('wp_print_scripts', array(&$this, 'nsn_wooxim_export_import_admin_load_scripts'));
        }

	}


    function nsn_wooxim_export_import_admin_load_scripts()
    {
        $this->m_plugin_url = trailingslashit(WP_PLUGIN_URL.'/'.dirname(plugin_basename(__FILE__)));

        wp_register_script('Backend_NSN_wooxim_export_import_jquery', $this->m_plugin_url . 'js/jquery/jquery-1.11.1.min.js' );
        wp_register_script('Backend_NSN_wooxim_export_import_jquery_ui', $this->m_plugin_url . 'js/jquery/jquery-ui-1.11.1.min.js' );

        wp_enqueue_script('Backend_NSN_wooxim_export_import_jquery');
        wp_enqueue_script('Backend_NSN_wooxim_export_import_jquery_ui');

        wp_register_script('Backend_NSN_wooxim_export_import_Js', $this->m_plugin_url . 'js/backend-scripts.js' );
        wp_enqueue_script('Backend_NSN_wooxim_export_import_Js');

//        $get_message = get_option('mand_message_custom');
//        if ( empty( $get_message ) )
//            $get_message = 'The Fields Marked In Red Are Mandatory';
//
//        wp_localize_script( 'BackendNSN_WooExtSearchJs', 'ajax_object', array( 'errorMessage' =>  $get_message,
//            'backend_ajaxurl' => $this->m_plugin_url.'ajax-backend-control.php',
//            'post_type' => get_post_type()) );
    }


	public function translations() {
		load_plugin_textdomain( 'wooexim-import', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}

	public function admin_menu() {
		add_menu_page( 'WOOEXIM', 'WOOEXIM', 'manage_options', 'wooexim-import', '', '', 56 );
		add_submenu_page( 'wooexim-import', 'Import Product', 'Import Products', 'manage_options', 'wooexim-import', array(&$this, 'render_admin_action'));
//		add_submenu_page( 'wooexim-import', 'Import Product', 'Import Products', 'manage_options', 'wooexim-import', array('WOOEXIM_Import', 'render_admin_action'));
	}
	
	public function render_admin_action() {
		$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'upload';
		require_once(plugin_dir_path(__FILE__).'inc/wooexim-import-common.php');
		require_once(plugin_dir_path(__FILE__)."inc/wooexim-import-{$action}.php");
	}
	
	public function render_ajax_action() {
		require_once(plugin_dir_path(__FILE__)."inc/wooexim-import-ajax.php");
		die(); // this is required to return a proper result
	}
}
	
$wooexim_import = new WOOEXIM_Import();	

require_once( WOOEXIM_INC_PATH.'wooexim-export.php' );
require_once( WOOEXIM_INC_PATH.'wooexim-save-settings.php' );
require_once( WOOEXIM_INC_PATH.'wooexim-spreadsheet.php' );
require_once( WOOEXIM_EXPORT_DIR.'lib/PHPExcel.php' );

$wooexim_export  = new Woo_wooexim_export();
?>