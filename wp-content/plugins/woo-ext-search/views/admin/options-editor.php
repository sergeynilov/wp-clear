<?php
if (!defined('NSN_WooExtSearch')) {
    exit; // Exit if accessed directly
}
echo '<pre>++++ $m_parentControl->m_optionEditorItem::'.print_r($m_parentControl->m_optionEditorItem,true).'</pre>';
//echo '<pre>$m_parentControl->m_products_attrs_list::'.print_r($m_parentControl->m_products_attrs_list,true).'</pre>';

function showPostsSelectionForAnyAttr( $m_parentControl, $next_products_attr, $hide_attr_post_selection, $service_category_for_attrs_array ) {  ?>
                    <table style="<?php echo $hide_attr_post_selection ? ' display:none; ' : "" ?>" id="table_attrs_<?php echo $next_products_attr['attribute_name'] ?>" class="nsn_woo_ext_search_thin_border nsn_woo_ext_search_block_padding_sm">
                            <?php foreach( $next_products_attr['attrs'] as $next_products_attrs_struct ) { ?>
                                <?php  $next_products_attrs_struct_name = nsnWooToolsData::changeSubmitKey( $next_products_attrs_struct['name'] ) ?>
                                <tr>
                                    <td>
                                <label for="nsn-woo-ext-search-meta_info_key"><u><?php echo $next_products_attrs_struct_name ?></u></label>

                                <?php $attribute_service_category_attr_id = !empty( $service_category_for_attrs_array['nsn-woo-ext-search-attribute_service_category_attr_'.$next_products_attr['attribute_name'] . '_' . $next_products_attrs_struct_name ] ) ? $service_category_for_attrs_array['nsn-woo-ext-search-attribute_service_category_attr_'.$next_products_attr['attribute_name'] . '_' . $next_products_attrs_struct_name ] : '';
//                                echo '<pre>$next_products_attrs_struct::'.print_r($next_products_attrs_struct,true).'</pre>';
//                                echo '<pre>$service_category_for_attrs_array::'.print_r($service_category_for_attrs_array,true).'</pre>'; ?>
                                <select aria-required="true" id="nsn-woo-ext-search-attribute_service_category_attr_<?php echo $next_products_attr['attribute_name'].'_' . $next_products_attrs_struct_name ?>" name="nsn-woo-ext-search-attribute_service_category_attr_<?php echo $next_products_attr['attribute_name'] . '_'.
                                    $next_products_attrs_struct_name ?>" >



                                    <option value="" >  -Select post-  </option>

                                    <?php foreach( $m_parentControl->m_posts_of_service_category_list as $next_key=>$next_service_category ) {  ?>
                                        <option value="<?php echo $next_service_category->ID ?>" <?php echo ( $attribute_service_category_attr_id ==$next_service_category->ID ? 'selected' : '' ) ?> ><?php echo $next_service_category->post_title ?></option>
                                    <?php } ?>
                                </select>&nbsp;
                                <p class="nsn_woo_ext_search_next_padding_bottom">Selecting post, it will be used by <b><?php echo $next_products_attr['attribute_label'] ?></b> link as description of this attribute. </p>
                                    <td>
                                </tr>
                            <?php } ?>
                    </table>
<?php
}
//echo '<pre>fields_to_show_array::'.print_r($m_parentControl->m_optionEditorItem,true).'</pre>';

?>

<script type="text/javascript" language="JavaScript">
    /*<![CDATA[*/

    // public static function prepareParams($commonParams = true, $paramsArray = array(), $return_array= true)
    var nsn_woo_ext_search_backendFuncsObj = new nsn_woo_ext_search_backendFuncs( { // must be called before jQuery(document).ready(function ($) {
        <?php echo nsnClass_appFuncs::prepareParams( true,  array('m_plugin_url'=> $m_parentControl->m_plugin_url, 'm_plugin_dir'=> $m_parentControl->m_plugin_dir, 'm_plugin_name'=> $m_parentControl->m_plugin_name, 'm_page_url'=> get_site_url()  ), false )?>
    } );

//    alert( "nsn_woo_ext_search_backendFuncsObj::"+var_dump(nsn_woo_ext_search_backendFuncsObj) )
    jQuery(document).ready(function ($) {
        nsn_woo_ext_search_backendFuncsObj.onInit()
    });

    /*]]>*/
</script>


<script type="text/javascript" language="JavaScript">
    /*<![CDATA[*/

//    function cbx_use_in_search_onChange( attribute_name ) {
//        if ( $("#cbx_use_in_search_"+attribute_name).is(':checked') ) {
//            $("#select_use_in_search_field_type_"+attribute_name).removeAttr("disabled");
//            $("#use_in_search_hint_"+attribute_name).removeAttr("disabled");
//        } else {
//            $("#select_use_in_search_field_type_"+attribute_name).val("");
//            $("#use_in_search_hint_"+attribute_name).val("");
//            select_use_in_search_field_type_onChange("", attribute_name)
//            $("#select_use_in_search_field_type_"+attribute_name).attr("disabled","disabled");
//            $("#use_in_search_hint_"+attribute_name).attr("disabled","disabled");
//            $("#" + "span_single_text_" + attribute_name).css("display", "none");
//            $("#" + "span_boolean_" + attribute_name).css("display", "none");
//            $("#" + "span_predefined_selection_" + attribute_name).css("display", "none");
//            $("#" + "span_range_" + attribute_name).css("display", "none");
//        }
//    }
//
//    function select_use_in_search_field_type_onChange( selector_value, attribute_name ) {
//        // alert( "select_use_in_search_field_type_onChange attribute_name::"+var_dump(attribute_name) )
//        if ( selector_value == "single_text" ) {
//            $("#" + "span_single_text_" + attribute_name).css("display", "inline");
//            $("#" + "span_boolean_" + attribute_name).css("display", "none");
//            $("#" + "span_predefined_selection_" + attribute_name).css("display", "none");
//            $("#" + "span_range_" + attribute_name).css("display", "none");
//        } //if ( selector_value == "single_text" ) {
//        if (selector_value == "range") {
//            $("#" + "span_single_text_" + attribute_name).css("display", "none");
//            $("#" + "span_boolean_" + attribute_name).css("display", "none");
//            $("#" + "span_predefined_selection_" + attribute_name).css("display", "none");
//            $("#" + "span_range_" + attribute_name).css("display", "inline");
//        }
//        if (selector_value == "predefined_selection") {
//            $("#" + "span_single_text_" + attribute_name).css("display", "none");
//            $("#" + "span_boolean_" + attribute_name).css("display", "none");
//            $("#" + "span_predefined_selection_" + attribute_name).css("display", "inline");
//            $("#" + "span_range_" + attribute_name).css("display", "none");
//        }
//        if (selector_value == "boolean") {
//            $("#" + "span_single_text_" + attribute_name).css("display", "none");
//            $("#" + "span_boolean_" + attribute_name).css("display", "inline");
//            $("#" + "span_predefined_selection_" + attribute_name).css("display", "none");
//            $("#" + "span_range_" + attribute_name).css("display", "none");
//        }
//    } // function select_use_in_search_field_type_onChange( selector_value, attribute_name ) {

    /*]]>*/
</script>



<div class="form-editor-wrap">
    <h3><?php echo esc_html__("Options"); //echo '<pre>$m_parentControl->m_pageUrl::'.print_r($m_parentControl->m_pageUrl,true).'</pre>';   ?></h3>


    <?php
    echo '<pre>$m_parentControl->m_hasValidationErrors::'.print_r($m_parentControl->m_hasValidationErrors,true).'</pre>';

//    if ( $m_parentControl->m_hasValidationErrors > 0 ) {
//        echo '<pre>$_POST::' . print_r($_POST, true) . '</pre>';
//        echo '<pre>$m_parentControl->m_optionEditorItem::' . print_r($m_parentControl->m_optionEditorItem, true) . '</pre>';
//    }

    if ( $m_parentControl->m_hasValidationErrors > 0 ) : ?>
    <div class="nsn_woo_ext_search_error_block" >
        <strong>Some fields <b>( <?php echo count($m_parentControl->m_hasValidationErrors) ?> )</b> are invalid. Fill them and submit again.</strong>
    </div>
    <?php endif; ?>


    <?php if ( $m_parentControl->m_hasValidationErrors == 0 ) : ?>
    <div >
        <center><h1>You updates plugin's options successfully!</h1></center>
    </div>
    <?php endif; ?>



    <form method="post" class="validate" action="<?php echo get_site_url() . $m_parentControl->m_pageUrl; ?>" id="form_options_editor" enctype="multipart/form-data">
        <input type="hidden" name="<?php echo $m_parentControl->m_labelNonceInput ?>" value="<?php echo wp_create_nonce($m_parentControl->m_labelNonce) ?>" />
        <input type="hidden" value="update" id="action" name="action">


            <fieldset class="nsn_woo_ext_search_block_padding_md nsn_woo_ext_search_next_padding_bottom nsn_woo_ext_search_thin_border">
                <legend><h3>1) Common Settings:</h3>  </legend>
            <div class="form-option-field form-require term-nsn-woo-ext-search-meta_info_key-wrap <?php echo showErrorMessage("nsn-woo-ext-search-meta_info_key", "nsn-woo-ext-search-meta_info_key", $m_parentControl->m_hasValidationErrors, true) ?>">
                <label for="nsn-woo-ext-search-meta_info_key"><?php echo esc_html__("Key for internal using on inputs generation") ?></label>

                <input type="text" aria-required="true" value="<?php echo !empty($m_parentControl->m_optionEditorItem['meta_info_key']) ? $m_parentControl->m_optionEditorItem['meta_info_key'] :  '' ?>" id="nsn-woo-ext-search-meta_info_key" name="nsn-woo-ext-search-meta_info_key" size="30" maxlength="50"
                >&nbsp;
                <?php echo showErrorMessage("nsn-woo-ext-search-meta_info_key" , "nsn-woo-ext-search-meta_info_key" , $m_parentControl->m_hasValidationErrors, false) ?>

                <p class="nsn_woo_ext_search_next_padding_bottom">This key will be used on frontend pages to exclude conflicts with other inputs.</p>
            </div>




            <div class="form-option-field form-require term-nsn-woo-ext-search-service_category_for_attrs-wrap <?php echo showErrorMessage("nsn-woo-ext-search-service_category_for_attrs", "nsn-woo-ext-search-service_category_for_attrs", $m_parentControl->m_hasValidationErrors, true) ?>">

                <?php $service_category_for_attrs= ( !empty($m_parentControl->m_optionEditorItem['service_category_for_attrs']) ? $m_parentControl->m_optionEditorItem['service_category_for_attrs'] : '' ) ?>
                <label for="nsn-woo-ext-search-service_category_for_attrs"><?php echo esc_html__("Service category for fields/attributes posts") ?></label>
                <select aria-required="true" id="nsn-woo-ext-search-service_category_for_attrs" name="nsn-woo-ext-search-service_category_for_attrs" >
                    <option value="" >  -Select category-  </option>

                 <?php foreach( $m_parentControl->m_categories_list as $next_key=>$next_post_category ) {  ?>
                    <option value="<?php echo $next_post_category['ID']?>" <?php echo ( strtolower($service_category_for_attrs)==$next_post_category['ID'] ? 'selected' : '' ) ?> ><?php echo $next_post_category['name']?></option>
                    <?php } ?>
                </select>&nbsp;

                <?php echo showErrorMessage("nsn-woo-ext-search-service_category_for_attrs" , "nsn-woo-ext-search-service_category_for_attrs" , $m_parentControl->m_hasValidationErrors, false) ?>

                <p class="nsn_woo_ext_search_next_padding_bottom">With posts Category selected, posts from it can be selected for fields/attributes in block below.</p>
            </div>

                <div class="form-option-field form-require term-nsn-woo-ext-search-products_per_page-wrap <?php echo showErrorMessage("nsn-woo-ext-search-products_per_page", "nsn-woo-ext-search-products_per_page", $m_parentControl->m_hasValidationErrors, true) ?>">
                    <label for="nsn-woo-ext-search-products_per_page"><?php echo esc_html__("Products per page") ?></label>
                    <?php $products_per_page= ( !empty($m_parentControl->m_optionEditorItem['products_per_page']) ? $m_parentControl->m_optionEditorItem['products_per_page'] : ''
                     ); ?>
                    <input id="nsn-woo-ext-search-products_per_page" name="nsn-woo-ext-search-products_per_page" value="<?php echo $products_per_page ?>" class="text-right" size="4" maxlength="4">
                    <?php echo showErrorMessage("nsn-woo-ext-search-products_per_page" , "nsn-woo-ext-search-products_per_page" , $m_parentControl->m_hasValidationErrors, false) ?>
                    <p class="nsn_woo_ext_search_next_padding_bottom">Number of products per page on products result listing </p>
                </div>



                <div class="form-option-field form-require term-nsn-woo-ext-search-products_pagination_per_page_list-wrap <?php echo showErrorMessage("nsn-woo-ext-search-products_pagination_per_page_list", "nsn-woo-ext-search-products_pagination_per_page_list", $m_parentControl->m_hasValidationErrors, true) ?>">
                    <label for="nsn-woo-ext-search-products_pagination_per_page_list"><?php echo esc_html__("Products per page by default") ?></label>
                    <?php $products_pagination_per_page_list= ( !empty($m_parentControl->m_optionEditorItem['products_pagination_per_page_list']) ? $m_parentControl->m_optionEditorItem['products_pagination_per_page_list'] : '' ); ?>
                    <input id="nsn-woo-ext-search-products_pagination_per_page_list" name="nsn-woo-ext-search-products_pagination_per_page_list" value="<?php echo $products_pagination_per_page_list ?>" class="text-right" size="40" maxlength="255">
                    <?php echo showErrorMessage("nsn-woo-ext-search-products_pagination_per_page_list" , "nsn-woo-ext-search-products_pagination_per_page_list" , $m_parentControl->m_hasValidationErrors, false) ?>
                    <p class="nsn_woo_ext_search_next_padding_bottom">Number of products per page on products result listing by default. Leave empty if you do not want user             could number of products per 1 page></select></p>
                </div>


                <div class="form-option-field form-require term-nsn-woo-ext-search-show_bookmark_functionality-wrap <?php echo showErrorMessage("nsn-woo-ext-search-show_bookmark_functionality", "nsn-woo-ext-search-show_bookmark_functionality", $m_parentControl->m_hasValidationErrors, true) ?>">
                    <label for="nsn-woo-ext-search-show_bookmark_functionality"><?php echo esc_html__("Show bookmark functionality") ?></label>

                    <?php $show_bookmark_functionality= ( !empty($m_parentControl->m_optionEditorItem['show_bookmark_functionality']) ? $m_parentControl->m_optionEditorItem['show_bookmark_functionality'] : '' ); ?>
                    <select aria-required="true" id="nsn-woo-ext-search-show_bookmark_functionality" name="nsn-woo-ext-search-show_bookmark_functionality" >
                        <option value="" >  -Select-  </option>
                        <option value="yes" <?php echo ( strtolower($show_bookmark_functionality)=='yes' ? 'selected' : '' ) ?> >Yes</option>
                        <option value="no" <?php echo ( strtolower($show_bookmark_functionality)=='no' ? 'selected' : '' ) ?> >No</option>
                    </select>&nbsp;

                    <?php echo showErrorMessage("nsn-woo-ext-search-show_bookmark_functionality" , "nsn-woo-ext-search-show_bookmark_functionality" , $m_parentControl->m_hasValidationErrors, false) ?>

                    <p class="nsn_woo_ext_search_next_padding_bottom">If "Yes" selected, then user could save/use bookmarks.</p>
                </div>


                <div class="form-option-field form-require term-nsn-woo-ext-search-show_bookmark_alerts-wrap <?php echo showErrorMessage("nsn-woo-ext-search-show_bookmark_alerts", "nsn-woo-ext-search-show_bookmark_alerts", $m_parentControl->m_hasValidationErrors, true) ?>">
                    <?php $show_bookmark_alerts= ( !empty($m_parentControl->m_optionEditorItem['show_bookmark_alerts']) ? $m_parentControl->m_optionEditorItem['show_bookmark_alerts'] : '' ) ?>
                    <label for="nsn-woo-ext-search-show_bookmark_alerts"><?php echo esc_html__("Show bookmark alerts"  ) ?></label>
                    <select aria-required="true" id="nsn-woo-ext-search-show_bookmark_alerts" name="nsn-woo-ext-search-show_bookmark_alerts" >
                        <option value="" >  -Select-  </option>
                        <option value="yes" <?php echo ( strtolower($show_bookmark_alerts)=='yes' ? 'selected' : '' ) ?> >Yes</option>
                        <option value="no" <?php echo ( strtolower($show_bookmark_alerts)=='no' ? 'selected' : '' ) ?> >No</option>
                    </select>&nbsp;

                    <?php echo showErrorMessage("nsn-woo-ext-search-show_bookmark_alerts" , "nsn-woo-ext-search-show_bookmark_alerts" , $m_parentControl->m_hasValidationErrors, false) ?>
                    <p class="nsn_woo_ext_search_next_padding_bottom">If "Yes" selected, working with bookmarks user will see alert message.</p>
                </div>


                <div class="form-option-field form-require term-nsn-woo-ext-search-service_post_for_help_text-wrap <?php echo showErrorMessage("nsn-woo-ext-search-service_post_for_help_text", "nsn-woo-ext-search-service_post_for_help_text", $m_parentControl->m_hasValidationErrors, true) ?>">
                    <?php $service_post_for_help_text= ( !empty($m_parentControl->m_optionEditorItem['service_post_for_help_text']) ? $m_parentControl->m_optionEditorItem['service_post_for_help_text'] : '' ) ?>
                    <label for="nsn-woo-ext-search-service_post_for_help_text"><?php echo esc_html__("Service post for help text") ?></label>
                    <select aria-required="true" id="nsn-woo-ext-search-service_post_for_help_text" name="nsn-woo-ext-search-service_post_for_help_text" >
                        <option value="" >  -Select post-  </option>
                        <?php foreach( $m_parentControl->m_posts_of_service_category_list as $next_key=>$next_service_category ) {  ?>
                            <option value="<?php echo $next_service_category->ID ?>" <?php echo ( $service_post_for_help_text ==$next_service_category->ID ? 'selected' : '' ) ?> ><?php echo $next_service_category->post_title ?></option>
                        <?php } ?>
                    </select>&nbsp;
                    <?php echo showErrorMessage("nsn-woo-ext-search-service_post_for_help_text" , "nsn-woo-ext-search-service_post_for_help_text" , $m_parentControl->m_hasValidationErrors, false) ?>

                    <p class="nsn_woo_ext_search_next_padding_bottom">If some post is selected, link to this post will be visible in Controls Box</p>
                </div>




            </fieldset>




            <div >


                <fieldset class="nsn_woo_ext_search_next_padding_bottom nsn_woo_ext_search_block_padding_md nsn_woo_ext_search_thin_border">
                    <legend><h3>2) Product fields/attributes, which must be shown in filter criteria:</h3>  </legend>
                    <p class="nsn_woo_ext_search_admin_info">Before editing of this block, please fill and save block above, as some selection in this block depends on data above.</p>



                    <div class="form-option-field form-require term-nsn-woo-ext-search-show_in_extended_search_sku-wrap <?php echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_sku", "nsn-woo-ext-search-show_in_extended_search_sku", $m_parentControl->m_hasValidationErrors, true) ?>">
                        <?php $show_in_extended_search_sku= ( !empty($m_parentControl->m_optionEditorItem['show_in_extended_search_sku']) ? $m_parentControl->m_optionEditorItem['show_in_extended_search_sku'] : '' ) ?>
                        <label for="nsn-woo-ext-search-show_in_extended_search_sku"><?php echo esc_html__("Show sku in extended search box") ?></label>
                        <select aria-required="true" id="nsn-woo-ext-search-show_in_extended_search_sku" name="nsn-woo-ext-search-show_in_extended_search_sku" >
                            <option value="" >  -Select-  </option>
                            <option value="yes" <?php echo ( strtolower($show_in_extended_search_sku)=='yes' ? 'selected' : '' ) ?> >Yes</option>
                            <option value="no" <?php echo ( strtolower($show_in_extended_search_sku)=='no' ? 'selected' : '' ) ?> >No</option>
                        </select>&nbsp;

                        <?php echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_sku" , "nsn-woo-ext-search-show_in_extended_search_sku" , $m_parentControl->m_hasValidationErrors, false) ?>

                        <p class="nsn_woo_ext_search_next_padding_bottom">If "Yes" selected, <b>sku</b> be used for filter in filters box.</p>
                    </div>



                    <div class="form-option-field form-require term-nsn-woo-ext-search-show_in_extended_search_rating-wrap <?php echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_rating", "nsn-woo-ext-search-show_in_extended_search_rating", $m_parentControl->m_hasValidationErrors, true) ?>">
                        <?php $show_in_extended_search_rating= ( !empty($m_parentControl->m_optionEditorItem['show_in_extended_search_rating']) ? $m_parentControl->m_optionEditorItem['show_in_extended_search_rating'] : '' ) ?>
                        <label for="nsn-woo-ext-search-show_in_extended_search_rating"><?php echo esc_html__("Show rating in extended search box") ?></label>
                        <select aria-required="true" id="nsn-woo-ext-search-show_in_extended_search_rating" name="nsn-woo-ext-search-show_in_extended_search_rating" >
                            <option value="" >  -Select-  </option>
                            <option value="yes" <?php echo ( strtolower($show_in_extended_search_rating)=='yes' ? 'selected' : '' ) ?> >Yes</option>
                            <option value="no" <?php echo ( strtolower($show_in_extended_search_rating)=='no' ? 'selected' : '' ) ?> >No</option>
                        </select>&nbsp;

                        <?php echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_rating" , "nsn-woo-ext-search-show_in_extended_search_rating" , $m_parentControl->m_hasValidationErrors, false) ?>

                        <p class="nsn_woo_ext_search_next_padding_bottom">If "Yes" selected, <b>rating</b> be used for filter in filters box.</p>
                    </div>



                    <div class="form-option-field form-require term-nsn-woo-ext-search-show_in_extended_search_post_title-wrap <?php echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_post_title", "nsn-woo-ext-search-show_in_extended_search_post_title", $m_parentControl->m_hasValidationErrors, true) ?>">
                        <?php $show_in_extended_search_post_title= ( !empty($m_parentControl->m_optionEditorItem['show_in_extended_search_post_title']) ? $m_parentControl->m_optionEditorItem['show_in_extended_search_post_title'] : '' ) ?>
                        <label for="nsn-woo-ext-search-show_in_extended_search_post_title"><?php echo esc_html__("Show post title/content in extended search box") ?></label>
                        <select aria-required="true" id="nsn-woo-ext-search-show_in_extended_search_post_title" name="nsn-woo-ext-search-show_in_extended_search_post_title" >
                            <option value="" >  -Select-  </option>
                            <option value="yes" <?php echo ( strtolower($show_in_extended_search_post_title)=='yes' ? 'selected' : '' ) ?> >Yes</option>
                            <option value="no" <?php echo ( strtolower($show_in_extended_search_post_title)=='no' ? 'selected' : '' ) ?> >No</option>
                        </select>&nbsp;
                        <?php echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_post_title" , "nsn-woo-ext-search-show_in_extended_search_post_title" , $m_parentControl->m_hasValidationErrors, false) ?>
                        <p class="nsn_woo_ext_search_next_padding_bottom">If "Yes" selected, <b>post title/content</b> be used for filter in filters box.</p>
                    </div>


                    <div class="form-option-field form-require term-nsn-woo-ext-search-show_in_extended_search_tag-wrap <?php echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_tag", "nsn-woo-ext-search-show_in_extended_search_tag", $m_parentControl->m_hasValidationErrors, true) ?>">
                        <?php $show_in_extended_search_tag= ( !empty($m_parentControl->m_optionEditorItem['show_in_extended_search_tag']) ? $m_parentControl->m_optionEditorItem['show_in_extended_search_tag'] : '' ) ?>
                        <label for="nsn-woo-ext-search-show_in_extended_search_tag"><?php echo esc_html__("Show Tags Box in extended search box") ?></label>
                        <select aria-required="true" id="nsn-woo-ext-search-show_in_extended_search_tag" name="nsn-woo-ext-search-show_in_extended_search_tag" >
                            <option value="" >  -Select-  </option>
                            <option value="yes" <?php echo ( strtolower($show_in_extended_search_tag)=='yes' ? 'selected' : '' ) ?> >Yes</option>
                            <option value="no" <?php echo ( strtolower($show_in_extended_search_tag)=='no' ? 'selected' : '' ) ?> >No</option>
                        </select>&nbsp;
                        <?php echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_tag" , "nsn-woo-ext-search-show_in_extended_search_tag" , $m_parentControl->m_hasValidationErrors, false) ?>
                        <p class="nsn_woo_ext_search_next_padding_bottom">If "Yes" selected, <b>tags</b> be used for filter in filters box.</p>
                    </div>



                    <div class="form-option-field form-require term-nsn-woo-ext-search-show_in_extended_search_categories_box-wrap <?php echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_categories_box", "nsn-woo-ext-search-show_in_extended_search_categories_box", $m_parentControl->m_hasValidationErrors, true) ?>">
                        <?php $show_in_extended_search_categories_box= ( !empty($m_parentControl->m_optionEditorItem['show_in_extended_search_categories_box']) ? $m_parentControl->m_optionEditorItem['show_in_extended_search_categories_box'] : '' ) ?>
                        <label for="nsn-woo-ext-search-show_in_extended_search_categories_box"><?php echo esc_html__("Show Categories Box in extended search box") ?></label>
                        <select aria-required="true" id="nsn-woo-ext-search-show_in_extended_search_categories_box" name="nsn-woo-ext-search-show_in_extended_search_categories_box" >
                            <option value="" >  -Select-  </option>
                            <option value="yes" <?php echo ( strtolower($show_in_extended_search_categories_box)=='yes' ? 'selected' : '' ) ?> >Yes</option>
                            <option value="no" <?php echo ( strtolower($show_in_extended_search_categories_box)=='no' ? 'selected' : '' ) ?> >No</option>
                        </select>&nbsp;
                        <?php echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_categories_box" , "nsn-woo-ext-search-show_in_extended_search_categories_box" , $m_parentControl->m_hasValidationErrors, false) ?>
                        <p class="nsn_woo_ext_search_next_padding_bottom">If "Yes" selected, <b>Categories Box</b> will be visible in filters box.</p>
                    </div>



                    <div class="form-option-field form-require term-nsn-woo-ext-search-show_in_extended_search_prices_box-wrap <?php echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_prices_box", "nsn-woo-ext-search-show_in_extended_search_prices_box", $m_parentControl->m_hasValidationErrors, true) ?>">
                        <?php $show_in_extended_search_prices_box= ( !empty($m_parentControl->m_optionEditorItem['show_in_extended_search_prices_box']) ? $m_parentControl->m_optionEditorItem['show_in_extended_search_prices_box'] : '' ) ?>
                        <label for="nsn-woo-ext-search-show_in_extended_search_prices_box"><?php echo esc_html__("Show Prices Box in extended search box") ?></label>
                        <select aria-required="true" id="nsn-woo-ext-search-show_in_extended_search_prices_box" name="nsn-woo-ext-search-show_in_extended_search_prices_box"  onchange="javascript:nsn_woo_ext_search_backendFuncsObj.show_in_extended_search_prices_box_onChange() "  >
                            <option value="" >  -Select-  </option>
                            <option value="yes" <?php echo ( strtolower($show_in_extended_search_prices_box)=='yes' ? 'selected' : '' ) ?> >Yes</option>
                            <option value="no" <?php echo ( strtolower($show_in_extended_search_prices_box)=='no' ? 'selected' : '' ) ?> >No</option>
                        </select>&nbsp;
                        <?php echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_prices_box" , "nsn-woo-ext-search-show_in_extended_search_prices_box" , $m_parentControl->m_hasValidationErrors, false) ?>
                        <p class="nsn_woo_ext_search_next_padding_bottom">If "Yes" selected, <b>Prices Box</b> will be visible in filters box.</p>
                    </div>


                    <div class="form-option-field form-require term-nsn-woo-ext-search-show_in_extended_search_price_ranges-wrap <?php echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_price_ranges", "nsn-woo-ext-search-show_in_extended_search_price_ranges", $m_parentControl->m_hasValidationErrors, true) ?>" style="<?php echo strtolower($show_in_extended_search_prices_box)!='yes' ? ' display:none; ' : "" ?>" id="div_nsn-woo-ext-search-show_in_extended_search_price_ranges">
                        <?php $show_in_extended_search_price_ranges= ( !empty($m_parentControl->m_optionEditorItem['show_in_extended_search_price_ranges']) ? $m_parentControl->m_optionEditorItem['show_in_extended_search_price_ranges'] : '' ) ?>
                        <label for="nsn-woo-ext-search-show_in_extended_search_price_ranges"><?php echo esc_html__("Enter comma separated price ranges") ?></label>
                        <input aria-required="true" id="nsn-woo-ext-search-show_in_extended_search_price_ranges" name="nsn-woo-ext-search-show_in_extended_search_price_ranges" maxlength="255" size="40" value="<?php echo $show_in_extended_search_price_ranges ?> ">
                        <?php echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_price_ranges" , "nsn-woo-ext-search-show_in_extended_search_price_ranges" , $m_parentControl->m_hasValidationErrors, false) ?>
                        <p class="nsn_woo_ext_search_next_padding_bottom">These <b>price ranges</b>( example : 5, 20, 50, 100 ) will be used on frontend pages to in price selection filter box</p>
                    </div>




                    <div class="form-option-field form-require term-nsn-woo-ext-search-show_in_extended_search_hide_with_zero_products-wrap <?php echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_hide_with_zero_products", "nsn-woo-ext-search-show_in_extended_search_hide_with_zero_products", $m_parentControl->m_hasValidationErrors, true) ?>">
                        <?php $show_in_extended_search_hide_with_zero_products= ( !empty($m_parentControl->m_optionEditorItem['show_in_extended_search_hide_with_zero_products']) ? $m_parentControl->m_optionEditorItem['show_in_extended_search_hide_with_zero_products'] : '' ) ?>
                        <label for="nsn-woo-ext-search-show_in_extended_search_hide_with_zero_products"><?php echo esc_html__("In extended search block hide elements with zero products") ?></label>
                        <select aria-required="true" id="nsn-woo-ext-search-show_in_extended_search_hide_with_zero_products" name="nsn-woo-ext-search-show_in_extended_search_hide_with_zero_products" >
                            <option value="" >  -Select-  </option>
                            <option value="yes" <?php echo ( strtolower($show_in_extended_search_hide_with_zero_products)=='yes' ? 'selected' : '' ) ?> >Yes</option>
                            <option value="no" <?php echo ( strtolower($show_in_extended_search_hide_with_zero_products)=='no' ? 'selected' : '' ) ?> >No</option>
                        </select>&nbsp;
                        <?php echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_hide_with_zero_products" , "nsn-woo-ext-search-show_in_extended_search_hide_with_zero_products" , $m_parentControl->m_hasValidationErrors, false) ?>
                        <p class="nsn_woo_ext_search_next_padding_bottom">If "Yes" selected, in extended search block <b>elements with zero products</b> will be hidden.</p>
                    </div>


                    <div class="form-option-field form-require term-nsn-woo-ext-search-show_in_extended_search_show_number_of_products-wrap <?php echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_show_number_of_products", "nsn-woo-ext-search-show_in_extended_search_show_number_of_products", $m_parentControl->m_hasValidationErrors, true) ?>">
                        <?php $show_in_extended_search_show_number_of_products= ( !empty($m_parentControl->m_optionEditorItem['show_in_extended_search_show_number_of_products']) ? $m_parentControl->m_optionEditorItem['show_in_extended_search_show_number_of_products'] : '' ) ?>
                        <label for="nsn-woo-ext-search-show_in_extended_search_show_number_of_products"><?php echo esc_html__("In extended search block show number of products") ?></label>
                        <select aria-required="true" id="nsn-woo-ext-search-show_in_extended_search_show_number_of_products" name="nsn-woo-ext-search-show_in_extended_search_show_number_of_products" >
                            <option value="" >  -Select-  </option>
                            <option value="yes" <?php echo ( strtolower($show_in_extended_search_show_number_of_products)=='yes' ? 'selected' : '' ) ?> >Yes</option>
                            <option value="no" <?php echo ( strtolower($show_in_extended_search_show_number_of_products)=='no' ? 'selected' : '' ) ?> >No</option>
                        </select>&nbsp;
                        <?php echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_show_number_of_products" , "nsn-woo-ext-search-show_in_extended_search_show_number_of_products" , $m_parentControl->m_hasValidationErrors, false) ?>
                        <p class="nsn_woo_ext_search_next_padding_bottom">If "Yes" selected, in extended search block <b>number of products</b> will be visible, but that could make the  form working slowly.</p>
                    </div>


<!--                    <div class="form-option-field form-require term-nsn-woo-ext-search-show_in_extended_search_number_blocked_tags-wrap --><?php //echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_number_blocked_tags", "nsn-woo-ext-search-show_in_extended_search_number_blocked_tags", $m_parentControl->m_hasValidationErrors, true) ?><!--">-->
<!--                        --><?php //$show_in_extended_search_number_blocked_tags= ( !empty($m_parentControl->m_optionEditorItem['show_in_extended_search_number_blocked_tags']) ? $m_parentControl->m_optionEditorItem['show_in_extended_search_number_blocked_tags'] : '' ) ?>
<!--                        <label for="nsn-woo-ext-search-show_in_extended_search_number_blocked_tags">--><?php //echo esc_html__("Number of blocked product tags") ?><!--</label>-->
<!--                        <input aria-required="true" id="nsn-woo-ext-search-show_in_extended_search_number_blocked_tags" name="nsn-woo-ext-search-show_in_extended_search_number_blocked_tags" maxlength="3" size="3" value="--><?php //echo $show_in_extended_search_number_blocked_tags ?><!--">-->
<!--                        --><?php //echo showErrorMessage("nsn-woo-ext-search-show_in_extended_search_number_blocked_tags" , "nsn-woo-ext-search-show_in_extended_search_number_blocked_tags" , $m_parentControl->m_hasValidationErrors, false) ?>
<!--                        <p class="nsn_woo_ext_search_next_padding_bottom"><b>Number of blocked product tags</b> will help to show most popular tags in filter box</p>-->
<!--                    </div>-->


                <table>

                <?php $service_category_for_attrs_array= array();
//                echo '<pre>$m_parentControl->m_optionEditorItem[service_category_for_attrs_array]::'.print_r($m_parentControl->m_optionEditorItem['service_category_for_attrs_array'],true).'</pre>';
//                die("-1 XXZ");
                if (!empty($m_parentControl->m_optionEditorItem['service_category_for_attrs_array']) and is_array($m_parentControl->m_optionEditorItem['service_category_for_attrs_array'])) {
                    $service_category_for_attrs_array= $m_parentControl->m_optionEditorItem['service_category_for_attrs_array'];
                }
                ?>



                    <!----------------    -->
<!--                --><?php //echo '<pre>$m_parentControl->m_products_attrs_list::'.print_r($m_parentControl->m_products_attrs_list,true).'</pre>';
                foreach( $m_parentControl->m_products_attrs_list as $next_products_attr ) {   ?>
                     <tr style="vertical-align: top">

                         <?php
                         $show_attribute_in_extended_search_value= '';
                         $show_attribute_link_to_post_value= '';
                         $show_attribute_in_post_products_list_value= '';
                         if (!empty($m_parentControl->m_optionEditorItem['attribute_option_values']) and is_array($m_parentControl->m_optionEditorItem['attribute_option_values'])) {
                             foreach( $m_parentControl->m_optionEditorItem['attribute_option_values'] as $next_key=>$next_option ) {
//                                 echo '<pre>++$next_products_attr[attribute_name]::'.print_r($next_products_attr['attribute_name'],true).'</pre>';
                                 if ( $next_option['attr_name'] == $next_products_attr['attribute_name'] ) {
                                     $show_attribute_in_extended_search_value = !empty($next_option['show_attribute_in_extended_search']) ? $next_option['show_attribute_in_extended_search' ] : '';
                                     $show_attribute_link_to_post_value = !empty($next_option['show_attribute_link_to_post' ]) ? $next_option['show_attribute_link_to_post' ] : '';
                                     $show_attribute_in_post_products_list_value = !empty($next_option['show_attribute_in_post_products_list' ]) ? $next_option['show_attribute_in_post_products_list' ] : '';
                                 }
                             }

                         }
//                         echo '<pre>$show_attribute_in_extended_search_value::'.print_r($show_attribute_in_extended_search_value,true).'</pre>';
//                         echo '<pre>$show_attribute_link_to_post_value::'.print_r($show_attribute_link_to_post_value,true).'</pre>';
//                         die("-1 XXZ000000 ");
//                         echo '<pre>$show_attribute_in_post_products_list_value::'.print_r($show_attribute_in_post_products_list_value,true).'</pre>';

                         ?>
                         <td><?php echo $next_products_attr['attribute_label'] . '( <b>' .$next_products_attr['attribute_name']. ' </b>)' ?></td>

                         <td style="vertical-align: top">
                             <div class="form-option-field form-require term-nsn-woo-ext-search_in_extended_search_wrap <?php echo showErrorMessage("nsn-woo-ext-search-show_attribute_in_extended_search_".$next_products_attr['attribute_name'], "nsn-woo-ext-search-show_attribute_in_extended_search_".$next_products_attr['attribute_name'], $m_parentControl->m_hasValidationErrors, true) ?>">
                            <select aria-required="true" id="nsn-woo-ext-search-show_attribute_in_extended_search_<?php echo $next_products_attr['attribute_name'] ?>" name="nsn-woo-ext-search-show_attribute_in_extended_search_<?php echo $next_products_attr['attribute_name'] ?>"  onchange="javascript:nsn_woo_ext_search_backendFuncsObj.show_attribute_in_extended_search_onChange('<?php echo $next_products_attr['attribute_name'] ?>') "    >
                                 <option value="" >  -Select-  </option>
                                 <option value="yes" <?php echo ( strtolower($show_attribute_in_extended_search_value)=='yes' ? 'selected' : '' ) ?> >Yes</option>
                                 <option value="no" <?php echo ( strtolower($show_attribute_in_extended_search_value)=='no' ? 'selected' : '' ) ?> >No</option>
                             </select>&nbsp;
                             <?php echo showErrorMessage("nsn-woo-ext-search-show_attribute_in_extended_search_".$next_products_attr['attribute_name'] , "nsn-woo-ext-search-show_attribute_in_extended_search_" . $next_products_attr['attribute_name'], $m_parentControl->m_hasValidationErrors, false) ?>
                             <p class="nsn_woo_ext_search_next_padding_bottom">If "Yes", <b><?php echo $next_products_attr['attribute_label'] ?></b> be used for<br> filter in filters box and shown in the resulting data listing. </p>
                             </div>
                         </td>

                         <td style="vertical-align: top; <?php echo $show_attribute_in_extended_search_value != 'yes' ? " display:none; " : ""?>" id="td_nsn-woo-ext-search-show_attribute_link_to_post_<?php echo $next_products_attr['attribute_name'] ?>" >
                             <?php //echo '<pre>++::'.print_r($show_attribute_in_extended_search_value,true).'</pre>';   ?>
                             <select aria-required="true" id="nsn-woo-ext-search-show_attribute_link_to_post_<?php echo $next_products_attr['attribute_name'] ?>" name="nsn-woo-ext-search-show_attribute_link_to_post_<?php echo $next_products_attr['attribute_name'] ?>" onchange="javascript:nsn_woo_ext_search_backendFuncsObj.show_attribute_link_to_post_onChange('<?php echo $next_products_attr['attribute_name'] ?>') "  >
                                 <option value="" >  -Select-  </option>
                                 <option value="yes" <?php echo ( strtolower($show_attribute_link_to_post_value)=='yes' ? 'selected' : '' ) ?> >Yes</option>
                                 <option value="no" <?php echo ( strtolower($show_attribute_link_to_post_value)=='no' ? 'selected' : '' ) ?> >No</option>
                             </select>&nbsp;

                             <p class="nsn_woo_ext_search_next_padding_bottom">If "Yes", any <b><?php echo $next_products_attr['attribute_label'] ?></b> item will<br> be link to Post, describing this <b><?php echo $next_products_attr['attribute_label'] ?></b>. </p>


                             <?php if( !empty($m_parentControl->m_optionEditorItem['attribute_option_values']) ) {
                                 foreach( $m_parentControl->m_optionEditorItem['attribute_option_values'] as $next_option_value ) {
//                                 echo '<pre>$next_option_value::'.print_r($next_option_value,true).'</pre>';
                                 ?>

                                <?php if ( $next_option_value['attr_name'] == $next_products_attr['attribute_name'] ) {   ?>
                                <?php showPostsSelectionForAnyAttr($m_parentControl, $next_products_attr, $next_option_value['show_attribute_link_to_post'] != 'yes', ( !empty( $service_category_for_attrs_array[$next_products_attr['attribute_name']] ) ? $service_category_for_attrs_array[$next_products_attr['attribute_name']] : '' ) ) ?>


                                <?php } ?>




                             <?php } ?>
                             <?php } ?>

                         </td>



<!--                         --><?php //echo '<pre>++ $show_in_extended_search_value::'.print_r($show_in_extended_search_value,true).'</pre>';   ?>
                         <td style="vertical-align: top; <?php  echo ($show_attribute_in_extended_search_value  != 'yes' or $show_attribute_link_to_post_value != 'yes' ) ? " display:none " : ""?>" id="td_nsn-woo-ext-search-show_attribute_in_post_products_list_<?php echo $next_products_attr['attribute_name'] ?>">
                             <select aria-required="true" id="nsn-woo-ext-search-show_attribute_in_post_products_list_<?php echo $next_products_attr['attribute_name'] ?>" name="nsn-woo-ext-search-show_attribute_in_post_products_list_<?php echo $next_products_attr['attribute_name'] ?>" >
                                 <option value="" >  -Select-  </option>
                                 <option value="yes" <?php echo ( strtolower($show_attribute_in_post_products_list_value)=='yes' ? 'selected' : '' ) ?> >Yes</option>
                                 <option value="no" <?php echo ( strtolower($show_attribute_in_post_products_list_value)=='no' ? 'selected' : '' ) ?> >No</option>
                             </select>&nbsp;
                             <p class="nsn_woo_ext_search_next_padding_bottom">If "Yes", below of <b><?php echo $next_products_attr['attribute_label'] ?></b> description<br> list of related products is visible.</p>
                         </td>

                     </tr>

                     <tr>
                         <td colspan="5">&nbsp;<hr><hr></td>
                     </tr>

                 <?php } ?>
                 </table>

                </fieldset>

            </div>





            <div >
                <fieldset class="nsn_woo_ext_search_next_padding_bottom nsn_woo_ext_search_block_padding_md nsn_woo_ext_search_thin_border">
                    <legend><h3>3) Fields, which must be shown in the resulting data listing:</h3></legend>
                    <p class="nsn_woo_ext_search_admin_info">Before editing of this block, please fill and save blocks above, as some selection in this block depends on data in blocks above.</p>

                    <?php $fields_to_show_array= !empty($m_parentControl->m_optionEditorItem['fields_to_show_array'])?$m_parentControl->m_optionEditorItem['fields_to_show_array']:array();
//                    echo '<pre>++++  $fields_to_show_array::'.print_r($fields_to_show_array,true).'</pre>';
                    $fields_to_show_value= '';
                    foreach( /*$fields_to_show_array */ $m_parentControl->m_fields_to_show as $next_fields_to_show  /* => $next_fields_to_show_value */ ) {
//                        echo '<pre>$next_fields_to_show::'.print_r($next_fields_to_show,true).'</pre>';
                        foreach( $fields_to_show_array as $next_fields_name=>$next_fields_value ) {
//                            echo '<pre>$next_fields_name::'.print_r($next_fields_name,true).'</pre>';
                            if ( $next_fields_name == $next_fields_to_show ) {
                                $fields_to_show_value= $next_fields_value;
                                break;
                            }
                        }
//                         echo '<pre>$fields_to_show_value::'.print_r($fields_to_show_value,true).'</pre>';
                        ?>
                        <div class="form-option-field form-require term-nsn-woo-ext-search-fields_to_show_<?php echo $next_fields_to_show ?>-wrap <?php echo showErrorMessage("nsn-woo-ext-search-fields_to_show_<?php echo $next_fields_to_show ?>", "nsn-woo-ext-search-fields_to_show_<?php echo $next_fields_to_show ?>", $m_parentControl->m_hasValidationErrors, true) ?>">
                            <label for="nsn-woo-ext-search-fields_to_show_<?php echo $next_fields_to_show ?>"><?php echo ("Show the <b>" . $next_fields_to_show ."</b> field in the resulting data listing") ?></label>
                            <select aria-required="true" id="nsn-woo-ext-search-fields_to_show_<?php echo $next_fields_to_show ?>" name="nsn-woo-ext-search-fields_to_show_<?php echo $next_fields_to_show ?>" >
                                <option value="" >  -Select-  </option>
                                <option value="yes" <?php echo ( $fields_to_show_value == 'yes' ? 'selected' : '' ) ?> >Yes</option>
                                <option value="no" <?php echo ( $fields_to_show_value == 'no' ? 'selected' : '' ) ?> >No</option>
                            </select>&nbsp;

                            <?php echo showErrorMessage("nsn-woo-ext-search-fields_to_show_<?php echo $next_fields_to_show ?>" , "nsn-woo-ext-search-fields_to_show_<?php echo $next_fields_to_show ?>" , $m_parentControl->m_hasValidationErrors, false) ?>

                            <p class="nsn_woo_ext_search_next_padding_bottom"> If "Yes" selected, the field <b><?php echo $next_fields_to_show ?></b> will be visible in the resulting data listing.</p>
                        </div>


                    <?php } ?>



                    <div class="form-option-field form-require term-nsn-woo-ext-search-fields_to_show_rating-wrap <?php echo showErrorMessage("nsn-woo-ext-search-fields_to_show_rating", "nsn-woo-ext-search-fields_to_show_rating", $m_parentControl->m_hasValidationErrors, true) ?>">
                        <?php $fields_to_show_rating= ( !empty($m_parentControl->m_optionEditorItem['fields_to_show_rating']) ? $m_parentControl->m_optionEditorItem['fields_to_show_rating'] : '' ) ?>
                        <label for="nsn-woo-ext-search-fields_to_show_rating">Show the <b>rating</b> field in the resulting data listing</label>
                        <select aria-required="true" id="nsn-woo-ext-search-fields_to_show_rating" name="nsn-woo-ext-search-fields_to_show_rating" >
                            <option value="" >  -Select-  </option>
                            <option value="yes" <?php echo ( strtolower($fields_to_show_rating)=='yes' ? 'selected' : '' ) ?> >Yes</option>
                            <option value="no" <?php echo ( strtolower($fields_to_show_rating)=='no' ? 'selected' : '' ) ?> >No</option>
                        </select>&nbsp;

                        <?php echo showErrorMessage("nsn-woo-ext-search-fields_to_show_rating" , "nsn-woo-ext-search-fields_to_show_rating" , $m_parentControl->m_hasValidationErrors, false) ?>

                        <p class="nsn_woo_ext_search_next_padding_bottom">If "Yes" selected, the field <b>rating</b> will be visible in the resulting data listing.</p>
                    </div>


                    <div class="form-option-field form-require term-nsn-woo-ext-search-fields_to_show_sku-wrap <?php echo showErrorMessage("nsn-woo-ext-search-fields_to_show_sku", "nsn-woo-ext-search-fields_to_show_sku", $m_parentControl->m_hasValidationErrors, true) ?>">
                        <?php $fields_to_show_sku= ( !empty($m_parentControl->m_optionEditorItem['fields_to_show_sku']) ? $m_parentControl->m_optionEditorItem['fields_to_show_sku'] : '' ) ?>
                        <label for="nsn-woo-ext-search-fields_to_show_sku">Show the <b>sku</b> field in the resulting data listing</label>
                        <select aria-required="true" id="nsn-woo-ext-search-fields_to_show_sku" name="nsn-woo-ext-search-fields_to_show_sku" >
                            <option value="" >  -Select-  </option>
                            <option value="yes" <?php echo ( strtolower($fields_to_show_sku)=='yes' ? 'selected' : '' ) ?> >Yes</option>
                            <option value="no" <?php echo ( strtolower($fields_to_show_sku)=='no' ? 'selected' : '' ) ?> >No</option>
                        </select>&nbsp;

                        <?php echo showErrorMessage("nsn-woo-ext-search-fields_to_show_sku" , "nsn-woo-ext-search-fields_to_show_sku" , $m_parentControl->m_hasValidationErrors, false) ?>

                        <p class="nsn_woo_ext_search_next_padding_bottom">If "Yes" selected, the field <b>sku</b> will be visible in the resulting data listing.</p>
                    </div>


                    <div class="form-option-field form-require term-nsn-woo-ext-search-fields_to_show_tags-wrap <?php echo showErrorMessage("nsn-woo-ext-search-fields_to_show_tags", "nsn-woo-ext-search-fields_to_show_tags", $m_parentControl->m_hasValidationErrors, true) ?>">
                        <?php $fields_to_show_tags= ( !empty($m_parentControl->m_optionEditorItem['fields_to_show_tags']) ? $m_parentControl->m_optionEditorItem['fields_to_show_tags'] : '' ) ?>
                        <label for="nsn-woo-ext-search-fields_to_show_tags">Show the <b>tags</b> field in the resulting data listing</label>
                        <select aria-required="true" id="nsn-woo-ext-search-fields_to_show_tags" name="nsn-woo-ext-search-fields_to_show_tags" >
                            <option value="" >  -Select-  </option>
                            <option value="yes" <?php echo ( strtolower($fields_to_show_tags)=='yes' ? 'selected' : '' ) ?> >Yes</option>
                            <option value="no" <?php echo ( strtolower($fields_to_show_tags)=='no' ? 'selected' : '' ) ?> >No</option>
                        </select>&nbsp;

                        <?php echo showErrorMessage("nsn-woo-ext-search-fields_to_show_tags" , "nsn-woo-ext-search-fields_to_show_tags" , $m_parentControl->m_hasValidationErrors, false) ?>

                        <p class="nsn_woo_ext_search_next_padding_bottom">If "Yes" selected, the field <b>tags</b> will be visible in the resulting data listing.</p>
                    </div>

                </fieldset>
            </div >






            <!--
            <div class="form-option-field form-require term-nsn-woo-tools_attribute_in_search-wrap <?php echo showErrorMessage("nsn-woo-tools_attribute_in_search", "nsn-woo-tools_attribute_in_search", $m_parentControl->m_hasValidationErrors, true) ?>">
                <label for="nsn-woo-tools_attribute_in_search"><?php echo esc_html__("Save attributes are used in search.") ?></label>
                <table style="border: 1px dotted grey; ">
                    <tr>
                        <th colspan="2" style="text-align: center">
                            Use in search crireria
                        </th>
                        <th>
                        </th>
                        <th colspan="2" style="text-align: center">
                            Show in the resulting data listing listing
                        </th>
                     </tr>

<?php //foreach( $products_attrs_list as $next_products_attr ) {
//                    //echo '<pre>::'.print_r($next_products_attr,true).'</pre>';
//                    reset($use_in_search_array);
//                    $search_criteria_tag= '';
//                    $search_criteria_readonly_tag= ' disabled ';
//                    $search_criteria_field_type= '';
//                    $show_in_resulting_listing_tag= '';
//                    $search_criteria_field_hint= '';
//                    $search_criteria_all_fields_array= array();
//                    foreach( $use_in_search_array as $next_use_in_search ) {
//                        if ( $next_use_in_search['name'] == $next_products_attr['attribute_name'] ) {
//                            $search_criteria_tag= ' checked ';
//                            $search_criteria_readonly_tag= '';
//                            $search_criteria_field_type= $next_use_in_search['field_type'];
//                            $search_criteria_all_fields_array= $next_use_in_search;
//                            $search_criteria_field_hint= !empty($next_use_in_search['hint']) ? $next_use_in_search['hint'] : '';
//                            //if ( $next_use_in_search['field_type'] == 'range' ) {
//                            //    $search_criteria_field_type_tag = ' selected ';
//                            //}
//                            break;
//                        }
//                    }
//                    reset($show_in_resulting_listing_array);
//                    foreach( $show_in_resulting_listing_array as $next_show_in_resulting_listing ) {
//                        if ($next_show_in_resulting_listing == $next_products_attr['attribute_name']) {
//                            $show_in_resulting_listing_tag= ' checked ';
//                            break;
//                        }
//                    }
//                    //echo '<pre>$search_criteria_readonly_tag::'.print_r($search_criteria_readonly_tag,true).'</pre>';
//
//
//
//
//                    echo '<tr>';
//                    echo '<td style="text-align: right">' . $next_products_attr['attribute_label'] .
//                        ':&nbsp;</td><td>'.
//                        '&nbsp;<input type="checkbox" id="cbx_use_in_search_'.$next_products_attr['attribute_name'].'"
// name="cbx_use_in_search_'.$next_products_attr['attribute_name'].'"value="1" '.$search_criteria_tag.'  onchange="javascript:cbx_use_in_search_onChange( \''.$next_products_attr['attribute_name'].'\') " >&nbsp;<select id="select_use_in_search_field_type_'.$next_products_attr['attribute_name'].'" name="select_use_in_search_field_type_'.$next_products_attr['attribute_name'].'" onchange="javascript:select_use_in_search_field_type_onChange(this.value, \''.$next_products_attr['attribute_name'].'\') " '. $search_criteria_readonly_tag.' ><option value="">  --Select Field Type--  </option> '.
//                        '<option value="single_text" '.($search_criteria_field_type == "single_text" ? " selected " : "").' >Single Text</option>'.
//                        '<option value="range" '.($search_criteria_field_type == "range" ? " selected " : "").' >Range values</option>'.
//                        '<option value="predefined_selection" '.($search_criteria_field_type == "predefined_selection" ? " selected " : "").' >Predefined Selection</option>'.
//                        '<option value="boolean" '.($search_criteria_field_type == "boolean" ? " selected " : "").' > Boolean values</option></select>';
//                    echo '<input type="text" id="use_in_search_hint_'.$next_products_attr['attribute_name'].'" name="use_in_search_hint_'.$next_products_attr['attribute_name'].'" value="'.$search_criteria_field_hint.'" placeholder="Hint Text" '. $search_criteria_readonly_tag.'  >&nbsp;'.show_additive_attr_by_field_type($next_products_attr['attribute_name'], $search_criteria_field_type, $search_criteria_all_fields_array);
//
//                    echo '</td>';
//                    echo '<td style="width: 30px"></td>';
//
//
//
//
//                    echo '<td style="text-align: right">' . $next_products_attr['attribute_label'] .
//                        ':&nbsp;</td>&nbsp;&nbsp;&nbsp;&nbsp;<td>'.
//                        '&nbsp;<input type="checkbox" id="cbx_show_in_resulting_listing_'.$next_products_attr['attribute_name'].'"
//name="cbx_show_in_resulting_listing_'.$next_products_attr['attribute_name'].'" value="1" '.$show_in_resulting_listing_tag.'>';
//                    echo '</td>';
//                    echo '</tr>';
//                }?>
                </table>
                <?php echo showErrorMessage("nsn-woo-tools_attribute_in_search", "nsn-woo-tools_attribute_in_search", $m_parentControl->m_hasValidationErrors, false) ?>
                <p class="nsn_woo_ext_search_next_padding_bottom">Specify which attributes are used in search criteria and which are cisible in dropdown rows.</p>
            </div>  --?


        <!-- Common options: -->




        <div>
            <p class="submit">
                <button type="submit" class="button button-primary" ><?php echo esc_html__("Update") ?></button>
                &nbsp;&nbsp;
                <button type="reset" class="button action" onclick="javascript:document.location='<?php //echo get_site_url() . $m_parentControl->m_pageUrl?>admin.php?page=NSN_WooExtSearch_plugin_info'"><?php echo esc_html__("Cancel") ?></button>
                &nbsp;&nbsp;
            </p>
        </div>

    </form>

</div> <!-- div class="form-editor-wrap" -->